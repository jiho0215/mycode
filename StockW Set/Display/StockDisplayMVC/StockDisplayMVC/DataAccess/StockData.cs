﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StockDisplayMVC.Models;
using StockDisplayMVC.StockDisplaySvc;
using AutoMapper;

namespace StockDisplayMVC.DataAccess
{
    public class StockData
    {
        public GetSMAResponseModel GetSMA(GetSMARequestModel request)
        {
            GetSMAResponse response = default;
            using (var client = new StockDisplaySvcClient())
            {
                response = client.GetSMA(new GetSMARequest()
                {
                    Interval = request.Interval,
                    NumberOfSMA = request.NumberOfSMA,
                    SeriesType = request.SeriesType,
                    Symbol = request.Symbol
                });
            }

            return AutoMapper.Mapper.Map<GetSMAResponseModel>(response);
        }
        public List<StockInfo> GetAllSymbols(int minVolume)
        {
            List<StockInfo> result;
            using (var client = new StockDisplaySvcClient())
            {
                result = client.GetAllSymbols(minVolume);
            }
            return result;
        }
    }
}