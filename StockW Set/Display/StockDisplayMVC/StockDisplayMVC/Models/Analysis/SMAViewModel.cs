﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StockDisplayMVC.Models.Analysis
{
    public class SMAViewModel
    {
        public string Symbol { get; set; }
        public double SimpleMovingAverage { get; set; }
        public DateTime CloseDate { get; set; }
        public double LastClosePrice { get; set; }
        public double LCPSMAPosition
        {
            get
            {
                return (LastClosePrice - SimpleMovingAverage) / LastClosePrice * 100;
            }
        }
        public double Lowest { get; set; }
        public double Highest { get; set; }
        public double LowHighPosition
        {
            get
            {
                return Math.Round(((LastClosePrice - Lowest) / (Highest - Lowest) * 100), 2);
            }
        }
        public double PotentialGain
        {
            get
            {
                return Math.Round((Highest - LastClosePrice) / LastClosePrice * 100, 2);
            }
        }
    }
}