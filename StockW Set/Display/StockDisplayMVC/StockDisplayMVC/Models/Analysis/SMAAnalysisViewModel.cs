﻿using StockDisplayMVC.Models.Analysis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StockDisplayMVC.Models
{
    public class SMAAnalysisViewModel
    {
        public SMAAnalysisViewModel()
        {
            SMAs = new List<SMAViewModel>();
        }
        public List<SMAViewModel> SMAs { get; set; }
        public int Interval { get; set; }
        public string SeriesType { get; set; }
        public int? NumberOfSMA { get; set; }
        public List<int> IntervalList
        {
            get
            {
                return new List<int>(){
                    5,
                    10,
                    30,
                    60,
                    300
                };
            }
        }
        public List<string> SeriesTypeList
        {
            get
            {
                return new List<string>()
                {
                    "Close",
                    "Open",
                    "High",
                    "Low",
                    "Avg"
                };
            }
        }
        public int MinVolume { get; set; }
    }
}