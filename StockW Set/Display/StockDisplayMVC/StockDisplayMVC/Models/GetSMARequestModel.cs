﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StockDisplayMVC.Models
{
    public class GetSMARequestModel
    {
        public int MinVolume { get; set; }
        public string Symbol { get; set; }
        public int Interval { get; set; }
        public string SeriesType { get; set; }
        public int? NumberOfSMA { get; set; }
    }
}