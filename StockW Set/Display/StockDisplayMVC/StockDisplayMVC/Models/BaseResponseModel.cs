﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StockDisplayMVC.Models
{
    public class BaseResponseModel
    {
        public bool IsSuccess { get; set; }
        public string ErrorMsg { get; set; }
    }
}