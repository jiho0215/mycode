﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StockDisplayMVC.Models
{
    public class GetSMAResponseModel : BaseResponseModel
    {
        public string Symbol { get; set; }
        public List<SMAModel> SMAList { get; set; }
        public double LastClosePrice { get; set; }
    }
    public class SMAModel
    {
        public DateTime StartDate { get; set; }
        public double SimpleMovingAverage { get; set; }
        public double Lowest { get; set; }
        public double Highest { get; set; }
    }
}