﻿using AutoMapper;
using StockDisplayMVC.DataAccess;
using StockDisplayMVC.Models;
using StockDisplayMVC.StockDisplaySvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using StockDisplayMVC.DataBuilder;
using StockDisplayMVC.Models.Analysis;

namespace StockDisplayMVC.Controllers
{
    public class HomeController : Controller
    {
        private SMABuilder SMABuilder = new SMABuilder();
        private StockData StockData = new StockData();
        private List<string> Symbols = new List<string>();

        public ActionResult Index()
        {

            return View();
        }
        public ActionResult BasicAnalysis(GetSMARequestModel request)
        {
            if(request.MinVolume == 0)
            {
                request.MinVolume = 10000000;
            }
            Symbols = GetSymbols(request.MinVolume);
            return SMAPartialView(request);
        }

        public ActionResult SMAPartialView(GetSMARequestModel request)
        {
            request.Interval = (request.Interval == 0) ? 30 : request.Interval;
            request.SeriesType = (string.IsNullOrWhiteSpace(request.SeriesType)) ? "Avg" : request.SeriesType;

            var viewModel = new SMAAnalysisViewModel()
            {
                Interval = request.Interval,
                NumberOfSMA = request.NumberOfSMA,
                SeriesType = request.SeriesType,
                MinVolume = request.MinVolume
            };

            for (var i = 0; i < Symbols.Count; i++)
            {
                request.Symbol = Symbols[i];
                var response = StockData.GetSMA(request);
                if(response.SMAList.Count() > 0)
                {
                    var targetSMA = response.SMAList[0];
                    var sma = new SMAViewModel()
                    {
                        Symbol = response.Symbol,
                        LastClosePrice = response.LastClosePrice,
                        SimpleMovingAverage = (targetSMA != null) ? targetSMA.SimpleMovingAverage : 0,
                        CloseDate = (targetSMA != null) ? targetSMA.StartDate : default,
                        Lowest = targetSMA.Lowest,
                        Highest = targetSMA.Highest
                    };
                    viewModel.SMAs.Add(sma);
                }
            }

            return View(viewModel);
        }
        private List<string> GetSymbols(int minVolume)
        {
            var symbols = new List<string>()
            {
                "TSLA","JNUG","AMD","NIO","BA"
            };

            var stockInfos = StockData.GetAllSymbols(minVolume);

            symbols = stockInfos.Where(x => x.ActiveFlag.ToUpper().Equals("Y") && string.IsNullOrEmpty(x.Disabled)).Select(x => x.Symbol).ToList();

            return symbols;
        }
        public ActionResult About()
        {


            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}