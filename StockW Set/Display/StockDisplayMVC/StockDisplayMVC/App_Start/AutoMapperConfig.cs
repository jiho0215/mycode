﻿using StockDisplayMVC.Models;
using StockDisplayMVC.StockDisplaySvc;

namespace StockDisplayMVC.App_Start
{
    public static class AutoMapperConfig
    {
        public static void CreateMaps()
        {
            AutoMapper.Mapper.CreateMap<GetSMARequestModel, GetSMARequest>();
            AutoMapper.Mapper.CreateMap<GetSMAResponse, GetSMAResponseModel>();
            AutoMapper.Mapper.CreateMap<SMA, SMAModel>();
        }
    }
}