﻿
$(document).ready(() => {
 InitSMADatatable();
 //InitFilterSetup();
 //InitRefreshButton();
});

function InitSMADatatable() {
 $("#smaTable").DataTable({
  "order":[[6, "asc"]]
 });
}

function GetNewInterval() {
 return $('input[name=interval-group]:checked').val();
}

function GetNewSeriesType() {
 return $('input[name=series-group]:checked').val();
}
