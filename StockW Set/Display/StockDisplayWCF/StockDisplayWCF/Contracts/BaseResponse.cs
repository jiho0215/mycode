﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StockDisplayWCF.Contracts
{
    public class BaseResponse
    {
        public bool IsSuccess { get; set; }
        public string ErrorMsg { get; set; }
    }
}