﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StockDisplayWCF.Contracts
{
    public class GetSMAResponse :BaseResponse
    {
        public GetSMAResponse()
        {
            SMAList = new List<SMA>();
        }
        public string Symbol { get; set; }
        public List<SMA> SMAList { get; set; }
        public double LastClosePrice { get; set; }
    }
    public class SMA
    {
        public DateTime StartDate { get; set; }
        public double SimpleMovingAverage { get; set; }
        public double Lowest { get; set; }
        public double Highest { get; set; }
    }
}