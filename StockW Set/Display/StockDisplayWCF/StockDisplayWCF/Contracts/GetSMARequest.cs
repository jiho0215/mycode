﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StockDisplayWCF.Contracts
{
    public class GetSMARequest
    {
        public string Symbol { get; set; }
        public int Interval { get; set; }
        public List<int> IntervalOptions
        {
            get
            {
                return new List<int>()
                {
                    5, 30
                };
            }
        }
        //public string TimePeriod { get; set; }
        //public List<string> TimePeriodOptions
        //{
        //    get
        //    {
        //        return new List<string>()
        //        {
        //            "day","weekly","monthly"
        //        };
        //    }
        //}
        public string SeriesType { get; set; }
        public List<string> SeriesTypeOptions
        {
            get
            {
                return new List<string>()
                {
                    "Close","Open","High","Low","Average"
                };
            }
        }
        public int? NumberOfSMA { get; set; }
    }
}