﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StockDisplayWCF.Contracts
{
    public class StockStatistics
    {
        public string Symbol { get; set; }
        public double AverageVolatility { get; set; }
        public double AverageVolume { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int StartDay { get; set; }
        public int EndDay { get; set; }
        public double Movement { get; set; }
    }
}