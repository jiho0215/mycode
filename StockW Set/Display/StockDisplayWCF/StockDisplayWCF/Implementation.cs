﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StockDisplayWCF.Contracts;
using StockDisplayWCF.DataAccess;

namespace StockDisplayWCF
{
    public class Implementation
    {
        private GetEODLogStatistics GetEODLogStatistics = new GetEODLogStatistics();
        private PositiveMovement PositiveMovement = new PositiveMovement();
        public List<StockAverageInfo> GetBabyStocks(int? minVolatility, int? minVolume)
        {
            return GetEODLogStatistics.GetBabyStocks(minVolatility, minVolume);
        }

        public List<StockAverageInfo> GetAvgVolumeVolatility(int minVolume, int minVolatility, int startDay, int endDay)
        {
            return GetEODLogStatistics.GetAvgVolumeVolatility(minVolume, minVolatility, startDay, endDay);
        }
        public List<EODLogGetMovement_Result> GetMovement(string symbol, int startDay, int endDay, bool useAverage = false)
        {
            return GetEODLogStatistics.GetMovement(symbol, startDay, endDay, useAverage);
        }
        public void ShortPositiveMovements(int endDay = 1, int startDay = 0)
        {
            PositiveMovement.ShortPositiveMovements(endDay, startDay);
        }

        public List<StockAverageInfo> GetMovementByFilter(int startDay, int endDay, int minVolume)
        {
            return GetEODLogStatistics.GetMovementByFilter(startDay, endDay, minVolume);
        }

        public List<StockAverageInfo> GetSequancialPositiveMovement(int startDay, int endDay, int minVolume = 100000, bool isPositive = true)
        {
            return PositiveMovement.GetSequancialPositiveMovement(startDay, endDay, minVolume, isPositive);
        }

        public GetSMAResponse GetSMA(GetSMARequest request)
        {
            return GetEODLogStatistics.GetSMA(request);
        }

        public List<StockInfo> GetAllSymbols(int minVolume)
        {
            return GetEODLogStatistics.GetAllSymbols(minVolume);
        }
    }
}