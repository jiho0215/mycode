﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StockDisplayWCF.Contracts;

namespace StockDisplayWCF.DataAccess
{
    public class PositiveMovement
    {
        private StockWEntities Entity = new StockWEntities();
        public void ShortPositiveMovements(int endDay = 1, int startDay = 0)
        {
            int minVolume = 200000;
            int minVolatility = 1;
            
            //Filter by volume and volatility
            var candidates = new Implementation().GetAvgVolumeVolatility(minVolume, minVolatility, startDay, endDay);
            var candidateSymbols = candidates.Select(x => x.Symbol).ToList();

            List<StockStatistics> stockStatistics = new List<StockStatistics>();

            foreach(string symbol in candidateSymbols)
            {
                var movement = new Implementation().GetMovement(symbol, startDay, endDay);
                StockStatistics ss = new StockStatistics()
                {
                    Symbol = symbol,
                    StartDay = startDay,
                    EndDay = endDay,
                    Movement = (double) movement[0].Movement
                };
                stockStatistics.Add(ss);
            }

            //Filter by closing position 
            
            
            //Filter by distribution

        }

        public List<StockAverageInfo> GetSequancialPositiveMovement(int startDay, int endDay, int minVolume = 100000, bool isPositive = true)
        {
            var result = new List<StockAverageInfo>();
            Dictionary<string, StockAverageInfo> dic = new Dictionary<string, StockAverageInfo>();
            Dictionary<string, StockAverageInfo> tempDic = new Dictionary<string, StockAverageInfo>();

            int sday = startDay;
            int count = endDay - startDay;
            for (int i = 0; i < count; i++)
            {
                var response = new Implementation().GetMovementByFilter(sday, sday + 1, minVolume);
                if (i == 0)
                {
                    foreach (var row in response)
                    {
                        if(isPositive && row.Movement <= 0 || !isPositive && row.Movement >= 0)
                        {
                            continue;
                        }
                        var info = new StockAverageInfo()
                        {
                            Symbol = row.Symbol,
                            Movement = row.Movement,
                            AverageVolume = row.AverageVolume
                        };
                        dic.Add(row.Symbol, info);
                    }
                }
                else
                {
                    tempDic = dic;
                    dic = new Dictionary<string, StockAverageInfo>();
                    foreach (var row in response)
                    {
                        if (isPositive && row.Movement <= 0 || !isPositive && row.Movement >= 0)
                        {
                            continue;
                        }
                        if (tempDic.ContainsKey(row.Symbol))
                        {
                            var exInfo = tempDic[row.Symbol];
                            var avgMovement = ((exInfo.Movement * (i + 1)) + row.Movement) / (i + 2);
                            var avgVolume = ((exInfo.AverageVolume * (i + 1)) + row.AverageVolume) / (i + 2);
                            var info = new StockAverageInfo()
                            {
                                Symbol = row.Symbol,
                                Movement = avgMovement,
                                AverageVolume = avgVolume
                            };
                            dic.Add(row.Symbol, info);
                        }
                    }
                }
                sday++;
            }

            foreach(var item in dic)
            {
                result.Add(item.Value);
            }
            if (isPositive)
            {
                return result.OrderByDescending(x => x.Movement).ToList();
            }
            else
            {
                return result.OrderBy(x => x.Movement).ToList();
            }
        }
    }
}