﻿using StockDisplayWCF.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StockDisplayWCF.DataAccess
{
    public class GetEODLogStatistics
    {
        private StockWEntities Entity = new StockWEntities();

        public List<StockAverageInfo> GetBabyStocks(int? minVolatility, int? minVolume)
        {
            var response = Entity.EODLog30DayFreshGetVolaVolume(minVolume, minVolatility);

            var stockAvgInfoList = new List<StockAverageInfo>();

            foreach (var res in response)
            {
                var stockAvgInfo = new StockAverageInfo()
                {
                    Symbol = res.Symbol,
                    StartDate = (DateTime)res.startdate,
                    EndDate = (DateTime)res.endDate,
                    AverageVolume = (double)res.AvgVolume,
                    AverageVolatility = (double)res.AvgVolatility
                };
                stockAvgInfoList.Add(stockAvgInfo);
            }
            return stockAvgInfoList;
        }

        public List<StockAverageInfo> GetAvgVolumeVolatility(int minVolume, int minVolatility, int startDay, int endDay)
        {
            var response = Entity.EODLogGetVolaVolume(null, startDay, endDay, minVolume, minVolatility, "N");

            var list = new List<StockAverageInfo>();

            foreach (var res in response)
            {
                var si = new StockAverageInfo()
                {
                    Symbol = res.Symbol,
                    AverageVolatility = (double)res.AvgVolatility,
                    AverageVolume = (double)res.AvgVolume,
                    StartDate = (DateTime)res.startdate,
                    EndDate = (DateTime)res.endDate
                };
                list.Add(si);
            }

            return list;
        }

        public List<EODLogGetMovement_Result> GetMovement(string symbol, int startDay, int endDay, bool useAverage = false)
        {
            return Entity.EODLogGetMovement(symbol, startDay, endDay, (useAverage) ? "Y" : "N").ToList();
        }

        public List<StockAverageInfo> GetMovementByFilter(int startDay, int endDay, int minVolume)
        {
            var result = new List<StockAverageInfo>();
            var eodLogs = Entity.EODLogGetMovementByFilter(null, startDay, endDay, minVolume, "N");
            foreach (var log in eodLogs)
            {
                var sInfo = new StockAverageInfo()
                {
                    Symbol = log.symbol,
                    Movement = (double)log.Movement,
                    StartDate = (DateTime)log.startdate,
                    EndDate = (DateTime)log.enddate,
                    AverageVolume = (double)log.AvgVolume
                };
                result.Add(sInfo);
            }
            return result;
        }

        public GetSMAResponse GetSMA(GetSMARequest request)
        {
            var response = new GetSMAResponse()
            {
                Symbol = request.Symbol
            };
            var allDataDesc = Entity.EODLogs.Where(x => x.Symbol == request.Symbol).OrderByDescending(x => x.CloseDate).ToList();
            var numberOfSMA = (request.NumberOfSMA == null || request.NumberOfSMA == 0) ? 5 : request.NumberOfSMA;
            var interval = (request.Interval == 0) ? 5 : request.Interval;
            var maxI = allDataDesc.Count() / interval;
            maxI = (numberOfSMA > maxI) ? maxI : (int)numberOfSMA;
            for (var i = 0; i < maxI; i++)
            {
                var sma = new SMA();
                var lowest = 0.0;
                var highest = 0.0;
                double sum = default;
                for (var j = 1; j <= interval; j++)
                {
                    var targetIndex = (i * interval) + j - 1;
                    var targetEODLog = allDataDesc[targetIndex];
                    double targetPrice = default;
                    if (request.SeriesType?.ToLower() == "close")
                    {
                        targetPrice = targetEODLog.Close;
                    }
                    else if (request.SeriesType?.ToLower() == "open")
                    {
                        targetPrice = targetEODLog.Open;
                    }
                    else if (request.SeriesType?.ToLower() == "high")
                    {
                        targetPrice = targetEODLog.High;
                    }
                    else if (request.SeriesType?.ToLower() == "low")
                    {
                        targetPrice = targetEODLog.Low;
                    }
                    else
                    {
                        targetPrice = (targetEODLog.High + targetEODLog.Low) / 2;
                    }
                    if (j == 1)
                    {
                        sma.StartDate = targetEODLog.CloseDate;
                        lowest = targetEODLog.Low;
                        highest = targetEODLog.High;
                    }
                    lowest = (lowest > targetEODLog.Low) ? targetEODLog.Low : lowest;
                    highest = (highest < targetEODLog.High) ? targetEODLog.High : highest;
                    sum += targetPrice;
                }
                sma.SimpleMovingAverage = sum / interval;
                sma.Lowest = lowest;
                sma.Highest = highest;
                response.SMAList.Add(sma);
                response.LastClosePrice = allDataDesc[0].Close;
                response.IsSuccess = true;
            }
            return response;
        }

        public List<StockInfo> GetAllSymbols(int minVolume)
        {
            var result = Entity.EODLogGetSymbolByAvgVolume(null, 0, 100, minVolume);
            var symbols = result.Select(x => x.Symbol).ToList();

            var stocks = Entity.StockInfoes.Where(x => symbols.Contains(x.Symbol)).ToList();
            return stocks;
        }
    }
}