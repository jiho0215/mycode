﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using StockDisplayWCF.Contracts;

namespace StockDisplayWCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Service1 : IStockDisplaySvc
    {
        Implementation Implementation = new Implementation();

        public string GetData(int value)
        {
            return string.Format("You entered: {0}", value);
        }

        //Get 30 days fresh stocks with limited volatility and volume
        public List<StockAverageInfo> GetBabyStocks(int? minVolatility, int? minVolume)
        {
            return Implementation.GetBabyStocks(minVolatility, minVolume);
        }

        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite == null)
            {
                throw new ArgumentNullException("composite");
            }
            if (composite.BoolValue)
            {
                composite.StringValue += "Suffix";
            }
            return composite;
        }

        public List<StockAverageInfo> GetAvgVolumeVolatility(int minVolume, int minVolatility, int startDay, int endDay)
        {
            return Implementation.GetAvgVolumeVolatility(minVolume, minVolatility, startDay, endDay);
        }
        public void ShortPositiveMovements(int endDay = 1, int startDay = 0)
        {
            Implementation.ShortPositiveMovements(endDay, startDay);
        }

        public List<StockAverageInfo> GetMovementByFilter(int startDay, int endDay, int minVolume)
        {
            return Implementation.GetMovementByFilter(startDay, endDay, minVolume);
        }
        public List<StockAverageInfo> GetSequancialPositiveMovement(int startDay, int endDay, int minVolume = 100000, bool isPositive = true)
        {
            return Implementation.GetSequancialPositiveMovement(startDay, endDay, minVolume, isPositive);
        }

        public GetSMAResponse GetSMA(GetSMARequest request)
        {
            return Implementation.GetSMA(request);
        }
        public List<StockInfo> GetAllSymbols(int minVolume)
        {
            return Implementation.GetAllSymbols(minVolume);
        }
    }
}
