﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace AVAPISvc.Contracts.Responses
{
    [DataContract]
    public class GetDailyResponse
    {
        [DataMember]
        public string Symbol { get; set; }
        [DataMember]
        public string RequestURL { get; set; }
        [DataMember]
        public List<EODLog> EODLogList { get; set; }
        [DataMember]
        public bool IsSucess { get; set; }
        [DataMember]
        public string ErrorMsg { get; set; }
        [DataMember]
        public string WarningMsg { get; set; }
    }
}