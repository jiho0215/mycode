﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace AVAPISvc.Contracts.Responses
{
    [DataContract]
    public class GetIntradayResponse
    {
        [DataMember]
        public string Symbol { get; set; }
        public List<IntraDayLog> IntradayLogList { get; set; }
        [DataMember]
        public bool IsSucess { get; set; }
        [DataMember]
        public string ErrorMsg { get; set; }
        [DataMember]
        public string WarningMsg { get; set; }
    }
}