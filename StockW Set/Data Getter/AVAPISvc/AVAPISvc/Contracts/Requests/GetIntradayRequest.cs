﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace AVAPISvc.Contracts.Requests
{
    [DataContract]
    public class GetIntradayRequest
    {
        [DataMember]
        public string Symbol { get; set; }
    }
}