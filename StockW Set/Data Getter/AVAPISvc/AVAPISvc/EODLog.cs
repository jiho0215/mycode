//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AVAPISvc
{
    using System;
    using System.Collections.Generic;
    
    public partial class EODLog
    {
        public string Symbol { get; set; }
        public System.DateTime CloseDate { get; set; }
        public System.DateTime CRE_DTTM { get; set; }
        public double Open { get; set; }
        public double Close { get; set; }
        public double Low { get; set; }
        public double High { get; set; }
        public decimal Volume { get; set; }
    }
}
