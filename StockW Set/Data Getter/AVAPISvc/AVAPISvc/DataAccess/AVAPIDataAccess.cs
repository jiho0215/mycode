﻿//using Avapi;
//using Avapi.AvapiTIME_SERIES_DAILY;
//using Avapi.AvapiTIME_SERIES_INTRADAY;
using AVAPISvc.AVAPI;
using AVAPISvc.AVAPI.TIME_SERIES_DAILY;
using AVAPISvc.AVAPI.TIME_SERIES_INTRADAY;
using AVAPISvc.Contracts.Requests;
using AVAPISvc.Contracts.Responses;
using AVAPISvc.Helper;
using System;

namespace AVAPISvc.DataAccess
{
    public class AVAPIDataAccess
    {
        private readonly IAvapiConnection Connection = new AVAPIConnectionHelper().GetConnector();
        public IAvapiResponse_TIME_SERIES_DAILY GetDailyCompact(GetDailyRequest request)
        {
            var time_series_daily = Connection.GetQueryObject_TIME_SERIES_DAILY();
            var dailyResponse = time_series_daily.Query(request.Symbol, Const_TIME_SERIES_DAILY.TIME_SERIES_DAILY_outputsize.compact);

            return dailyResponse;
        }
        public IAvapiResponse_TIME_SERIES_DAILY GetDailyFull(GetDailyRequest request)
        {
            var time_series_daily = Connection.GetQueryObject_TIME_SERIES_DAILY();
            var dailyResponse = time_series_daily.Query(request.Symbol, Const_TIME_SERIES_DAILY.TIME_SERIES_DAILY_outputsize.full);

            return dailyResponse;
        }
        public IAvapiResponse_TIME_SERIES_INTRADAY GetIntradayCompact(GetIntradayRequest request, Const_TIME_SERIES_INTRADAY.TIME_SERIES_INTRADAY_interval interval)
        {
            var time_series_intraday = Connection.GetQueryObject_TIME_SERIES_INTRADAY();
            var intradayResponse = time_series_intraday.Query(request.Symbol, interval, Const_TIME_SERIES_INTRADAY.TIME_SERIES_INTRADAY_outputsize.compact);

            return intradayResponse;
        }
    }
}