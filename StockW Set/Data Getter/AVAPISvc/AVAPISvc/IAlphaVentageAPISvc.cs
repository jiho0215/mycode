﻿using Avapi.AvapiTIME_SERIES_INTRADAY;
using AVAPISvc.Contracts.Requests;
using AVAPISvc.Contracts.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace AVAPISvc
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IAlphaVentageAPISvc" in both code and config file together.
    [ServiceContract]
    public interface IAlphaVentageAPISvc
    {
        [OperationContract]
        GetDailyResponse GetEODCompact(GetDailyRequest request);

        [OperationContract]
        GetDailyResponse GetEODFull(GetDailyRequest request);

        [OperationContract]
        GetIntradayResponse GetIntraday(GetIntradayRequest request, Const_TIME_SERIES_INTRADAY.TIME_SERIES_INTRADAY_interval interval);
    }
}
