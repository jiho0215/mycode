﻿using Avapi.AvapiTIME_SERIES_DAILY;
using Avapi.AvapiTIME_SERIES_INTRADAY;
using AVAPISvc.Contracts.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AVAPISvc.Converter
{
    public class DailyConverter
    {
        public List<EODLog> ConvertDaily(string symbol, IList<TimeSeries_Type_TIME_SERIES_DAILY> timeSeries)
        {
            var EODList = new List<EODLog>();

            for (int i = 0; i < timeSeries.Count(); i++)
            {
                var log = timeSeries[i];

                EODLog eodLog = new EODLog()
                {
                    Symbol = symbol,
                    CloseDate = Convert.ToDateTime(log.DateTime),
                    Close = (log.close.Equals("0")) ? 0 : Convert.ToDouble(log.close),
                    Open = (log.open.Equals("0")) ? 0 : Convert.ToDouble(log.open),
                    Low = (log.low.Equals("0")) ? 0 : Convert.ToDouble(log.low),
                    High = (log.high.Equals("0")) ? 0 : Convert.ToDouble(log.high),
                    CRE_DTTM = DateTime.Now,
                    Volume = (log.volume.Equals("0")) ? 0 : Convert.ToInt32(log.volume)
                };

                EODList.Add(eodLog);
            }

            return EODList;
        }

        public List<IntraDayLog> ConvertIntraday(IList<TimeSeries_Type_TIME_SERIES_INTRADAY> timeSeries)
        {
            var IntradayLog = new List<IntraDayLog>();


            return IntradayLog;
        }
    }
}