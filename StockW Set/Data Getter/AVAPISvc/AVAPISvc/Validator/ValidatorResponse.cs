﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AVAPISvc.Validator
{
    public class ValidatorResponse
    {
        public bool IsSucess { get; set; }
        public string ErrorMsg { get; set; }
        public string WarningMsg { get; set; }

    }
}