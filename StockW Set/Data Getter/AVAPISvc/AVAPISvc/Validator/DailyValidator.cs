﻿using Avapi.AvapiTIME_SERIES_DAILY;
using AVAPISvc.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AVAPISvc.Validator
{
    public class DailyValidator
    {
        public ValidatorResponse ValidateRawData(IAvapiResponse_TIME_SERIES_DAILY rawData)
        {
            var baseResponse = new ValidatorResponse();

            if(rawData == null)
            {
                baseResponse.ErrorMsg = "Unexpected Error from ValidateRawData(): Null response from Alpha Ventage";
                return baseResponse;
            }

            if(rawData.Data == null)
            {
                baseResponse.ErrorMsg = "Unexpected Error from ValidateRawData(): Null Data";
                return baseResponse;
            }

            if(rawData.Data.Error)
            {
                baseResponse.ErrorMsg = rawData.Data.ErrorMessage;
                return baseResponse;
            }

            if (rawData.Data.TimeSeries == null || rawData.Data.TimeSeries.Count == 0)
            {
                baseResponse.ErrorMsg = "Unexpected Error from ValidateRawData(): Null Time Series";
                return baseResponse;
            }

            baseResponse.IsSucess = true;
            return baseResponse;
        }
    }
}