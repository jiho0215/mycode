﻿using Avapi.AvapiTIME_SERIES_INTRADAY;
using AVAPISvc.Contracts.Requests;
using AVAPISvc.Contracts.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace AVAPISvc
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "AlphaVentageAPISvc" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select AlphaVentageAPISvc.svc or AlphaVentageAPISvc.svc.cs at the Solution Explorer and start debugging.
    public class AlphaVentageAPISvc : IAlphaVentageAPISvc
    {
        public GetDailyResponse GetEODCompact(GetDailyRequest request)
        {
            return new Implement().GetDailyCompact(request);
        }


        public GetDailyResponse GetEODFull(GetDailyRequest request)
        {
            return new Implement().GetDailyFull(request);
        }


        public GetIntradayResponse GetIntraday(GetIntradayRequest request, Const_TIME_SERIES_INTRADAY.TIME_SERIES_INTRADAY_interval interval)
        {
            return new Implement().GetIntraday(request, interval);
        }
    }
}
