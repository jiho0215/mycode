﻿using AVAPISvc.Contracts.Responses;
using AVAPISvc.Contracts.Requests;
using AVAPISvc.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Avapi.AvapiTIME_SERIES_INTRADAY;
using AVAPISvc.Converter;
using AVAPISvc.Validator;

namespace AVAPISvc
{
    public class Implement
    {
        private readonly AVAPIDataAccess AVAPIDataAccess = new AVAPIDataAccess();
        private readonly DailyValidator DailyValidator = new DailyValidator();
        private readonly DailyConverter DailyConverter = new DailyConverter();
        public GetDailyResponse GetDailyCompact(GetDailyRequest request)
        {
            var dailyRawData = AVAPIDataAccess.GetDailyCompact(request);
            var validatorResponse = DailyValidator.ValidateRawData(dailyRawData);
            var response = new GetDailyResponse()
            {
                Symbol = request.Symbol,
                RequestURL = dailyRawData.LastHttpRequest,
                IsSucess = validatorResponse.IsSucess,
                ErrorMsg = validatorResponse.ErrorMsg,
                WarningMsg = validatorResponse.WarningMsg
            };
            if(response.IsSucess)
            {
                response.EODLogList = DailyConverter.ConvertDaily(request.Symbol, dailyRawData.Data.TimeSeries);
            }
            return response;
        }
        public GetDailyResponse GetDailyFull(GetDailyRequest request)
        {
            var dailyRawData = AVAPIDataAccess.GetDailyFull(request);
            var validatorResponse = DailyValidator.ValidateRawData(dailyRawData);
            var response = new GetDailyResponse()
            {
                Symbol = request.Symbol,
                IsSucess = validatorResponse.IsSucess,
                ErrorMsg = validatorResponse.ErrorMsg,
                WarningMsg = validatorResponse.WarningMsg
            };
            if (response.IsSucess)
            {
                response.EODLogList = DailyConverter.ConvertDaily(request.Symbol, dailyRawData.Data.TimeSeries);
            }
            return response;
        }
        public GetIntradayResponse GetIntraday(GetIntradayRequest request, Const_TIME_SERIES_INTRADAY.TIME_SERIES_INTRADAY_interval interval)
        {
            var response = new GetIntradayResponse();
            //var intradayRawData = AVAPIDataAccess.GetIntradayCompact(request, interval);
            //response = (GetDailyResponse)DailyValidator.ValidateRawData(intradayRawData);
            //if (response.IsSucess)
            //{
            //    response.IntradayLogList = DailyConverter.ConvertDaily(request.Symbol, intradayRawData.Data.TimeSeries);
            //}
            return response;
        }
    }
}