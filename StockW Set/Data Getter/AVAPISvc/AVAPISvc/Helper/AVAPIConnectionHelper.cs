﻿using Avapi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace AVAPISvc.Helper
{
    public class AVAPIConnectionHelper
    {
        public IAvapiConnection GetConnector()
        {
            IAvapiConnection connection = AvapiConnection.Instance;

            var api_key = WebConfigurationManager.AppSettings["API_Key"];
            // Set up the connection and pass the API_KEY provided by alphavantage.co
            connection.Connect(api_key);
            return connection;
        }
    }
}