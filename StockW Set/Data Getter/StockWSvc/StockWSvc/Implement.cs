﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StockWSvc.Contracts.Response;
using StockWSvc.Contracts.Request;
using StockWSvc.DataAccess;

namespace StockWSvc
{
    public class Implement
    {
        private readonly StockWDataAccess StockWDataAccess = new StockWDataAccess();
        public InsertDailyLogResponse InsertDailyLog(InsertDailyLogRequest request)
        {
            return StockWDataAccess.InsertDailyLog(request);
        }
        public GetAllSymbolsResponse GetAllSymbols()
        {
            return StockWDataAccess.GetAllSymbols();
        }
    }
}