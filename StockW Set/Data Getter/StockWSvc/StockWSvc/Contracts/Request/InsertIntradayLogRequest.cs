﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace StockWSvc.Contracts.Request
{
    [DataContract]
    public class InsertIntradayLogRequest
    {
        [DataMember]
        public List<IntraDayLog> IntradayLogList { get; set; }
    }
}