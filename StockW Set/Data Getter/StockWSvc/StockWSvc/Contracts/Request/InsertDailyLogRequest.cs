﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace StockWSvc.Contracts.Request
{
    [DataContract]
    public class InsertDailyLogRequest
    {
        [DataMember]
        public List<EODLog> EODLogList { get; set; }
    }
}