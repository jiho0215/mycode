﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace StockWSvc.Contracts.Response
{
    [DataContract]
    public class GetAllSymbolsResponse
    {
        public GetAllSymbolsResponse()
        {
            Symbols = new List<string>();
            ErrorMsgList = new List<string>();
            WarningMsgList = new List<string>();
        }
        [DataMember]
        public List<string> Symbols { get; set; }
        [DataMember]
        public bool IsSucess { get; set; }

        [DataMember]
        public List<string> ErrorMsgList { get; set; }

        [DataMember]
        public List<string> WarningMsgList { get; set; }
    }
}