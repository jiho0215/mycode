﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace StockWSvc.Contracts.Response
{
    [DataContract]
    public class InsertIntradayLogResponse
    {
        [DataMember]
        public bool IsSucess { get; set; }

        [DataMember]
        public List<string> ErrorMsgList { get; set; }

        [DataMember]
        public List<string> WarningMsgList { get; set; }
    }
}