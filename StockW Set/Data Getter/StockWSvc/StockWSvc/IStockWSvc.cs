﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using StockWSvc.Contracts.Response;
using StockWSvc.Contracts.Request;

namespace StockWSvc
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IStockWSvc" in both code and config file together.
    [ServiceContract]
    public interface IStockWSvc
    {
        [OperationContract]
        InsertDailyLogResponse InsertDailyLog(InsertDailyLogRequest request);

        [OperationContract]
        GetAllSymbolsResponse GetAllSymbols();
    }
}
