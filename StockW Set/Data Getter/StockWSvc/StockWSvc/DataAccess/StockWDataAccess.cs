﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StockWSvc.Contracts.Response;
using StockWSvc.Contracts.Request;

namespace StockWSvc.DataAccess
{
    public class StockWDataAccess
    {
        public InsertDailyLogResponse InsertDailyLog(InsertDailyLogRequest request)
        {
            var response = new InsertDailyLogResponse();
            using (var db = new StockWEntities())
            {
                var insertList = request.EODLogList.Where(x => !db.EODLogs.Where(y => y.Symbol == x.Symbol).Select(z => z.CloseDate).ToList().Contains(x.CloseDate)).ToList();

                try
                {
                    db.EODLogs.AddRange(insertList);
                    db.SaveChanges();
                }
                catch (Exception e)
                {
                    response.IsSucess = false;
                    response.ErrorMsgList.Add(e.Message);
                }
            }
            response.IsSucess = true;
            return response;
        }
        public GetAllSymbolsResponse GetAllSymbols()
        {
            var response = new GetAllSymbolsResponse();
            response.IsSucess = true;
            try
            {
                using (var db = new StockWEntities())
                {
                    response.Symbols.AddRange(db.StockInfoes.OrderBy(x => x.Symbol).Select(x => x.Symbol.Trim()).ToList());
                }
            }
            catch (Exception e)
            {
                response.ErrorMsgList.Add(e.Message);
                response.IsSucess = false;
            }
            return response;
        }
    }
}