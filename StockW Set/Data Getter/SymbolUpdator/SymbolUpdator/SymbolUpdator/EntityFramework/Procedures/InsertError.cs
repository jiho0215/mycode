﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SymbolUpdator.EntityFramework.Procedures
{
    public class InsertError
    {
        public void InsertErrorMsg(string symbol, string msg)
        {
            using (var db = new StockWEntities())
            {
                var error = new Error_Log()
                {
                    LogId = decimal.Parse(DateTime.Now.ToString("yyMMddHHmmssFFF")),
                    Symbol = symbol,
                    Message = msg,
                    CRE_DTTM = DateTime.Now
                };
                db.Error_Log.Add(error);
                db.SaveChanges();
                Console.WriteLine("Error : " + msg + " Symbol: " + symbol);
            }
        }
    }
}
