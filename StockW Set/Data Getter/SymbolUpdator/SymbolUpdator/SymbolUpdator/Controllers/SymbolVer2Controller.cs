﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SymbolUpdator.Helpers;
using System.IO;
using System.Data.OleDb;
using System.Data;
using Excel = Microsoft.Office.Interop.Excel;
using SymbolUpdator.EntityFramework.Procedures;
using System.Net;
using SymbolUpdator.Models;

namespace SymbolUpdator.Controllers
{
    public class SymbolVer2Controller
    {
        SymbolData SymbolData = new SymbolData();
        DownloadHelper DownloadHelper = new DownloadHelper();
        public void RefreshSymbolList()
        {
            //Nasdaq traded List
            var nasdaqListFTPURL = SymbolUpdator.Settings.Default.NasdaqTraderTFSDirectory + SymbolUpdator.Settings.Default.NasdaqTraded; 
            var symbolInfoLineList = DownloadSymbolFile(nasdaqListFTPURL);
            var symbolList = ConvertNasdaqTradedList(symbolInfoLineList);
            AddMissingSymbols(symbolList);

            symbolInfoLineList = null;
            symbolList = null;

            //Other List
            var otherListFTPURL = SymbolUpdator.Settings.Default.NasdaqTraderTFSDirectory + SymbolUpdator.Settings.Default.OtherLIsted;
            symbolInfoLineList = DownloadSymbolFile(otherListFTPURL);
            symbolList = ConvertOtherList(symbolInfoLineList);
            AddMissingSymbols(symbolList);
        }
        public List<string> DownloadSymbolFile(string url)
        {
            var symbolInfoLineList = new List<string>();
            if(string.IsNullOrEmpty(url))
            {
                return null;
            }
            using (var request = new WebClient())
            {
                request.Credentials = new NetworkCredential("anonymous", "");
                try
                {
                    var newFileData = request.DownloadData(url);
                    if(newFileData == null)
                    {
                        return null;
                    }
                    else
                    {
                        using(var stream = new MemoryStream(newFileData))
                        using(var reader = new StreamReader(stream))
                        {
                            while(!reader.EndOfStream)
                            {
                                var line = reader.ReadLine();
                                symbolInfoLineList.Add(line);
                            }
                        }
                    }

                }
                catch (Exception e)
                {
                    throw e;
                }
            }
            return symbolInfoLineList;
        }

        public List<StockInfo> ConvertNasdaqTradedList(List<string> symbolInfoLineList)
        {
            var symbolList = new List<StockInfo>();
            //Nasdaq Traded| Symbol | Security Name | Listing Exchange | Market Category | ETF | Round Lot Size| Test Issue | Financial Status | CQS Symbol | NASDAQ Symbol | NextShares
            //Y | A | Agilent Technologies, Inc.Common Stock| N | | N | 100 | N || A | A | N
            for (var i = 1; i < symbolInfoLineList.Count; i++)
            {
                try
                {
                    var symbolInfoStringList = symbolInfoLineList[i].Split('|').ToList();
                    if(symbolInfoStringList.Count() < 12)
                    {
                        continue;
                    }
                    symbolList.Add(new StockInfo()
                    {
                        Symbol = symbolInfoStringList[10],
                        ActiveFlag = "Y",
                        CreateDTTM = DateTime.Now,
                        Industry = symbolInfoStringList[4],
                        Name = symbolInfoStringList[2]
                    });
                }
                catch(Exception e)
                {
                    throw e;
                }
            }
            return symbolList;
        }

        public List<StockInfo> ConvertOtherList(List<string> symbolInfoLineList)
        {
            var symbolList = new List<StockInfo>();
            //ACT Symbol| Security Name | Exchange | CQS Symbol | ETF | Round Lot Size| Test Issue | NASDAQ Symbol
            //A | Agilent Technologies, Inc.Common Stock| N | A | N | 100 | N | A
            for (var i = 1; i < symbolInfoLineList.Count; i++)
            {
                try
                {
                    var symbolInfoStringList = symbolInfoLineList[i].Split('|').ToList();
                    if (symbolInfoStringList.Count() < 8)
                    {
                        continue;
                    }
                    symbolList.Add(new StockInfo()
                    {
                        Symbol = symbolInfoStringList[7],
                        ActiveFlag = "Y",
                        CreateDTTM = DateTime.Now,
                        //Industry = symbolInfoStringList[4],
                        Name = symbolInfoStringList[1]
                    });
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
            return symbolList;

        }

        public void AddMissingSymbols(List<StockInfo> symbolList)
        {
            new SymbolData().AddMissingSymbols(symbolList);
        }
    }
}
