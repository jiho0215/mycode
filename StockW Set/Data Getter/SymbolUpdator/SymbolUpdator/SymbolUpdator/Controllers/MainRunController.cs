﻿using System;
using System.Collections.Generic;
using System.Text;
using SymbolUpdator.Helpers;

namespace SymbolUpdator.Controllers
{
    public class MainRunController
    {
        TimeHelper TimeHelper = new TimeHelper();

        public enum RunEnum { EODStart, IntradayStart, EODEnd, IntradayEnd, SymbolUpdate, FilterList, ValidateList };
        Dictionary<RunEnum, string> NextRunDic = new Dictionary<RunEnum, string>();
        RunEnum NextRun;
        DateTime NextRunDate = DateTime.Now;

        public void InitSettings()
        {
            NextRunDic.Add(RunEnum.SymbolUpdate, SymbolUpdator.Settings.Default.SymbolUpdateTime);
            NextRun = RunEnum.SymbolUpdate;

            //Debug
            //    DateTime now = DateTime.Now;
            //    NextRunDic[RunEnum.SymbolUpdate]ㅊ = (now.AddMinutes(1)).ToString("HH:mm");

        }
        public void UnlimitCircle()
        {
            InitSettings();

            //test
            new SymbolVer2Controller().RefreshSymbolList();
            //test end

            while (true)
            {
                if (IsRuntime(NextRun))
                {
                    RunTrigger(NextRun);
                }
                Console.WriteLine("waiting... " + DateTime.Now);
                TimeHelper.Pause(60000);
            }
        }

        public bool IsRuntime(RunEnum nextRunEnum)
        {
            bool isRunTime = false;
            DateTime runTime = Convert.ToDateTime(NextRunDic[nextRunEnum]);
            DateTime nextDTTM = NextRunDate.Date + runTime.TimeOfDay;
            if (nextDTTM.DayOfWeek == DayOfWeek.Saturday || nextDTTM.DayOfWeek == DayOfWeek.Sunday)
            {
                NextRunDate = GetNextBusinessDay(NextRunDate);
                Console.WriteLine("It's weekend. End Symbol Updater. " + DateTime.Now);
                return isRunTime;
            }
            if (DateTime.Now > nextDTTM)
            {
                isRunTime = true;
            }
            return isRunTime;
        }

        public void RunTrigger(RunEnum nextRunEnum)
        {
            switch (nextRunEnum)
            {
                case RunEnum.SymbolUpdate:
                    if (NextRunDate.DayOfWeek == DayOfWeek.Saturday || NextRunDate.DayOfWeek == DayOfWeek.Sunday)
                    {
                        Console.WriteLine("It's weekend. End Symbol Updater. " + DateTime.Now);
                        return;
                    }
                    Console.WriteLine("* Update Symbol Start " + DateTime.Now);
                    //new SymbolController().RefreshSymbolList();
                    new SymbolVer2Controller().RefreshSymbolList();
                    Console.WriteLine("* Update Symbol End " + DateTime.Now);
                    NextRunDate = GetNextBusinessDay(NextRunDate);
                    break;
                default:
                    break;
            }
        }
        private DateTime GetNextBusinessDay(DateTime dateTime)
        {
            dateTime = dateTime.AddDays(1);
            if (dateTime.DayOfWeek == DayOfWeek.Saturday)
            {
                dateTime = dateTime.AddDays(1);
            }
            if (dateTime.DayOfWeek == DayOfWeek.Sunday)
            {
                dateTime = dateTime.AddDays(1);
            }
            return dateTime;
        }
    }
}
