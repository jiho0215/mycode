﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SymbolUpdator.Models
{
    public class Symbol
    {
        public string SymbolString { get; set; }
        public string SecurityName { get; set; }
        public string MarketCategory { get; set; }
        public string ETF { get; set; }
        public string RoundLotSize { get; set; }
        public string TestIssue { get; set; }
        public string FinancialStatus { get; set; }
        public string CQSSymbol { get; set; }
        

        ///Nasdaq Traded|Symbol|Security Name|Listing Exchange|Market Category|ETF|Round Lot Size|Test Issue|Financial Status|CQS Symbol|NASDAQ Symbol|NextShares
    }
}
