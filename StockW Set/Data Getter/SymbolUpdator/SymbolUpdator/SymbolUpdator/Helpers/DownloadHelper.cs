﻿//using StockW.EntityFramwork.Procedures;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SymbolUpdator.Helpers
{
    public class DownloadHelper
    {
        public int Timeout { get; set; }

        private void DownloadFileDirectFromURL(string url)
        {
            try
            {
                // Start the process with the info we specified.
                // Call WaitForExit and then the using-statement will close.
                using (Process exeProcess = Process.Start(url))
                {
                    exeProcess.WaitForExit();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            Thread.Sleep(10000);
        }
        public void DownloadSymbolListFile(string url, string fullPath, string tmpFileName = "companylist.csv", string tmpDirectory = @"C:\Users\jiho0\Downloads")
        {
            //delete existing tmp file
            if (File.Exists(Path.Combine(tmpDirectory, tmpFileName)))
            {
                try
                {
                    File.Delete(Path.Combine(tmpDirectory, tmpFileName));
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
            //Download File in Tmp Path.
            DownloadFileDirectFromURL(url);
            //Move the file
            try
            {
                File.Move(Path.Combine(tmpDirectory, tmpFileName), fullPath);
            }
            catch (Exception e)
            {
                //new InsertError().InsertErrorMsg("", e.Message);
            }
        }
    }
}
