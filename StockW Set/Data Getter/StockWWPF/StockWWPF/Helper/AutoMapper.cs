﻿using AutoMapper;
using StockWWPF.AVAPISvc;
using StockWWPF.Model;
using StockWWPF.Model.StockWSvc;
using StockWWPF.StockWSvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockWWPF.Helper
{
    public class AutoMapper
    {
        private static MapperConfiguration MapperConfiguration = new MapperConfiguration(cfg =>
        {
            cfg.CreateMap<GetDailyResponse, GetDailyResponseModel>();
            cfg.CreateMap<GetDailyRequestModel, GetDailyRequest>();
            cfg.CreateMap<AVAPISvc.EODLog, EODLog>();
            cfg.CreateMap<GetIntradayResponse, GetIntradayResponseModel>();
            cfg.CreateMap<GetIntradayRequestModel, GetIntradayRequest>();
            //cfg.CreateMap<AVAPISvc.IntraDayLog, IntraDayLog>();
            cfg.CreateMap<InsertDailyLogRequestModel, InsertDailyLogRequest>();
            cfg.CreateMap<InsertDailyLogResponse, InsertDailyLogResponseModel>();
            cfg.CreateMap<EODLog, StockWSvc.EODLog>();
            cfg.CreateMap<GetAllSymbolsResponse, GetAllSymbolsResponseModel>();
        });

        public IMapper IMapper = MapperConfiguration.CreateMapper();

    }
}
