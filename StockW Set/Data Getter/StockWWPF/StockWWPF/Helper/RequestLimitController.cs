﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockWWPF.Helper
{
    public class RequestLimitController
    {
        public DateTime? StartTime { get; set; }
        public int LimitPerMinute { get; set; }
        public int CurrentCount { get; set; }
        public bool GetPermission(DateTime now)
        {
            if (StartTime == null || (now - (DateTime)StartTime).Seconds >= 60)
            {
                StartTime = now;
                CurrentCount = 1;
                return true;
            }
            if(CurrentCount < LimitPerMinute)
            {
                CurrentCount++;
                return true;
            }
            return false;
        }
    }
}
