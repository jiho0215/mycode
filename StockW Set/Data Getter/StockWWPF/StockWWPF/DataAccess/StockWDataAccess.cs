﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StockWWPF.Model.StockWSvc;
using StockWWPF.StockWSvc;

namespace StockWWPF.DataAccess
{
    public class StockWDataAccess
    {
        private readonly StockWSvcClient Client = new StockWSvcClient();
        private Helper.AutoMapper Mapper = new Helper.AutoMapper();

        public InsertDailyLogResponseModel InsertDailyLog(InsertDailyLogRequestModel request)
        {
            var response = Client.InsertDailyLog(Mapper.IMapper.Map<InsertDailyLogRequest>(request));
            return Mapper.IMapper.Map<InsertDailyLogResponseModel>(response);
        }
        public GetAllSymbolsResponseModel GetAllSymbols()
        {
            return Mapper.IMapper.Map<GetAllSymbolsResponseModel>(Client.GetAllSymbols());
        }
    }
}
