﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using StockWWPF.AVAPISvc;
using StockWWPF.Model;
using StockWWPF.Helper;

namespace StockWWPF.DataAccess
{
    public class AVAPIDataAccess
    {
        private readonly AlphaVentageAPISvcClient Client = new AlphaVentageAPISvcClient();
        private Helper.AutoMapper Mapper = new Helper.AutoMapper();
        public GetDailyResponseModel GetEODCompact(GetDailyRequestModel request)
        {
            var response = Client.GetEODCompact(Mapper.IMapper.Map<GetDailyRequest>(request));
            return Mapper.IMapper.Map<GetDailyResponseModel>(response);
        }

        public GetDailyResponseModel GetEODFull(GetDailyRequestModel request)
        {
            var response = Client.GetEODCompact(Mapper.IMapper.Map<GetDailyRequest>(request));
            return Mapper.IMapper.Map<GetDailyResponseModel>(response);
        }

        //public GetIntradayResponse GetIntraday(GetIntradayRequest request)
        //{
        //    var response = Client.GetIntraday(request, (Const_TIME_SERIES_INTRADAY.TIME_SERIES_INTRADAY_interval) interval);
        //    return response;
        //}
    }
}
