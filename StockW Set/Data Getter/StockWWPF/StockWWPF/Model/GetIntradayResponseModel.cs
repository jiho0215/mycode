﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockWWPF.Model
{
    public class GetIntradayResponseModel
    {
        public string Symbol { get; set; }
        public List<IntraDayLog> IntradayLogList { get; set; }
        public bool IsSucess { get; set; }
        public string ErrorMsg { get; set; }
        public string WarningMsg { get; set; }
    }
}
