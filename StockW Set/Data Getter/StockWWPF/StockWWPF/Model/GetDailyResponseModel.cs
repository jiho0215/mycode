﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockWWPF.Model
{
    public class GetDailyResponseModel
    {
        public string Symbol { get; set; }
        public string RequestURL { get; set; }
        public List<EODLog> EODLogList { get; set; }
        public bool IsSucess { get; set; }
        public string ErrorMsg { get; set; }
        public string WarningMsg { get; set; }
    }
}
