﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockWWPF.Model.StockWSvc
{
    public class InsertDailyLogResponseModel
    {
        public bool IsSucess { get; set; }

        public List<string> ErrorMsgList { get; set; }

        public List<string> WarningMsgList { get; set; }
    }
}
