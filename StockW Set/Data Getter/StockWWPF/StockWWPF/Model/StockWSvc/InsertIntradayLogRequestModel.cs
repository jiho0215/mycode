﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockWWPF.Model.StockWSvc
{
    public class InsertIntradayLogRequestModel
    {
        public List<IntraDayLog> IntradayLogList { get; set; }
    }
}
