﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockWWPF.Model.StockWSvc
{
    public class InsertDailyLogRequestModel
    {
        public List<EODLog> EODLogList { get; set; }
    }
}
