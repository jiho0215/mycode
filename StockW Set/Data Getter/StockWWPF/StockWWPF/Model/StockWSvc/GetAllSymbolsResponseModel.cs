﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockWWPF.Model.StockWSvc
{
    public class GetAllSymbolsResponseModel
    {
        public List<string> Symbols { get; set; }
        public bool IsSucess { get; set; }
        public List<string> ErrorMsgList { get; set; }
        public List<string> WarningMsgList { get; set; }
    }
}
