﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using StockWWPF.Controller;
using StockWWPF.Helper;

namespace StockWWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly DailyLogController DailyLogController = new DailyLogController();
        private bool isManualEndClicked = false;
        private bool isAutoEndClicked = false;
        public MainWindow()
        {
            InitializeComponent();
            //DailyLogController.ProcessSingleDailyRequestOnPermit("USB-A");
        }

        private async void Manual_Start_Click(object sender, RoutedEventArgs e)
        {
            isManualEndClicked = false;
            DisableAllButtons();
            Manual_End.IsEnabled = true;
            await Task.Run(() => ManualRun());
            EnableAllButtons();
        }
        private void Manual_End_Click(object sender, RoutedEventArgs e)
        {
            isManualEndClicked = true;
            EnableAllButtons();
        }
        private void Auto_Start_Click(object sender, RoutedEventArgs e)
        {
            isAutoEndClicked = false;
            DisableAllButtons();
            Auto_Start.IsEnabled = true;
            //AutoRun();
        }
        private void Auto_End_Click(object sender, RoutedEventArgs e)
        {
            isAutoEndClicked = true;
            EnableAllButtons();
        }
        private void AddLog(string log)
        {
            this.Dispatcher.Invoke(() =>
            {
                LogTextBox.AppendText(log + "\r");
                LogTextBox.ScrollToEnd();
            });
        }
        private void DisableAllButtons()
        {
            Manual_Start.IsEnabled = false;
            Manual_End.IsEnabled = false;
            Auto_Start.IsEnabled = false;
            Auto_End.IsEnabled = false;
        }
        private void EnableAllButtons()
        {
            Manual_Start.IsEnabled = true;
            Manual_End.IsEnabled = false;
            Auto_Start.IsEnabled = true;
            Auto_End.IsEnabled = false;
        }

        //private string ProcessSingleDailyRequest(string symbol, bool isFull = false)
        //{
        //    return DailyLogController.ProcessSingleDailyRequest(symbol, isFull);
        //}

        private void AutoRun()
        {
            //ProcessSingleDailyRequest()
        }
        private void ManualRun()
        {
            DailyLogController.FeedSymbolQueue();
            DailyLogController.SetSymbolStartPoint("RSXJ");
            var symbol = DailyLogController.DequeueSymbol();

            while (!string.IsNullOrWhiteSpace(symbol) && !isManualEndClicked)
            {
                var log = DailyLogController.ProcessSingleDailyRequestOnPermit(symbol);
                AddLog(log);
                symbol = DailyLogController.DequeueSymbol();
            }
        }
    }
}
