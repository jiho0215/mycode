﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StockWWPF.DataAccess;
using StockWWPF.Helper;
using StockWWPF.Model;
using StockWWPF.Model.StockWSvc;

namespace StockWWPF.Controller
{
    public class DailyLogController
    {
        private readonly AVAPIDataAccess AVAPIDataAccess = new AVAPIDataAccess();
        private readonly StockWDataAccess StockWDataAccess = new StockWDataAccess();
        private Queue<string> SymbolQueue = new Queue<string>();
        private RequestLimitController limitController = new RequestLimitController()
        {
            LimitPerMinute = 30
        };

        public GetDailyResponseModel GetDailyLogCompact(string symbol)
        {
            var request = new GetDailyRequestModel()
            {
                Symbol = symbol
            };

            return AVAPIDataAccess.GetEODCompact(request);
        }

        public GetDailyResponseModel GetDailyLogFull(string symbol)
        {
            var request = new GetDailyRequestModel()
            {
                Symbol = symbol
            };

            return AVAPIDataAccess.GetEODFull(request);
        }

        public GetAllSymbolsResponseModel GetAllSymbols()
        {
            return StockWDataAccess.GetAllSymbols();
        }

        public void FeedSymbolQueue()
        {
            var allSymbolsResponse = StockWDataAccess.GetAllSymbols();
            if (allSymbolsResponse.IsSucess && allSymbolsResponse.Symbols.Count() > 0)
            {
                SymbolQueue = new Queue<string>(allSymbolsResponse.Symbols);
            }
        }

        public string DequeueSymbol()
        {
            if (SymbolQueue.Count == 0)
                return null;
            return SymbolQueue.Dequeue();
        }

        public void SetSymbolStartPoint(string targetSymbol)
        {
            var symbol = DequeueSymbol();
            if (string.IsNullOrEmpty(symbol))
                return;
            while(!symbol.ToUpper().Equals(targetSymbol.ToUpper()))
            {
                symbol = DequeueSymbol();
                if (string.IsNullOrEmpty(symbol))
                    return;
            }

            SymbolQueue.Enqueue(symbol);
        }

        public string ProcessSingleDailyRequest(string symbol, bool isFull)
        {
            var startTime = DateTime.Now;
            var isSuccess = true;
            var errorMsg = "";
            var data = (isFull) ? GetDailyLogFull(symbol) : GetDailyLogCompact(symbol);
            if (data.IsSucess)
            {
                var insertResponse = StockWDataAccess.InsertDailyLog(new InsertDailyLogRequestModel()
                {
                    EODLogList = data.EODLogList
                });
                if (!insertResponse.IsSucess)
                {
                    isSuccess = false;
                    errorMsg = insertResponse.ErrorMsgList.ToString() + " " + insertResponse.WarningMsgList.ToString();
                }
            }
            else
            {
                isSuccess = false;
                errorMsg = data.ErrorMsg+ " " + data.WarningMsg;
            }
            var log = "Processed in " + Math.Round((double)(DateTime.Now - startTime).Milliseconds/1000, 1).ToString()
                + "sec Symbol: " + symbol + " Status: " + ((isSuccess) ? " Success" : "Failed " + errorMsg + "\r" + data.RequestURL);
            return log;
        }

        public string ProcessSingleDailyRequestOnPermit(string symbol, bool isFull = false)
        {
            var log = "";
            var waitSec = 0;
            while (limitController.GetPermission(DateTime.Now))
            {
                Task.Delay(1);
                waitSec++;
            }
            waitSec = 0;
            if(waitSec > 0)
            {
                log = "Waited " + waitSec + ". ";
            }
            log += ProcessSingleDailyRequest(symbol, isFull);
            return log;
        }
    }
}
