﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace EmployeeSorting
{
    class Employee : IComparable
    {
        public string ID { get; set; }
        public int Age { get; set; }
        public int Rank { get; set; }
        public double Salary { get; set; }

        int IComparable.CompareTo(object obj)
        {
            Employee emp = (Employee)obj;
            return String.Compare(this.ID, emp.ID);
        }

        public static IComparer sortAgeAscending()
        {
            return (IComparer)new SortAgeAscendingHelper();
        }
        public static IComparer sortRankDescending()
        {
            return (IComparer)new SortRankDescendingHelper();
        }
        public static IComparer sortSalaryAscending()
        {
            return (IComparer)new SortSalaryAscendingHelper();
        }
        public static IComparer sortSalaryDescending()
        {
            return (IComparer)new SortSalaryDescendingHelper();
        }
        
        class SortAgeAscendingHelper : IComparer
        {
            int IComparer.Compare(object x, object y)
            {
                Employee emp1 = (Employee)x;
                Employee emp2 = (Employee)y;

                if (emp1.Age > emp2.Age)
                    return 1;
                if (emp1.Age < emp2.Age)
                    return -1;
                else return 0;
            }
        }
        class SortRankDescendingHelper : IComparer
        {
            int IComparer.Compare(object x, object y)
            {
                Employee emp1 = (Employee)x;
                Employee emp2 = (Employee)y;

                if (emp1.Rank < emp2.Rank)
                    return 1;
                if (emp1.Rank > emp2.Rank)
                    return -1;
                else return 0;
            }
        }
        class SortSalaryAscendingHelper : IComparer
        {
            int IComparer.Compare(object x, object y)
            {
                Employee emp1 = (Employee)x;
                Employee emp2 = (Employee)y;

                if (emp1.Salary > emp2.Salary)
                    return 1;
                if (emp1.Salary < emp2.Salary)
                    return -1;
                else return 0;
            }
        }
        class SortSalaryDescendingHelper : IComparer
        {
            int IComparer.Compare(object x, object y)
            {
                Employee emp1 = (Employee)x;
                Employee emp2 = (Employee)y;

                if (emp1.Salary < emp2.Salary)
                    return 1;
                if (emp1.Salary > emp2.Salary)
                    return -1;
                else return 0;
            }
        }
    }
}
