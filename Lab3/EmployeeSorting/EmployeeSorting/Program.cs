﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeSorting
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rnd = new Random();
            Employee[] employees = new Employee[10];
            
            for (int ctr = 0; ctr < employees.Length; ctr++)
            {
                Employee em = new Employee();
                em.Age = rnd.Next(20, 80);
                em.ID = rnd.Next(1000, 9999).ToString();
                em.Rank = rnd.Next(1, 10);
                em.Salary = Math.Round((rnd.Next(50000,300000) + rnd.NextDouble()),2);
                employees[ctr] = em;
            }

            Console.WriteLine("Original");
            foreach (Employee em in employees)
            {
                Console.WriteLine("ID: " + em.ID + ", Age: " + em.Age + ", Rank: " + em.Rank + ", Salary: " + em.Salary);
            }

            //Array.Sort(employees);
            //Array.Sort(employees, Employee.sortAgeAscending());
            //Array.Sort(employees, Employee.sortRankDescending());
            Array.Sort(employees, Employee.sortSalaryAscending());
            //Array.Sort(employees, Employee.sortSalaryDescending());
            Console.WriteLine("Sorted");
            foreach (Employee em in employees)
            {
                Console.WriteLine("ID: " + em.ID + ", Age: " + em.Age + ", Rank: " + em.Rank + ", Salary: " + em.Salary);
            }

            Console.ReadKey();
        }

    }
}
