﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SinglyLinkedListTester
{
    class Employee
    {
        public string ID { get; set; }
        public int Age { get; set; }
        public double Salary { get; set; }
        public Employee() { }
        public Employee(string id, int age, double salary)
        {
            ID = id;
            Age = age;
            Salary = salary;
        }
    }
}
