﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SinglyLinkedList;

namespace SinglyLinkedListTester
{
    class Program
    {
        static void Main(string[] args)
        {
            new Program().Run();
        }
        public void Run()
        {
            SLList<Employee> emList = new SLList<Employee>();
            Random rnd = new Random();
            int EmpNum = 10;
            Node<Employee> node = new Node<Employee>();

            Console.WriteLine("Using AddAfter on Node == null.");
            try
            {
                emList.AddAfter(node, new Node<Employee>(new Employee("Volunteer1", 30, 0)));
            }
            catch (ArgumentNullException e)
            {
                Console.WriteLine("Error: " + e.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
            Console.WriteLine();

            Console.WriteLine("Create {0} Employees.", EmpNum);
            for (int i = 1; i <= EmpNum; i++)
            {
                emList.Add(new Node<Employee>(new Employee(i.ToString(), rnd.Next(20, 100), Math.Round((rnd.Next(40000, 200000) + rnd.NextDouble()), 2))));
            }
            Display(emList);

            //Console.WriteLine("9th node's Index is : " + emList.IndexOf(emList.GetNode(9)));
            Console.WriteLine();

            Console.WriteLine("Add an Employee(Volunteer2) after 0th node");
            emList.AddAfter(emList.GetNode(0), new Node<Employee>(new Employee("Volunteer2", 20, 222)));
            Display(emList);

            Console.WriteLine("Add an Employee(Volunteer3) in middle of the list : 5th node out of {0} nodes", emList.Count());
            emList.AddAfter(emList.GetNode(4), new Node<Employee>(new Employee("Volunteer3", 30, 333)));
            Display(emList);

            Console.WriteLine("Add an Employee(Volunteer3) at last of the list");
            emList.AddAfter(emList.GetNode(emList.Count() - 1), new Node<Employee>(new Employee("Volunteer4", 40, 444)));
            Display(emList);

            Console.WriteLine("Use an Identical foreign Node for MyList.AddAfter()");
            node = new Node<Employee>(new Employee("Volunteer3", 30, 333));
            node.next = new Node<Employee>(new Employee("Volunteer4", 40, 444));

            try
            {
                emList.AddAfter(node, new Node<Employee>(new Employee("extra", 50, 555)));
            }catch(Exception e)
            {
                Console.WriteLine("Error: " + e.Message);
            }

            Console.WriteLine();
            Console.WriteLine("Try Remove using out of range index");
            try
            {
                emList.Remove(emList.GetNode(3));
            }
            catch (IndexOutOfRangeException e)
            {
                Console.WriteLine("Error: " + e.Message);
            }
            Console.WriteLine();
            Display(emList);
            
            Console.WriteLine("Remove All");
            emList.RemoveAll();
            Display(emList);
            
            Console.WriteLine("Add Three new nodes");
            emList.Add(new Node<Employee>(new Employee("One", 30, 0)));
            Display(emList);
            emList.AddAfter(emList.GetNode(0), new Node<Employee>(new Employee("Two", 30, 0)));
            Display(emList);
            emList.Add(new Node<Employee>(new Employee("Three", 30, 0)));
            Display(emList);

            SLList<Employee> emListTwo = new SLList<Employee>();
            emListTwo.Add(new Node<Employee>(new Employee("One", 10, 111)));
            emListTwo.Add(new Node<Employee>(new Employee("Two", 20, 222)));
            emListTwo.Add(new Node<Employee>(new Employee("Three", 30, 333)));
            emListTwo.Add(new Node<Employee>(new Employee("Four", 40, 444)));

            Node<Employee> extraNode = new Node<Employee>(new Employee("Extra", 1000, 10000));
            extraNode.next = emListTwo.GetNode(1);
            Console.WriteLine("Check Circular!!! ");
            emListTwo.Add(extraNode);

            if (emListTwo.IsCircularList())
            {
                Console.WriteLine("This list is circular. Can not display.");
                
            }
            else
            {
                Console.WriteLine("Not a circular list.");
                Display(emListTwo);
            }

            Console.ReadKey();
        }
        public void Display(SLList<Employee> list)
        {
            Console.WriteLine("Total Count is {0}", list.Count());
            for (int i = 0; i < list.Count(); i++)
            {
                Console.WriteLine("#{0} employee's ID: {1}, Age:{2}, Salary:{3}", i, list.GetNode(i).data.ID, list.GetNode(i).data.Age, list.GetNode(i).data.Salary);
            }
            Console.WriteLine();
        }
    }
}
