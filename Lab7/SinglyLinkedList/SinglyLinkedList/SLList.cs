﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SinglyLinkedList
{
    public class SLList<T>
    {
        private Node<T> head;
        public int Count()
        {
            if (this.head == null) return 0;
            int count = 1;
            Node<T> curr = head;
            while (curr.next != null)
            {
                curr = curr.next;
                count++;
            }
            return count;
        }
        public void Add(Node<T> input)
        {
            if (this.head == null) { this.head = input; }
            else { GetNode(this.Count() - 1).next = input; }
        }
        public void AddAfter(Node<T> target, Node<T> input)
        {
            input.next = target.next;
            target.next = input;
        }
        public void Remove(Node<T> node)
        {
            int index = IndexOf(node);
            GetNode(index - 1).next = GetNode(index + 1);
        }
        public void RemoveAll()
        {
            head = null;
        }
        public Node<T> GetNode(int index)
        {
            OutofIndexThrower(index);
            Node<T> curr = head;
            for(int i = 0; i < index; i++)
            {
                curr = curr.next;
            }
            return curr;
        }
        private void OutofIndexThrower(int index)
        {
            if (index < 0 || index > this.Count() - 1) throw new IndexOutOfRangeException();
        }
        private int IndexOf(Node<T> input)
        {
            int index = 0;
            Node<T> curr = head;

            while (!curr.Equals(input))
            {
                if (curr.next == null) throw new Exception("No matched data found.");
                curr = curr.next;
                index++;
            }
            return index;
        }
        public Boolean IsCircularList()
        {
            if (head.next == null) return false;
            Node<T> curr1 = head;
            Node<T> curr2 = head.next;
            while (curr2.next != null)
            {
                if (curr1.Equals(curr2)) return true;
                if (curr2.next.next == null) return false;
                curr1 = curr1.next;
                curr2 = curr2.next.next;
            }
            return false;
        }
    }
}
