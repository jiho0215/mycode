﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SinglyLinkedList
{
    public class Node<T>
    {
        public Node<T> next = null;
        public T data = default(T);
        public Node() { }
        public Node(T input)
        {
            data = input;
        }
    }
}
