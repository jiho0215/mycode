﻿using System;
using System.Collections.Generic;
using System.Text;
using SymbolRefresher.Helper;


namespace SymbolRefresher.Controller
{
    public class MainRunController
    {
        TimeHelper TimeHelper = new TimeHelper();

        public enum RunEnum { EODStart, IntradayStart, EODEnd, IntradayEnd, SymbolUpdate, FilterList, ValidateList };
        Dictionary<RunEnum, string> NextRunDic = new Dictionary<RunEnum, string>();
        RunEnum NextRun;
        DateTime NextRunDate = DateTime.Now;

        public void InitSettings()
        {
            NextRunDic.Add(RunEnum.SymbolUpdate, SymbolRefresher.Settings.Default.SymbolUpdateTime);
            NextRun = RunEnum.SymbolUpdate;

            //    DateTime now = DateTime.Now;
            //    NextRunDic[RunEnum.SymbolUpdate]ㅊ = (now.AddMinutes(1)).ToString("HH:mm");

        }
        public void UnlimitCircle()
        {
            InitSettings();
            while (true)
            {
                if (IsRuntime(NextRun))
                {
                    RunTrigger(NextRun);
                }
                TimeHelper.Pause(60000);
            }
        }

        public bool IsRuntime(RunEnum nextRunEnum)
        {
            bool isRunTime = false;
            DateTime runTime = Convert.ToDateTime(NextRunDic[nextRunEnum]);
            DateTime nextDTTM = NextRunDate.Date + runTime.TimeOfDay;
            if (DateTime.Now > nextDTTM)
            {
                isRunTime = true;
            }
            return isRunTime;
        }

        public void RunTrigger(RunEnum nextRunEnum)
        {
            switch (nextRunEnum)
            {
                case RunEnum.SymbolUpdate:
                    Console.WriteLine("* Update Symbol Start " + DateTime.Now);
                    //new SymbolController().RefreshSymbolList();
                    Console.WriteLine("* Update Symbol End " + DateTime.Now);
                    NextRunDate = NextRunDate.AddDays(1);
                    break;
                default:
                    break;
            }
        }
    }
}
