﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Diagnostics;

namespace Copy_Paste
{
    internal partial class ProgressBar : Form
    {
        private string sourceFolder;
        private string targetFolder;
        public long TotalFileSize { get; set; }
        public long CopiedFilesSize { get; set; }
        Thread ThreadCopyItem;
        ManualResetEvent mrse = new ManualResetEvent(true);
        private long TotalCopiedFilesSize = 0L;
        int totalProgress = 0;
        int partialProgress = 0;
        List<Item> items = new List<Item>();
        Form1 f1;
        public delegate void CopyTotallyCompleted();
        public Boolean stopThread = false;


        public ProgressBar(string sourceFolder, string targetFolder, List<ListViewItem> droppedItems, Form1 f1)
        {
            InitializeComponent();
            this.sourceFolder = sourceFolder;
            this.targetFolder = targetFolder;
            this.f1 = f1;
            ThreadCopyItem = new Thread(() => CopyItems(droppedItems));
            ThreadCopyItem.Start();
        }

        /// <summary>
        /// Clone listviewitem and copy actual file. This will be invoked in a worker thread.
        /// </summary>
        /// <param name="droppedItems"></param>
        private void CopyItems(List<ListViewItem> droppedItems)
        {
            TotalFileSize = 0L;
            CopiedFilesSize = 0L;
            int index = 0;
            foreach (ListViewItem LVI in droppedItems)
            {
                Item tempName = new Item(sourceFolder, targetFolder, LVI.Text);
                items.Add(tempName);
                TotalFileSize += tempName.GetFileSize();

                if (!tempName.CheckSourceFileExistance())
                {
                    stopThread = true;
                    ClosingMessageBox("Source file does not exist. Please check your file.", "No File Exist");
                    return;
                }
                if (tempName.CheckTargetFileExistance())
                {
                    stopThread = true;
                    ClosingMessageBox("File already exist.", "File Exist");
                    return;
                }
            }
            while (stopThread == false && index < droppedItems.Count)
            {
                if (f1.InvokeRequired)
                {
                    f1.listViewTarget.BeginInvoke(new Action(() => f1.UpdateTargetListView(droppedItems[index])));
                }
                else
                {
                    f1.UpdateTargetListView(droppedItems[index]);
                }
                items[index].CopyPasteFile(this);
                CopiedFilesSize += items[index].GetFileSize();
                Thread.Sleep(500);
                mrse.WaitOne();
                index++;
            }
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new CopyTotallyCompleted(SetCompleted));
            }
            else
            {
                SetCompleted();
            }
        }

        private void btnPause_Click(object sender, EventArgs e)
        {
            switch (btnPause.Text)
            {
                case "Pause":
                    btnPause.Text = "Resume";
                    PauseThread();
                    break;

                case "Resume":
                    btnPause.Text = "Pause";
                    ResumeThread();
                    break;

                default:
                    break;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            switch (btnCancel.Text)
            {
                case "Cancel":
                    PauseThread();
                    if (MessageBox.Show("Do you want to cancel?","Cancel", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        TerminateThread();
                    } 
                    else
                    {
                        ResumeThread();
                    }
                    break;
                case "Close":
                    Close();
                    break;
                default:
                    break;
            }
        }

        public void UpdateProgress(string fileName, long copiedSize, long fileSize)
        {
            lblFileName.Text = fileName;
            TotalCopiedFilesSize = CopiedFilesSize + copiedSize;

            partialProgress = (int)Math.Round(((decimal)copiedSize / (decimal)fileSize) * 100);
            totalProgress = (int)Math.Round((((decimal)TotalCopiedFilesSize)/ (decimal)TotalFileSize) * 100);

            lblProgress.Text = totalProgress.ToString() + "% Completed";
            progressBar1.Value = partialProgress;
            progressBar2.Value = totalProgress;
        }

        public void ClosingMessageBox(string msg, string title)
        {
            DialogResult response = MessageBox.Show(msg,title,MessageBoxButtons.OK);
            if (response == DialogResult.OK & stopThread == true)
            {
                if (this.InvokeRequired)
                {
                    this.BeginInvoke(new CopyTotallyCompleted(Close));
                }
                else
                {
                    Close();
                }
            }
        }

        public void SetCompleted()
        {
            btnPause.Visible = false;
            btnCancel.Text = "Close";
        }
        public void PauseThread()
        {
            mrse.Reset();
        }
        public void ResumeThread()
        {
            mrse.Set();
        }
        public void TerminateThread()
        {
            stopThread = true;
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new CopyTotallyCompleted(Close));
            }
            else
            {
                Close();
            }
        }
    }
}