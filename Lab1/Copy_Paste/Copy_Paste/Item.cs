﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;
using System.Threading;

namespace Copy_Paste
{
    class Item
    {
        public string SourceFolder { get; set; }
        public string FileName { get; set; }
        public string TargetFolder { get; set; }
        long fileSize = 0L;
        string sourceFilePath = string.Empty;
        string targetFilePath = string.Empty;

        public Item(string sourceFolder, string targetFolder, string fileName)
        {
            this.SourceFolder = sourceFolder;
            this.TargetFolder = targetFolder;
            this.FileName = fileName;

            sourceFilePath = Path.Combine(SourceFolder, Path.GetFileName(FileName));
            targetFilePath = Path.Combine(TargetFolder, Path.GetFileName(FileName));
        }

        public long GetFileSize()
        {
            using (Stream inStream = File.Open(sourceFilePath, FileMode.Open))
            {
                fileSize = inStream.Length;
            }
                return fileSize;
        }
        internal Boolean CheckSourceFileExistance()
        {
            return File.Exists(sourceFilePath) ? true : false;
        }
        internal Boolean CheckTargetFileExistance()
        {
            return File.Exists(targetFilePath) ? true : false;
        }
        public Boolean CopyPasteFile(ProgressBar PB1)
        {
            if (CheckSourceFileExistance()&!CheckTargetFileExistance())
            {
                try
                {
                    using (Stream inStream = File.Open(sourceFilePath, FileMode.Open))
                    {
                        byte[] buffer = new byte[1024];
                        int bytesProcessed = 0;
                        int bytesRead = 0;
                        using (Stream outStream = File.Create(targetFilePath))
                        {
                            do
                            {
                                bytesRead = inStream.Read(buffer, 0, buffer.Length);
                                outStream.Write(buffer, 0, bytesRead);
                                bytesProcessed += bytesRead;
                                PB1.Invoke(new Action(() => PB1.UpdateProgress(FileName, inStream.Position, inStream.Length)));
                            }
                            while (bytesRead > 0);
                        }
                    }
                return true;
                }
                catch
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

    }
}
