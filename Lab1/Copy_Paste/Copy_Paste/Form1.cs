﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;


namespace Copy_Paste
{
    public delegate void AddListViewItemDelegate(ListViewItem LVI);
    /// <summary>
    /// This static class is for access to main form from another form
    /// </summary>

    public partial class Form1 : Form
    {
        private string sourceFolder = "";
        private string targetFolder = "";

        public Form1()
        {
            InitializeComponent();
            //UpdateMainForm.OnCopiedFile += UpdateMainForm_OnCopiedFile;
        }

        private void BtnSource_Click(object sender, EventArgs e)
        {
            try
            {
                var FD = new System.Windows.Forms.FolderBrowserDialog();
                if (FD.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    sourceFolder = FD.SelectedPath.ToString();
                    txtSource.Text = sourceFolder;
                    openDirectory(txtSource.Text, listViewSource);
                }
            }
            catch (Exception ex) { }
        }
        private void BtnTarget_Click(object sender, EventArgs e)
        {
            try
            {
                var FD = new System.Windows.Forms.FolderBrowserDialog();
                if (FD.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    targetFolder = FD.SelectedPath.ToString();
                    txtTarget.Text = targetFolder;
                    openDirectory(txtTarget.Text, listViewTarget);
                }
            }
            catch (Exception ex) { }
        }
        
        private void txtSource_KeyUp(object sender, KeyEventArgs e)
        {
            switch(e.KeyCode)
            {
                case Keys.Enter|Keys.Tab:
                    TextBox textBox = sender as TextBox;
                    if (CheckValidDirectory(textBox.Text))
                    {
                        openDirectory(textBox.Text, listViewSource);
                    }
                    //Doesn't work
                    e.Handled = e.SuppressKeyPress = true; 
                    break;
                default:
                    break;
            }
        }
        private void txtTarget_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                TextBox textBox = sender as TextBox;
                if (CheckValidDirectory(textBox.Text))
                {
                    openDirectory(textBox.Text, listViewTarget);
                }
            }
        }
        private void txtSource_TextChanged(object sender, EventArgs e)
        {
            this.sourceFolder = txtSource.Text;
        }
        private void txtTarget_TextChanged(object sender, EventArgs e)
        {
            this.targetFolder = txtTarget.Text;
        }

        private Boolean CheckValidDirectory(string folderPath)
        {
            Boolean isValid = false;
            if(folderPath == "")
            {
                MessageBox.Show("Please input a directory.");
                return false;
            }
            else if (System.IO.Directory.Exists(folderPath))
            {
                isValid = true;
            }
            else
            {
                isValid = false;
                MessageBox.Show(this, "Wrong Path: " + folderPath + "\nPlease input a valid directory.");
            }
            return isValid;
        }
        private void openDirectory(string folderPath, ListView LV)
        {
            LV.Clear();
            try
            {
                DirectoryInfo DI = new DirectoryInfo(folderPath);

                ImageList iconImageList = new ImageList();
                iconImageList.Images.Add(Properties.Resources.videoplay32);
                LV.LargeImageList = iconImageList;

                DirectoryInfo[] dirs = DI.GetDirectories();
                var filteredDirectory = dirs.Where(f => !f.Attributes.HasFlag(FileAttributes.Hidden));
                foreach (var f in filteredDirectory)
                {
                    LV.Items.Add(f.Name,0);
                }

                FileInfo[] files = DI.GetFiles();
                var filteredFiles = files.Where(f => !f.Attributes.HasFlag(FileAttributes.Hidden));
                foreach (var f in filteredFiles)
                {
                    LV.Items.Add(f.Name, 0);
                }   
            }
            catch (Exception ex) { }
        }

        private void listViewSource_ItemDrag(object sender, ItemDragEventArgs e)
        {
            if(listViewSource.SelectedIndices.Count > 0)
            {
                List<ListViewItem> LVI = new List<ListViewItem>();
                foreach (int index in listViewSource.SelectedIndices)
                {
                    LVI.Add(listViewSource.Items[index]);
                }
                listViewSource.DoDragDrop(LVI, DragDropEffects.Copy | DragDropEffects.Move);
            }
        }
        private void listViewTarget_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof(List<ListViewItem>)))
            {
                e.Effect = DragDropEffects.Copy;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }
        private void listViewTarget_DragDrop(object sender, DragEventArgs e)
        {
            List<ListViewItem> droppedItems = new List<ListViewItem>();
            droppedItems = e.Data.GetData(typeof(List<ListViewItem>)) as List<ListViewItem>;
            ProgressBar PB = new ProgressBar(sourceFolder,targetFolder,droppedItems, this);
            PB.Show();
        }

        internal void UpdateTargetListView(ListViewItem LVI)
        {
            listViewTarget.Items.Add((ListViewItem)LVI.Clone());
        }
        //public void UpdateMainForm_OnCopiedFile (ListViewItem LVI)
        //{
        //    Debug.WriteLine("Method OnCopiedFile");
        //}
        public void CloneListViewItem(ListViewItem LVI)
        {
            listViewTarget.Items.Add((ListViewItem)LVI.Clone());
        }
    }
    //public static class UpdateMainForm
    //{
    //    public static Form1 mainwin;
    //    public static event AddListViewItemDelegate OnCopiedFile;
    //    public static void CloneItem(ListViewItem LVI)
    //    {
    //        ThreadSafeCloneItem(LVI);
    //    }
    //    private static void ThreadSafeCloneItem(ListViewItem LVI)
    //    {
    //        if (mainwin != null && mainwin.InvokeRequired)
    //        {
    //            mainwin.Invoke(new AddListViewItemDelegate(ThreadSafeCloneItem), new object[] { LVI });
    //        }
    //        else
    //        {
    //            OnCopiedFile(LVI);
    //        }
    //    }
    //}
}
