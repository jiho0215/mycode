﻿namespace Copy_Paste
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtSource = new System.Windows.Forms.TextBox();
            this.txtTarget = new System.Windows.Forms.TextBox();
            this.BtnSource = new System.Windows.Forms.Button();
            this.BtnTarget = new System.Windows.Forms.Button();
            this.listViewSource = new System.Windows.Forms.ListView();
            this.listViewTarget = new System.Windows.Forms.ListView();
            this.pbDrag = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbDrag)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Source";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(423, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Target";
            // 
            // txtSource
            // 
            this.txtSource.Location = new System.Drawing.Point(59, 17);
            this.txtSource.Name = "txtSource";
            this.txtSource.Size = new System.Drawing.Size(191, 20);
            this.txtSource.TabIndex = 2;
            this.txtSource.TextChanged += new System.EventHandler(this.txtSource_TextChanged);
            this.txtSource.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtSource_KeyUp);
            // 
            // txtTarget
            // 
            this.txtTarget.Location = new System.Drawing.Point(467, 20);
            this.txtTarget.Name = "txtTarget";
            this.txtTarget.Size = new System.Drawing.Size(191, 20);
            this.txtTarget.TabIndex = 3;
            this.txtTarget.TextChanged += new System.EventHandler(this.txtTarget_TextChanged);
            this.txtTarget.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtTarget_KeyUp);
            // 
            // BtnSource
            // 
            this.BtnSource.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.BtnSource.Location = new System.Drawing.Point(256, 14);
            this.BtnSource.Name = "BtnSource";
            this.BtnSource.Size = new System.Drawing.Size(27, 25);
            this.BtnSource.TabIndex = 4;
            this.BtnSource.Text = "...";
            this.BtnSource.UseVisualStyleBackColor = true;
            this.BtnSource.Click += new System.EventHandler(this.BtnSource_Click);
            // 
            // BtnTarget
            // 
            this.BtnTarget.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.BtnTarget.Location = new System.Drawing.Point(664, 14);
            this.BtnTarget.Name = "BtnTarget";
            this.BtnTarget.Size = new System.Drawing.Size(27, 25);
            this.BtnTarget.TabIndex = 5;
            this.BtnTarget.Text = "...";
            this.BtnTarget.UseVisualStyleBackColor = true;
            this.BtnTarget.Click += new System.EventHandler(this.BtnTarget_Click);
            // 
            // listViewSource
            // 
            this.listViewSource.AllowDrop = true;
            this.listViewSource.Location = new System.Drawing.Point(15, 45);
            this.listViewSource.Name = "listViewSource";
            this.listViewSource.Size = new System.Drawing.Size(283, 350);
            this.listViewSource.TabIndex = 6;
            this.listViewSource.UseCompatibleStateImageBehavior = false;
            this.listViewSource.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.listViewSource_ItemDrag);
            // 
            // listViewTarget
            // 
            this.listViewTarget.AllowDrop = true;
            this.listViewTarget.Location = new System.Drawing.Point(426, 43);
            this.listViewTarget.Name = "listViewTarget";
            this.listViewTarget.Size = new System.Drawing.Size(283, 350);
            this.listViewTarget.TabIndex = 7;
            this.listViewTarget.UseCompatibleStateImageBehavior = false;
            this.listViewTarget.DragDrop += new System.Windows.Forms.DragEventHandler(this.listViewTarget_DragDrop);
            this.listViewTarget.DragEnter += new System.Windows.Forms.DragEventHandler(this.listViewTarget_DragEnter);
            // 
            // pbDrag
            // 
            this.pbDrag.Image = global::Copy_Paste.Properties.Resources.dragndrop;
            this.pbDrag.Location = new System.Drawing.Point(304, 69);
            this.pbDrag.Name = "pbDrag";
            this.pbDrag.Size = new System.Drawing.Size(116, 74);
            this.pbDrag.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbDrag.TabIndex = 8;
            this.pbDrag.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(727, 411);
            this.Controls.Add(this.pbDrag);
            this.Controls.Add(this.listViewTarget);
            this.Controls.Add(this.listViewSource);
            this.Controls.Add(this.BtnTarget);
            this.Controls.Add(this.BtnSource);
            this.Controls.Add(this.txtTarget);
            this.Controls.Add(this.txtSource);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "File Copy Utility";
            ((System.ComponentModel.ISupportInitialize)(this.pbDrag)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtSource;
        private System.Windows.Forms.TextBox txtTarget;
        private System.Windows.Forms.Button BtnSource;
        private System.Windows.Forms.Button BtnTarget;
        private System.Windows.Forms.ListView listViewSource;
        private System.Windows.Forms.PictureBox pbDrag;
        internal System.Windows.Forms.ListView listViewTarget;
    }
}

