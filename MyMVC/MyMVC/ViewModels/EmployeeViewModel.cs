﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyMVC.ViewModels
{
    public class EmployeeViewModel
    {
        public string EmployeeId { get; set; }
        public string FullName { get; set; }
        public string JoinYear { get; set; }
        public string JoinColor { get; set; }
    }
}