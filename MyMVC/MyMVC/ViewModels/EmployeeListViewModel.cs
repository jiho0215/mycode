﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyMVC.ViewModels
{
    public class EmployeeListViewModel
    {
        public List<EmployeeViewModel> employees { get; set; }
    }
}