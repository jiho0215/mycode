﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MyMVC.Models;
using MyMVC.ViewModels;

namespace MyMVC.Controllers
{
    public class EmployeeController : Controller
    {
        // GET: Employee
        public ActionResult Index()
        {
            EmployeeListViewModel empLVM = new EmployeeListViewModel();
            List<EmployeeViewModel> tempEmpLVM = new List<EmployeeViewModel>();
            List<Employee> empList = (new EmployeeBusinessLayer()).GetEmployees();
            empList.ForEach(emp =>
            {
                EmployeeViewModel empVM = new EmployeeViewModel();
                empVM.EmployeeId = emp.EmployeeId;
                ModelState.Remove("FullName");
                empVM.FullName = emp.FirstName + " " + emp.LastName;
                empVM.JoinYear = emp.JoinYear;
                empVM.JoinColor = (int.Parse(emp.JoinYear) < 2010) ? "#4d94ff" : "#80ff80";
                tempEmpLVM.Add(empVM);
            });
            empLVM.employees = tempEmpLVM;
            return View("Employee_Index", empLVM);
        }
        public ActionResult SaveEmployee([ModelBinder(typeof(MyEmployeeModelBinder))]Employee e)
        {
            EmployeeBusinessLayer empBal = new EmployeeBusinessLayer();
            empBal.SaveEmployee(e);
            return RedirectToAction("Index");
        }
        public class MyEmployeeModelBinder : DefaultModelBinder
        {
            protected override object CreateModel(ControllerContext controllerContext, ModelBindingContext bindingContext, Type modelType)
            {
                Employee e = new Employee();
                e.EmployeeId = controllerContext.RequestContext.HttpContext.Request.Form["EmployeeId"];
                e.FirstName = controllerContext.RequestContext.HttpContext.Request.Form["FirstName"];
                e.LastName = controllerContext.RequestContext.HttpContext.Request.Form["LastName"];
                e.JoinYear = controllerContext.RequestContext.HttpContext.Request.Form["JoinYear"];
                return e;
            }
        }
    }
}