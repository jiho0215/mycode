﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MyMVC.DataAccessLayer;
using MyMVC.ViewModels;

namespace MyMVC.Models
{
    public class EmployeeBusinessLayer
    {
        public List<Employee> GetEmployees()
        {
            MyMVCDBDAL employeeDal = new MyMVCDBDAL();
            return employeeDal.Employees.ToList();
        }
        public Employee SaveEmployee(Employee e)
        {
            MyMVCDBDAL myMVCDal = new MyMVCDBDAL();
            myMVCDal.Employees.Add(e);
            myMVCDal.SaveChanges();
            return e;
        }
    }
}