﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyBinaryTree
{
    public class BinaryTree<T>
    {
        public BTNode<T> Root { get; set; }
        public BinaryTree() { }
        public BinaryTree(T rootData)
        {
            Root = new BTNode<T>(rootData);
        }
        public void PreorderTraversal()
        {
            PreorderTraversal(Root);
        }
        public void InorderTraversal()
        {
            InorderTraversal(Root);
        }
        public void PostorderTraversal()
        {
            PostorderTraversal(Root);
        }
        private void PreorderTraversal(BTNode<T> node)
        {
            if (node != null)
            {
                Console.Write("{0} ", node.Data);
                PreorderTraversal(node.Left);
                PreorderTraversal(node.Right);
            }
        }
        private void InorderTraversal(BTNode<T> node)
        {
            if (node != null)
            {
                InorderTraversal(node.Left);
                Console.Write("{0} ", node.Data);
                InorderTraversal(node.Right);
            }
        }
        public void PostorderTraversal(BTNode<T> node)
        {
            if (node != null)
            {
                PostorderTraversal(node.Left);
                PostorderTraversal(node.Right);
                Console.Write("{0} ", node.Data);
            }
        }
        public void LevelTraversal()
        {
            Queue<BTNode<T>> q = new Queue<BTNode<T>>();
            q.Enqueue(Root);
            q.Enqueue(null);
            while (q.Count() > 0)
            {
                var node = q.Dequeue();
                if (node == null)
                {
                    Console.WriteLine();
                    if (q.Count() > 0) q.Enqueue(null); //Key point to perform without counting level
                    continue;
                }
                Console.Write("{0} ", node.Data);
                if (node.Left != null)
                    q.Enqueue(node.Left);
                if (node.Right != null)
                    q.Enqueue(node.Right);
            }
        }
        public int Count()
        {
            return Count(Root);
        }
        private int Count(BTNode<T> node)
        {
            if (node == null)
            {
                return 0;
            }
            else
            {
                return 1 + Count(node.Left) + Count(node.Right);
            }
        }
    }
}
//below is not good code - JL
//public void LevelTraversal()
//{
//    if(Root != null)
//    {
//        Queue<BTNode<T>> currQ = new Queue<BTNode<T>>();
//        Queue<BTNode<T>> nextQ = new Queue<BTNode<T>>();
//        currQ.Enqueue(Root);
//        while (currQ.Count() > 0)
//        {
//            BTNode<T> node = currQ.Dequeue();
//            Console.Write("{0} ", node.Data);
//            if (node.Left != null)
//            {
//                nextQ.Enqueue(node.Left);
//            }
//            if (node.Right != null)
//            {
//                nextQ.Enqueue(node.Right);
//            }
//            if (currQ.Count() == 0)
//            {
//                Console.WriteLine();
//                Queue<BTNode<T>> tempQ;
//                tempQ = currQ;
//                currQ = nextQ;
//                nextQ = tempQ;
//            }
//        }
//    }
//}
//public void LevelTraversal()
//{
//    if (Root != null)
//    {
//        Queue<BTNode<T>> q = new Queue<BTNode<T>>();
//        q.Enqueue(Root);
//        q.Enqueue(null);
//        int currLevelNode = 1;
//        int nextLevelNode = 0;
//        while (q.Count() > 0)
//        {
//            BTNode<T> node = q.Dequeue();
//            if (node == null)
//            {
//                Console.WriteLine();
//                continue;
//            }
//            currLevelNode--;
//            Console.Write("{0} ", node.Data);
//            if (node.Left != null)
//            {
//                q.Enqueue(node.Left);
//                nextLevelNode++;
//            }
//            if (node.Right != null)
//            {
//                q.Enqueue(node.Right);
//                nextLevelNode++;
//            }
//            if (currLevelNode == 0)
//            {
//                q.Enqueue(null);
//                currLevelNode = nextLevelNode;
//                nextLevelNode = 0;
//            }
//        }
//    }
//}
//public void LevelTraversal()
//{
//    if (Root != null)
//    {
//        Queue<BTNode<T>> q = new Queue<BTNode<T>>();
//        q.Enqueue(Root);
//        int currLevelNode = 1;
//        int nextLevelNode = 0;
//        while (q.Count() > 0)
//        {
//            if (currLevelNode == 0)
//            {
//                Console.WriteLine();
//                currLevelNode = nextLevelNode;
//                nextLevelNode = 0;
//            }
//            BTNode<T> node = q.Dequeue();
//            currLevelNode--;
//            if(node != null)
//            {
//                Console.Write("{0} ", node.Data);
//                q.Enqueue(node.Left);
//                q.Enqueue(node.Right);
//                nextLevelNode += 2;
//            }
//        }
//    }
//}
//public int Count()
//{
//    count = 0;
//    Count(Root);
//    return count;
//}
//private void Count(BTNode<T> node)
//{
//    if (node != null)
//    {
//        count++;
//        Count(node.Left);
//        Count(node.Right);
//    }
//}
