﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyBinaryTree;

namespace BinaryTreeTester
{
    class Program
    {
        static void Main(string[] args)
        {
            new Program().Run();
        }
        public void Run()
        {
            BinaryTree<int> bt = new BinaryTree<int>(1);
            bt.Root.Left = new BTNode<int>(2);
            bt.Root.Left.Left = new BTNode<int>(4);
            bt.Root.Left.Right = new BTNode<int>(5);
            bt.Root.Left.Right.Left = new BTNode<int>(6);
            //bt.Root.Left.Left.Right = bt.Root.Left.Right.Left;
            bt.Root.Right = new BTNode<int>(3);
            bt.Root.Right.Right = new BTNode<int>(7);
            bt.Root.Right.Right.Left = new BTNode<int>(8);

            Console.WriteLine("Preorder Traversal: ");
            bt.PreorderTraversal();
            Console.WriteLine("\nInorder Traversal: ");
            bt.InorderTraversal();
            Console.WriteLine("\nPostorder Traversal: ");
            bt.PostorderTraversal();
            Console.WriteLine("\nLevel Traversal: ");
            bt.LevelTraversal();
            Console.WriteLine("Count: {0}", bt.Count());

            Console.ReadKey();
        }
    }
}