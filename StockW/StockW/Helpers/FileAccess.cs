﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StockW.Models;
using Newtonsoft.Json;
using System.IO;

namespace StockW.Helpers
{
    public class FileAccess
    {
        public enum PeriodType { D1Rank, D2Rank, D3Rank, D5Rank, D10Rank, D25Rank, WeeklyRank, MonthlyRank, D1, D2, D3, D5, D10, D25, WEEKLY, MONTHLY}
        public enum DataType { Compact, Compare}
        public void WriteJson(string folder, string fileName, List<CompactStockInfoModel> stocks)
        {
            string fullPath = System.IO.Path.Combine(folder, fileName.ToString());
            string json = JsonConvert.SerializeObject(stocks.ToArray());
            //write string to file
            System.IO.File.WriteAllText(fullPath, json);
        }

        public void WriteJson(string folder, string fileName, List<CompareStockInfoModel> stocks)
        {
            string fullPath = System.IO.Path.Combine(folder, fileName.ToString());
            string json = JsonConvert.SerializeObject(stocks.ToArray());
            //write string to file
            System.IO.File.WriteAllText(fullPath, json);
        }

        public void ReadJsonTopRank(string folder, string fileName, DataType dataType, out List<CompactStockInfoModel> compactStocks, out List<CompareStockInfoModel> compareStocks)
        {
            compactStocks = new List<CompactStockInfoModel>();
            compareStocks = new List<CompareStockInfoModel>();

            if (dataType == DataType.Compact)
            {
                string fullPath = System.IO.Path.Combine(folder, fileName.ToString());
                using (StreamWriter file = File.CreateText(fullPath))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    //serialize object directly into file stream
                    serializer.Serialize(file, compactStocks);
                }
            }
            if (dataType == DataType.Compare)
            {
                string fullPath = System.IO.Path.Combine(folder, fileName.ToString());
                using (StreamWriter file = File.CreateText(fullPath))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    //serialize object directly into file stream
                    serializer.Serialize(file, compareStocks);
                }
            }
        }
    }
}
