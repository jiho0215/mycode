﻿using Avapi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockW
{
    public static class ConnectionHelper
    {
        public static IAvapiConnection GetConnector()
        {
            IAvapiConnection connection = AvapiConnection.Instance;

            // Set up the connection and pass the API_KEY provided by alphavantage.co
            connection.Connect("S70W95MNX24PL31Z");
            return connection;
        }        
    }
}
