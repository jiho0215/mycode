﻿using Avapi.AvapiTIME_SERIES_DAILY;
using Avapi.AvapiTIME_SERIES_INTRADAY;
using StockW.Models.DataContainer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockW.EntityFramwork.Procedures
{
    public class InsertData
    {
        private GetData GetData = new GetData();
        private bool InsertEODLog(EODLog eodLog)
        {
            using (var db = new StockWEntities())
            {
                try
                {
                    db.EODLogs.Add(eodLog);
                    db.SaveChanges();
                    return true;
                }
                catch (Exception e)
                {
                    new InsertError().InsertErrorMsg(eodLog.Symbol, "Failed to insert data InsertEODLog() for Date: " + ((DateTime)eodLog.CloseDate).Date + " Message: " + e.Message);
                    return false;
                }
            }
        }
        private void _insertIntradayLog(IntraDayLog intradayLog)
        {
            using (var db = new StockWEntities())
            {
                try
                {
                    db.IntraDayLogs.Add(intradayLog);
                    db.SaveChanges();
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }
        private bool GetFlagIsDataExistInDB(string symbol, DateTime date)
        {
            EODLog eodLog = GetData.GetEODLog(symbol, date);
            if (eodLog != null)
            {
                return true;
            }
            return false;
        }

        public void InsertEODLog(IAvapiResponse_TIME_SERIES_DAILY data)
        {
            string symbol = data.Data.MetaData.Symbol;
            int total = 0;
            int inserted = 0;
            int failed = 0;
            foreach (var log in data.Data.TimeSeries)
            {
                if (!GetFlagIsDataExistInDB(symbol, Convert.ToDateTime(log.DateTime)))
                {
                    total++;
                    EODLog eodLog = new EODLog()
                    {
                        Symbol = symbol,
                        CloseDate = Convert.ToDateTime(log.DateTime),
                        Close = Convert.ToDouble(log.close),
                        Open = Convert.ToDouble(log.open),
                        Low = Convert.ToDouble(log.low),
                        High = Convert.ToDouble(log.high),
                        CRE_DTTM = DateTime.Now,
                        Volume = Convert.ToInt32(log.volume)
                    };
                    if (InsertEODLog(eodLog))
                    {
                        inserted++;
                    }
                    else
                    {
                        failed++;
                    }
                }
                //else
                //{
                //    break;
                //}
            }
            string returnMessage = "Symbol: " + symbol + " Insert EODLog - Inserted: " + inserted + " Total: " + total + " Time: " + DateTime.Now.TimeOfDay.ToString();
            if (failed > 0)
            {
                returnMessage = "Symbol:" + symbol + " Insert EODLog - FAILED: " + failed + " Inserted: " + inserted + " Total: " + total + " Time: " + DateTime.Now.TimeOfDay.ToString();
            }
            Console.WriteLine(returnMessage);
        }

        public void InsertIntradayLog(IAvapiResponse_TIME_SERIES_INTRADAY data)
        {
            string symbol = data.Data.MetaData.Symbol;
            var log = data.Data.TimeSeries[0];
            IntraDayLog intradayLog = new IntraDayLog()
            {
                Symbol = symbol,
                CloseDate = Convert.ToDateTime(log.DateTime).Date,
                CloseDTTM = Convert.ToDateTime(log.DateTime),
                Close = Convert.ToDouble(log.close),
                Open = Convert.ToDouble(log.open),
                Low = Convert.ToDouble(log.low),
                High = Convert.ToDouble(log.high),
                CRE_DTTM = DateTime.Now,
                Volume = Convert.ToInt32(log.volume)
            };
            string returnMessage = "Symbol: " + symbol + " Insert IntradayLog. Time: " + DateTime.Now.TimeOfDay.ToString();
            if (!IsIntraDataExist(intradayLog))
            {
                _insertIntradayLog(intradayLog);
            }
            else {
                returnMessage = "Symbol: " + symbol + " IntradayLog data is already exist.";
            }
            Console.WriteLine(returnMessage);
        }

        private bool IsIntraDataExist(IntraDayLog intraDayLog)
        {
            bool result = GetData.GetIntradayLogByCloseDTTM(intraDayLog.Symbol, Convert.ToDateTime(intraDayLog.CloseDTTM)) != null;
            return result;
        }

        public void InsertFilteredStock(FilteredStock stock)
        {
            DateTime now = DateTime.Now;
            WatchSymbol watchSymbol = new WatchSymbol()
            {
                CreateDTTM = now,
                IsActive = "Y",
                IsHoldingStock = "N",
                IsManualInput = "N",
                IsMarketIndex = stock.IsMarketIndex ? "Y" : "N",
                IsValidated = "N",
                Symbol = stock.Symbol,
                TargetBuy = stock.TargetBuy,
                TargetSell = stock.TargetSell
            };
            using (var db = new StockWEntities())
            {
                try
                {
                    db.WatchSymbols.Add(watchSymbol);
                    db.SaveChanges();
                }
                catch (Exception e)
                {
                    new InsertError().InsertErrorMsg(stock.Symbol, "Failed to insert data InsertValidatedStock(). Message: " + e.Message);
                }
            }
        }
        public void UpsertFilteredList(List<FilteredStock> filteredStocks)
        {
            if(filteredStocks != null && filteredStocks.Count > 0)
            {
                foreach (var stock in filteredStocks)
                {
                    if(IsFilteredStockExist(stock.Symbol))
                    {
                        UpdateFilteredStock(SetTargetPrice(stock));
                    }
                    else
                    {
                        InsertFilteredStock(SetTargetPrice(stock));
                    }
                    
                }
            }
        }
        private bool IsFilteredStockExist(string symbol)
        {
            return (GetData.GetFilteredWatchStock(symbol) != null);
            
        }
        private void UpdateFilteredStock(FilteredStock stock)
        {
            using (var db = new StockWEntities())
            {
                try
                {
                    var existingStock = db.WatchSymbols.FirstOrDefault(x => x.Symbol == stock.Symbol);
                    if(existingStock != null)
                    {
                        existingStock.IsActive = "Y";
                        db.SaveChanges();
                    }
                }
                catch(Exception e)
                {
                    throw e;
                }
            }
        }
        public FilteredStock SetTargetPrice (FilteredStock stock)
        {
            return stock;
        }
    }
}