﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockW.EntityFramwork.Procedures
{
    public class InsertError
    {
        DateTime StartTime = Convert.ToDateTime(Properties.Settings.Default.StartTime);
        DateTime EndTime = Convert.ToDateTime(Properties.Settings.Default.EndTime).AddDays(1);
        public void InsertErrorMsg(string symbol, string msg)
        {
            using (var db = new StockWEntities())
            {
                if(db.Error_Log.Where(x => x.Symbol == symbol & x.CRE_DTTM > StartTime & x.CRE_DTTM < EndTime & x.Message == msg).Any())
                {
                    Console.WriteLine("Error exist.");
                    return;
                }
                var error = new Error_Log()
                {
                    LogId = decimal.Parse(DateTime.Now.ToString("yyMMddHHmmssFFF")),
                    Symbol = symbol,
                    Message = msg,
                    CRE_DTTM = DateTime.Now
                };
                db.Error_Log.Add(error);
                db.SaveChanges();
                Console.WriteLine("Error : " + msg + " Symbol: " + symbol);
            }
        }
    }
}
