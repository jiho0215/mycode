﻿using StockW.Models.DataContainer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockW.EntityFramwork.Procedures
{
    public class InsertSMA
    {
        public bool InsertDailyHistoricalData(DailyContent dailyContent)
        {
            //Daily
            return false;
        }
        public bool InsertSMAYesterday(Daily yesterday)
        {
            bool result = false;
            using (var db = new StockWEntities())
            {
                //var Yesterday = new Daily()
                //{
                //    Low = yesterday.Low,
                //    High = yesterday.High,
                //    Symbol = yesterday.Symbol,
                //    CRE_DTTM = yesterday.CRE_DTTM
                //};
                //db.Yesterdays.Add(Yesterday);
                //db.SaveChanges();
            }
            return result;
        }
        public bool InsertSMA5(SMA5Day sma5Days)
        {
            bool result = false;
            using (var db = new StockWEntities())
            {
                var SMA5 = new SMA5Day()
                {
                    LowAverage = sma5Days.LowAverage,
                    HighAverage = sma5Days.HighAverage,
                    Highest = sma5Days.Highest,
                    Lowest = sma5Days.Lowest,
                    Symbol = sma5Days.Symbol,
                    CRE_DTTM = sma5Days.CRE_DTTM
                };
                db.SMA5Day.Add(SMA5);
                db.SaveChanges();
            }
            return result;
        }
        public bool InsertSMA25(SMA25Day sma25Days)
        {
            bool result = false;
            using (var db = new StockWEntities())
            {
                var SMA25 = new SMA25Day()
                {
                    LowAverage = sma25Days.LowAverage,
                    HighAverage = sma25Days.HighAverage,
                    Highest = sma25Days.Highest,
                    Lowest = sma25Days.Lowest,
                    Symbol = sma25Days.Symbol,
                    CRE_DTTM = sma25Days.CRE_DTTM
                };
                db.SMA25Day.Add(SMA25);
                db.SaveChanges();
            }
            return result;
        }
        public bool InsertSMA60(SMA60Day sma60Days)
        {
            bool result = false;
            using (var db = new StockWEntities())
            {
                var SMA60 = new SMA60Day()
                {
                    LowAverage = sma60Days.LowAverage,
                    HighAverage = sma60Days.HighAverage,
                    Highest = sma60Days.Highest,
                    Lowest = sma60Days.Lowest,
                    Symbol = sma60Days.Symbol,
                    CRE_DTTM = sma60Days.CRE_DTTM
                };
                db.SMA60Day.Add(SMA60);
                db.SaveChanges();
            }
            return result;
        }
    }
}
