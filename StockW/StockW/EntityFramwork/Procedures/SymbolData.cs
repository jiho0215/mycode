﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockW.EntityFramwork.Procedures
{
    public class SymbolData
    {
        public void AddSymbol(StockInfo stockInfo)
        {
            using (var db = new StockWEntities())
            {
                try
                {
                    db.StockInfoes.Add(stockInfo);
                    db.SaveChanges();
                }
                catch (Exception e)
                {
                    new InsertError().InsertErrorMsg(stockInfo.Symbol, "Failed to insert data InsertValidatedStock(). Message: " + e.Message);
                }
            }
        }

        public void UpdateSymbol(StockInfo stockInfo)
        {
            //update if any change.(not null)

        }

        public void InactivateSymbol(string symbol)
        {
            using (var db = new StockWEntities())
            {
                var Symbol = db.StockInfoes.SingleOrDefault(x => x.Symbol == symbol);
                if(Symbol != null)
                {
                    Symbol.ActiveFlag = "N";
                    Symbol.UpdateDTTM = DateTime.Now;
                    db.SaveChanges();
                }
            }
        }

        public List<StockInfo> GetAllSymbols()
        {
            List<StockInfo> stockInfos = new List<StockInfo>();
            using (var db = new StockWEntities())
            {
                stockInfos= db.StockInfoes.OrderBy(x => x.Symbol).ToList();
            }
            return stockInfos;
        }

        public List<string> GetActiveSymbols()
        {
            var StockInfos = GetAllSymbols();
            return StockInfos.Where(x => x.ActiveFlag == "Y" && !x.Symbol.Contains("%^%") && !x.Symbol.Contains("%.%") && !x.Symbol.Contains("%~%")).Select(x => x.Symbol.Trim()).ToList();
        }

        public List<string> GetAllValidatedList()
        {
            List<string> symbols = new List<string>();
            using (var db = new StockWEntities())
            {
                symbols = db.WatchSymbols.Where(x => x.IsActive == "Y" && (x.IsValidated == "Y" || x.IsManualInput == "Y") && x.IsHoldingStock == "N").Select(x => x.Symbol.Trim()).ToList();
            }
            return symbols;
        }
        public List<string> GetAllHoldingList()
        {
            List<string> symbols = new List<string>();
            using (var db = new StockWEntities())
            {
                symbols = db.WatchSymbols.Where(x => x.IsHoldingStock == "Y").Select(x => x.Symbol.Trim()).ToList();
            }
            return symbols;
        }
    }
}
