﻿using Avapi.AvapiSMA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StockW.Models.DataContainer;
using StockW.DataAccess;
using StockW.Models.TechnicalDataModel;
using StockW.EntityFramwork.Procedures;
using StockW.EntityFramwork;

namespace StockW.Controllers
{
    public class DailyDataCollector
    {
        TechnicalData TechnicalData = new TechnicalData();
        StockData StockData = new StockData();
        public DailySMA GetDailySMA(string symbol, int timePeriod = 100, DateTime? dateTime = null)
        {
            DateTime currDate = (dateTime != null) ? ((DateTime)dateTime).Date : DateTime.Now.Date;

            DailySMA sma = new DailySMA();
            //CompactStockData resultData = new CompactStockData();
            //interval = point per 5min : 8hr * 12 = 96
            var lowSMAResponse = TechnicalData.GetSMA(symbol, Const_SMA.SMA_interval.daily, timePeriod, Const_SMA.SMA_series_type.low);
            
            //todo: return if duplicated date.

            foreach(SMAEMAIndicatorModel indicator in lowSMAResponse.SMAIndicators)
            {
                if(indicator.DateTime.Date == currDate)
                {
                    sma.SMADaily.LowToday = indicator.IndicatorValue;
                    sma.SMADaily.Symbol = symbol;
                    sma.SMADaily.CRE_DTTM = indicator.DateTime;
                }
                //Get SMA day 5 value
                if(indicator.DateTime.Date == currDate.AddDays(-5).Date)
                {
                    sma.SMA5Days.Lowest = indicator.IndicatorValue;
                    sma.SMA5Days.Symbol = symbol;
                    sma.SMA5Days.CRE_DTTM = indicator.DateTime;
                }
                //Get SMA day 25 value
                if (indicator.DateTime.Date == currDate.AddDays(-25).Date)
                {
                    sma.SMA25Days.Lowest = indicator.IndicatorValue;
                    sma.SMA25Days.Symbol = symbol;
                    sma.SMA25Days.CRE_DTTM = indicator.DateTime;
                }
                //Get SMA day 60 value
                if (indicator.DateTime.Date == currDate.AddDays(-60).Date)
                {
                    sma.SMA60Days.Lowest = indicator.IndicatorValue;
                    sma.SMA60Days.Symbol = symbol;
                    sma.SMA60Days.CRE_DTTM = indicator.DateTime;
                }
            }
            var highSMAResponse = TechnicalData.GetSMA(symbol, Const_SMA.SMA_interval.daily, timePeriod, Const_SMA.SMA_series_type.high);
            foreach (SMAEMAIndicatorModel indicator in lowSMAResponse.SMAIndicators)
            {
                if (indicator.DateTime.Date == currDate)
                {
                    sma.SMADaily.HighToday = indicator.IndicatorValue;
                    sma.SMADaily.Symbol = symbol;
                    sma.SMADaily.CRE_DTTM = indicator.DateTime;
                }
                //Get SMA day 5 value
                if (indicator.DateTime.Date == currDate.AddDays(-5).Date)
                {
                    sma.SMA5Days.Highest = indicator.IndicatorValue;
                    sma.SMA5Days.Symbol = symbol;
                    sma.SMA5Days.CRE_DTTM = indicator.DateTime;
                }
                //Get SMA day 25 value
                if (indicator.DateTime.Date == currDate.AddDays(-25).Date)
                {
                    sma.SMA25Days.Highest = indicator.IndicatorValue;
                    sma.SMA25Days.Symbol = symbol;
                    sma.SMA25Days.CRE_DTTM = indicator.DateTime;
                }
                //Get SMA day 60 value
                if (indicator.DateTime.Date == currDate.AddDays(-60).Date)
                {
                    sma.SMA60Days.Highest = indicator.IndicatorValue;
                    sma.SMA60Days.Symbol = symbol;
                    sma.SMA60Days.CRE_DTTM = indicator.DateTime;
                }
            }
            return sma;
        }
    }
}
