﻿using Avapi;
using Avapi.AvapiSMA;
using Avapi.AvapiEMA;
using StockW.Converters;
using StockW.Models.TechnicalDataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockW.DataAccess
{
    public class TechnicalData : IData
    {
        public SMAEMAModel GetSMA(string symbol, Const_SMA.SMA_interval interval, int timePeriod, Const_SMA.SMA_series_type type)
        {
            // Get the TIME_SERIES_DAILY query object
            Int_SMA SMA = Connection.GetQueryObject_SMA();
            // Perform the TIME_SERIES_DAILY request and get the result
            IAvapiResponse_SMA SMAResponse = SMA.Query(symbol, interval, timePeriod, type);
            return TechnicalConverter.SMAConvert(SMAResponse.Data);
        }
        public SMAEMAModel GetEMA(string symbol, Const_EMA.EMA_interval interval, int timePeriod, Const_EMA.EMA_series_type type)
        {
            // Get the TIME_SERIES_DAILY query object
            Int_EMA EMA = Connection.GetQueryObject_EMA();
            // Perform the TIME_SERIES_DAILY request and get the result
            IAvapiResponse_EMA EMAResponse = EMA.Query(symbol, interval, timePeriod, type);
            return TechnicalConverter.EMAConvert(EMAResponse.Data);
        }
    }
}
