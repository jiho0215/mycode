﻿using Avapi;
using Avapi.AvapiTIME_SERIES_INTRADAY;
using Avapi.AvapiTIME_SERIES_DAILY;
using Avapi.AvapiTIME_SERIES_WEEKLY;
using Avapi.AvapiTIME_SERIES_MONTHLY;
using Avapi.AvapiTIME_SERIES_MONTHLY_ADJUSTED;
using StockW.Models;
using StockW.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using static StockW.Helpers.Constant;
using StockW.Models.DataContainer;
using StockW.EntityFramwork.Procedures;
using StockW.Helpers;

namespace StockW.DataAccess
{
    public class StockData : IData
    {
        public IAvapiResponse_TIME_SERIES_DAILY GetDailyContent(string symbol, out bool isOffline)
        {
            isOffline = false;
            IAvapiResponse_TIME_SERIES_DAILY dailyResponse = null;
            try
            {
                Int_TIME_SERIES_DAILY time_series_daily = Connection.GetQueryObject_TIME_SERIES_DAILY();
                dailyResponse = time_series_daily.Query(symbol, Const_TIME_SERIES_DAILY.TIME_SERIES_DAILY_outputsize.compact);
                if(dailyResponse.Data == null)
                {
                    new InsertError().InsertErrorMsg(symbol, "Got null Data from URL: " + dailyResponse.LastHttpRequest);
                }
                else if (dailyResponse.Data.Error)
                {
                    if (dailyResponse.Data.ErrorMessage == "AlphaV error: Application Error" || dailyResponse.Data.ErrorMessage == "AlphaV error: Offline for Maintenance")
                    {
                        isOffline = true;
                        return null;
                    }
                    else
                    {
                        new InsertError().InsertErrorMsg(symbol, "Error from URL: " + dailyResponse.LastHttpRequest + " Error Msg: "+ dailyResponse.Data.ErrorMessage);
                        return null;
                    }
                }
                else if (dailyResponse.RawData.IndexOf("verify your API signature") > -1)
                {
                    new InsertError().InsertErrorMsg(symbol, dailyResponse.RawData);
                    isOffline = true;
                    return null;
                }
            }
            catch(Exception e)
            {
                var innerExceptionMessage = (e.InnerException == null) ? "" : e.InnerException.Message;
                new InsertError().InsertErrorMsg(symbol, "Failed to get Data. DailyDataCollector.GetHistoricalDailyData() Message: "  + e.Message + " Inner Message: " + innerExceptionMessage);
            }
            //return Converter.DailyConvert(dailyResponse);
            return dailyResponse;
        }

        public IAvapiResponse_TIME_SERIES_INTRADAY GetIntradayContent(string symbol)
        {
            Int_TIME_SERIES_INTRADAY time_series_intraDaily = null;
            IAvapiResponse_TIME_SERIES_INTRADAY intraDailyResponse = null;
            try
            {
                Console.WriteLine("GetIntradayContent() started " + DateTime.Now.TimeOfDay.ToString());
                time_series_intraDaily = Connection.GetQueryObject_TIME_SERIES_INTRADAY();
                intraDailyResponse = time_series_intraDaily.Query(symbol, Const_TIME_SERIES_INTRADAY.TIME_SERIES_INTRADAY_interval.n_1min, Const_TIME_SERIES_INTRADAY.TIME_SERIES_INTRADAY_outputsize.compact);
            }
            catch (Exception e)
            {
                var exceptionMsg = "Exception Msg: " + e.Message;
                var innerExceptionMessage = (e.InnerException == null) ? "" : " Inner Exception Msg: " + e.InnerException.Message;
                new InsertError().InsertErrorMsg(symbol, "Unexpected error occured from StockData.GetIntradayContent(). " + e.Message + innerExceptionMessage);
            }
            return intraDailyResponse;
        }

        public CompactStockModel GetIntraDayCompact(string symbol, Const_TIME_SERIES_INTRADAY.TIME_SERIES_INTRADAY_interval interval)
        {
            // Get the TIME_SERIES_DAILY query object
            Int_TIME_SERIES_INTRADAY time_series_intraDaily = Connection.GetQueryObject_TIME_SERIES_INTRADAY();
            // Perform the TIME_SERIES_DAILY request and get the result
            IAvapiResponse_TIME_SERIES_INTRADAY intraDailyResponse =
            time_series_intraDaily.Query(symbol, interval, Const_TIME_SERIES_INTRADAY.TIME_SERIES_INTRADAY_outputsize.compact);
            return Converter.IntradayCompactConvert(intraDailyResponse.Data);
        }
        public CompactStockModel GetWeeklyCompact(string symbol)
        {
            Int_TIME_SERIES_WEEKLY time_series_weekly = Connection.GetQueryObject_TIME_SERIES_WEEKLY();
            IAvapiResponse_TIME_SERIES_WEEKLY weeklyResponse = time_series_weekly.QueryPrimitive(symbol);
            return Converter.WeelyCompactConvert(weeklyResponse.Data);
        }
        public CompactStockModel GetMonthlyCompact(string symbol)
        {
            Int_TIME_SERIES_MONTHLY time_series_monthly = Connection.GetQueryObject_TIME_SERIES_MONTHLY();
            IAvapiResponse_TIME_SERIES_MONTHLY monthlyResponse = time_series_monthly.QueryPrimitive(symbol);
            return Converter.MonthlyCompactConvert(monthlyResponse.Data);
        }
        //Get average from daily data.
        public CompactStockInfoModel GetDayBaseAverage(string symbol, Period period, int round = 4)
        {
            //CompactStockModel result = GetDailyCompact(symbol);
            double openSum = 0;
            double closeSum = 0;
            double lowSum = 0;
            double highSum = 0;
            int openCnt = 0;
            int closeCnt = 0;
            int lowCnt = 0;
            int highCnt = 0;
            int runCount = 0;

            //foreach (CompactStockInfoModel info in result.CompactStockInfo)
            //{
            //    if (period == Period.Week && runCount > 8)
            //    {
            //        break;
            //    }
            //    else if (period == Period.Month && runCount > 31)
            //    {
            //        break;
            //    }
            //    else if (period == Period.Semianual && runCount > 184)
            //    {
            //        break;
            //    }
            //    else if (period == Period.Anual && runCount > 366)
            //    {
            //        break;
            //    }

            //    if (info.Open != 0)
            //    {
            //        openSum += info.Open;
            //        openCnt++;
            //    }
            //    if (info.Close != 0)
            //    {
            //        closeSum += info.Close;
            //        closeCnt++;
            //    }
            //    if (info.Low != 0)
            //    {
            //        lowSum += info.Low;
            //        lowCnt++;
            //    }
            //    if (info.High != 0)
            //    {
            //        highSum += info.High;
            //        highCnt++;
            //    }

            //    runCount++;
            //}
            CompactStockInfoModel averageInfo = new CompactStockInfoModel();
            averageInfo.Open = Math.Round((openSum / openCnt), round);
            averageInfo.Close = Math.Round((closeSum / closeCnt), round);
            averageInfo.Low = Math.Round((lowSum / lowCnt), round);
            averageInfo.High = Math.Round((highSum / highCnt), round);
            return averageInfo;
        }

        //Get average data.
        //public CompactStockInfoModel GetCompactStockAverage(CompactStockModel stockData, Periods period, int round = 4)
        //{
        //    double openSum = 0;
        //    double closeSum = 0;
        //    double lowSum = 0;
        //    double highSum = 0;
        //    int openCnt = 0;
        //    int closeCnt = 0;
        //    int lowCnt = 0;
        //    int highCnt = 0;
        //    int runCount = 0;

        //    //foreach (CompactStockInfoModel info in stockData.CompactStockInfo)
        //    //{
        //    //    if (period == Periods.D1 && runCount > 1)
        //    //    {
        //    //        break;
        //    //    }
        //    //    if (period == Periods.D2 && runCount > 2)
        //    //    {
        //    //        break;
        //    //    }
        //    //    if (period == Periods.D3 && runCount > 2)
        //    //    {
        //    //        break;
        //    //    }
        //    //    if (period == Periods.D5 && runCount > 5)
        //    //    {
        //    //        break;
        //    //    }
        //    //    if (period == Periods.D10 && runCount > 10)
        //    //    {
        //    //        break;
        //    //    }
        //    //    if (period == Periods.D25 && runCount > 25)
        //    //    {
        //    //        break;
        //    //    }
        //    //    else if (period == Periods.Weekly && runCount > 52)
        //    //    {
        //    //        break;
        //    //    }
        //    //    else if (period == Periods.Monthly && runCount > 36)
        //    //    {
        //    //        break;
        //    //    }

        //    //    if (info.Open != 0)
        //    //    {
        //    //        openSum += info.Open;
        //    //        openCnt++;
        //    //    }
        //    //    if (info.Close != 0)
        //    //    {
        //    //        closeSum += info.Close;
        //    //        closeCnt++;
        //    //    }
        //    //    if (info.Low != 0)
        //    //    {
        //    //        lowSum += info.Low;
        //    //        lowCnt++;
        //    //    }
        //    //    if (info.High != 0)
        //    //    {
        //    //        highSum += info.High;
        //    //        highCnt++;
        //    //    }
        //    //    runCount++;
        //    //}
        //    CompactStockInfoModel averageInfo = new CompactStockInfoModel();
        //    averageInfo.Open = Math.Round((openSum / openCnt), round);
        //    averageInfo.Close = Math.Round((closeSum / closeCnt), round);
        //    averageInfo.Low = Math.Round((lowSum / lowCnt), round);
        //    averageInfo.High = Math.Round((highSum / highCnt), round);
        //    return averageInfo;
        //}
    }
}
