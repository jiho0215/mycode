﻿using Avapi;
using StockW.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockW.DataAccess
{
    public abstract class IData
    {
        public IAvapiConnection Connection = ConnectionHelper.GetConnector();
        public StockDataConverter Converter = new StockDataConverter();
        public TechnicalDataConverter TechnicalConverter = new TechnicalDataConverter();
    }
}
