﻿using Avapi;
using Avapi.AvapiSMA;
using Avapi.AvapiEMA;
using StockW.Models.TechnicalDataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockW.Converters
{
    public class TechnicalDataConverter
    {
        public SMAEMAModel SMAConvert(IAvapiResponse_SMA_Content data)
        {
            SMAEMAModel SMA = new SMAEMAModel();
            SMA.Indicator = data.MetaData.Indicator;
            SMA.Interval = data.MetaData.Interval;
            SMA.LastRefreshed = DateTime.Parse(data.MetaData.LastRefreshed);
            SMA.SeriesType = data.MetaData.SeriesType;
            SMA.Symbol = data.MetaData.Symbol;
            SMA.TimePeriod = Int32.Parse(data.MetaData.TimePeriod);
            SMA.TimeZone = data.MetaData.TimeZone;
            foreach (TechnicalIndicator_Type_SMA indicator in data.TechnicalIndicator)
            {
                SMAEMAIndicatorModel indicatorModel = new SMAEMAIndicatorModel();
                indicatorModel.IndicatorValue = double.Parse(indicator.SMA);
                indicatorModel.DateTime = DateTime.Parse(indicator.DateTime);
                SMA.SMAIndicators.Add(indicatorModel);
            }
            return SMA;
        }
        public SMAEMAModel EMAConvert(IAvapiResponse_EMA_Content data)
        {
            SMAEMAModel EMA = new SMAEMAModel();
            EMA.Indicator = data.MetaData.Indicator;
            EMA.Interval = data.MetaData.Interval;
            EMA.LastRefreshed = DateTime.Parse(data.MetaData.LastRefreshed);
            EMA.SeriesType = data.MetaData.SeriesType;
            EMA.Symbol = data.MetaData.Symbol;
            EMA.TimePeriod = Int32.Parse(data.MetaData.TimePeriod);
            EMA.TimeZone = data.MetaData.TimeZone;
            foreach (TechnicalIndicator_Type_EMA indicator in data.TechnicalIndicator)
            {
                SMAEMAIndicatorModel indicatorModel = new SMAEMAIndicatorModel();
                indicatorModel.IndicatorValue = double.Parse(indicator.EMA);
                indicatorModel.DateTime = DateTime.Parse(indicator.DateTime);
            }
            return EMA;
        }
    }
}
