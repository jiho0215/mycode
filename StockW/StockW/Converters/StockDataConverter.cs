﻿using Avapi.AvapiTIME_SERIES_DAILY;
using Avapi.AvapiTIME_SERIES_INTRADAY;
using Avapi.AvapiTIME_SERIES_MONTHLY;
using Avapi.AvapiTIME_SERIES_WEEKLY;
using StockW.EntityFramwork;
using StockW.Models;
using StockW.Models.DataContainer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockW.Converters
{
    public class StockDataConverter
    {
        public CompactStockModel IntradayCompactConvert(IAvapiResponse_TIME_SERIES_INTRADAY_Content data)
        {
            CompactStockModel Result = new CompactStockModel();

            //if (data.Error)
            //{
            //    Console.WriteLine(data.ErrorMessage);
            //}
            //else
            //{
            //    Result.Information = data.MetaData.Information;
            //    Result.Symbol = data.MetaData.Symbol;
            //    Result.LastRefreshed = DateTime.Parse(data.MetaData.LastRefreshed);
            //    Result.OutputSize = data.MetaData.OutputSize;
            //    Result.TimeZone = data.MetaData.TimeZone;
            //    foreach (var timeseries in data.TimeSeries)
            //    {
            //        CompactStockInfoModel info = new CompactStockInfoModel();
            //        info.Open = double.Parse(timeseries.open);
            //        info.High = double.Parse(timeseries.high);
            //        info.Low = double.Parse(timeseries.low);
            //        info.Close = double.Parse(timeseries.close);
            //        info.Volume = Int32.Parse(timeseries.volume);
            //        info.DateTime = DateTime.Parse(timeseries.DateTime);
            //        Result.CompactStockInfo.Add(info);
            //    }
            //}
            return Result;
        }
        public DailyContent DailyConvert(IAvapiResponse_TIME_SERIES_DAILY dailyResponse)
        {
            DailyContent Result = new DailyContent();
            if(dailyResponse == null || dailyResponse.Data == null || dailyResponse.Data.TimeSeries.Count == 0)
            {
                return Result;
            }
            IAvapiResponse_TIME_SERIES_DAILY_Content data = dailyResponse.Data;
            Result.MetaData.Information = data.MetaData.Information;
            Result.MetaData.Symbol = data.MetaData.Symbol;
            Result.MetaData.LastRefreshed = data.MetaData.LastRefreshed;
            Result.MetaData.OutputSize = data.MetaData.OutputSize;
            Result.MetaData.TimeZone = data.MetaData.TimeZone;
            foreach (var timeseries in data.TimeSeries)
            {
                DailyTimeSeries info = new DailyTimeSeries();
                info.Open = double.Parse(timeseries.open);
                info.High = double.Parse(timeseries.high);
                info.Low = double.Parse(timeseries.low);
                info.Close = double.Parse(timeseries.close);
                info.Volume = double.Parse(timeseries.volume);
                info.DateTime = DateTime.Parse(timeseries.DateTime);
                Result.TimeSeriese.Add(info);
            }
            return Result;
        }
        public CompactStockModel MonthlyCompactConvert(IAvapiResponse_TIME_SERIES_MONTHLY_Content data)
        {
            CompactStockModel Result = new CompactStockModel();

            if (data.Error)
            {
                Console.WriteLine(data.ErrorMessage);
            }
            else
            {
                //Result.Information = data.MetaData.Information;
                //Result.Symbol = data.MetaData.Symbol;
                //Result.LastRefreshed = DateTime.Parse(data.MetaData.LastRefreshed);
                //Result.TimeZone = data.MetaData.TimeZone;
                //foreach (var timeseries in data.TimeSeries)
                //{
                //    CompactStockInfoModel info = new CompactStockInfoModel();
                //    info.Open = double.Parse(timeseries.open);
                //    info.High = double.Parse(timeseries.high);
                //    info.Low = double.Parse(timeseries.low);
                //    info.Close = double.Parse(timeseries.close);
                //    info.Volume = double.Parse(timeseries.volume);
                //    info.DateTime = DateTime.Parse(timeseries.DateTime);
                //    Result.CompactStockInfo.Add(info);
                //}
            }
            return Result;
        }
        public CompactStockModel WeelyCompactConvert(IAvapiResponse_TIME_SERIES_WEEKLY_Content data)
        {
            CompactStockModel Result = new CompactStockModel();

            if (data.Error)
            {
                Console.WriteLine(data.ErrorMessage);
            }
            else
            {
                //Result.Information = data.MetaData.Information;
                //Result.Symbol = data.MetaData.Symbol;
                //Result.LastRefreshed = DateTime.Parse(data.MetaData.LastRefreshed);
                //Result.TimeZone = data.MetaData.TimeZone;
                //foreach (var timeseries in data.TimeSeries)
                //{
                //    CompactStockInfoModel info = new CompactStockInfoModel();
                //    info.Open = double.Parse(timeseries.open);
                //    info.High = double.Parse(timeseries.high);
                //    info.Low = double.Parse(timeseries.low);
                //    info.Close = double.Parse(timeseries.close);
                //    info.Volume = double.Parse(timeseries.volume);
                //    info.DateTime = DateTime.Parse(timeseries.DateTime);
                //    Result.CompactStockInfo.Add(info);
                //}
            }
            return Result;
        }

        public DailyLog ConvertDailyContent(DailyContent data)
        {
            if (data == null)
            {
                return null;
            }
            //Get Lowest/Highest among N days. 5,10,15,...
            DailyLog result = new DailyLog();
            int count = data.TimeSeriese.Count();
            int avg4WeekCount = 20;
            int avg12WeekCount = 60;
            int avg20WeekCount = 100;
            double avgSum = 0;
            if (count == 0)
            {
                return result;
            }
            double lowest = data.TimeSeriese[0].Low;
            double highest = data.TimeSeriese[0].High;
            result.DailyLogId = decimal.Parse(DateTime.Now.ToString("yyMMddHHmmss"));
            result.Date = data.TimeSeriese[0].DateTime.Date;
            result.CRE_DTTM = DateTime.Now;
            result.Symbol = data.MetaData.Symbol;
            result.LowToday = lowest;
            result.HighToday = highest;
            result.CloseToday = data.TimeSeriese[0].Close;
            for (int y = 1; y < count; y++)
            {
                //GetAverages
                avgSum += (data.TimeSeriese[y].Low + data.TimeSeriese[y].High) / 5;
                switch (y)
                {
                    case 19:
                        {
                            result.Avg4Weeks = Math.Round(avgSum / avg4WeekCount, 5);
                            break;
                        }
                    case 59:
                        {
                            result.Avg12Weeks = Math.Round(avgSum / avg12WeekCount, 5);
                            break;
                        }
                    case 99:
                        {
                            result.Avg20Weeks = Math.Round(avgSum / avg20WeekCount, 5);
                            break;
                        }
                }

                if (y == 1 || y == 6 || y == 11 || y == 21 || y == 31 || y == 41 || y == 51)
                {
                    lowest = data.TimeSeriese[y].Low;
                    highest = data.TimeSeriese[y].High;
                }
                lowest = (lowest < data.TimeSeriese[y].Low) ? lowest : data.TimeSeriese[y].Low;
                highest = (highest > data.TimeSeriese[y].High) ? highest : data.TimeSeriese[y].High;
                switch (y)
                {
                    case 5:
                        {
                            result.Low5Day = lowest;
                            result.High5Day = highest;
                            break;
                        }
                    case 10:
                        {
                            result.Low10Day = lowest;
                            result.High10Day = highest;
                            break;
                        }
                    case 20:
                        {
                            result.Low20Day = lowest;
                            result.High20Day = highest;
                            break;
                        }
                    case 30:
                        {
                            result.Low30Day = lowest;
                            result.High30Day = highest;
                            break;
                        }
                    case 40:
                        {
                            result.Low40Day = lowest;
                            result.High40Day = highest;
                            break;
                        }
                    case 50:
                        {
                            result.Low50Day = lowest;
                            result.High50Day = highest;
                            break;
                        }
                    case 60:
                        {
                            result.Low60Day = lowest;
                            result.High60Day = highest;
                            break;
                        }
                }
            }
            return result;
        }
    }
}
