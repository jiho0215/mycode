﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockW.Models.TechnicalDataModel
{
    public class SMAEMAModel
    {
        public SMAEMAModel()
        {
            SMAIndicators = new List<SMAEMAIndicatorModel>();
        }
        public string Symbol { get; set; }
        public string Indicator { get; set; }
        public DateTime LastRefreshed { get; set; }
        public string Interval { get; set; }
        public int TimePeriod { get; set; }
        public string SeriesType { get; set; }
        public string TimeZone { get; set; }

        public List<SMAEMAIndicatorModel> SMAIndicators { get; set; }
    }
}
