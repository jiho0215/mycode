﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockW.Models.TechnicalDataModel
{
    public class SMAEMAIndicatorModel
    {
        public double IndicatorValue { get; set; }
        public DateTime DateTime { get; set; }
    }
}
