﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockW.Models.DataContainer
{
    public class DailyMetaData
    {
        public string Information
        {
            set;
            get;
        }

        public string Symbol
        {
            set;
            get;
        }

        public string LastRefreshed
        {
            set;
            get;
        }

        public string OutputSize
        {
            set;
            get;
        }

        public string TimeZone
        {
            set;
            get;
        }
    }
}
