﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockW.Models.DataContainer
{
    public class ComparedDataContainer
    {
        public List<CompareStockModel> DailyStockCompares { get; set; }
        public List<CompareStockModel> WeeklyStockCompares { get; set; }
        public List<CompareStockModel> MonthlyStockCompares { get; set; }

        public ComparedDataContainer()
        {
            DailyStockCompares = new List<CompareStockModel>();
            WeeklyStockCompares = new List<CompareStockModel>();
            MonthlyStockCompares = new List<CompareStockModel>();
        }
    }
}
