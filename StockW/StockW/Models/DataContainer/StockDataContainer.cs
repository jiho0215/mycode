﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockW.Models.DataContainer
{
    public class StockDataContainer
    {
        public List<CompactStockModel> DailyStocks { get; set; }
        public List<CompactStockModel> WeeklyStocks { get; set; }
        public List<CompactStockModel> MonthlyStocks { get; set; }

        public StockDataContainer()
        {
            DailyStocks = new List<CompactStockModel>();
            WeeklyStocks = new List<CompactStockModel>();
            MonthlyStocks = new List<CompactStockModel>();
        }
    }
}
