﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockW.Models
{
    public class CompareStockModel
    {
        public string CompareTitle { get; set; }
        public string Information { get; set; }
        public string Symbol { get; set; }
        public DateTime LastRefreshed { get; set; }
        public string OutputSize { get; set; }
        public string TimeZone { get; set; }

        public CompareStockInfoModel CompareStockInfo { get; set; }
    }
}
