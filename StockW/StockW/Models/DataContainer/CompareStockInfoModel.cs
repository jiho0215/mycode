﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockW.Models
{
    public class CompareStockInfoModel
    {
        public double LastVSLowHighDaily { get; set; }
        public double LastVSLowHighWeekly { get; set; }
        public double LastVSLowHighMonthly { get; set; }
        public DateTime DateTime { get; set; }
    }
}
