﻿using StockW.EntityFramwork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockW.Models.DataContainer
{
    public class DailySMA
    {
        public Daily SMADaily { get; set; }
        public SMA5Day SMA5Days { get; set; }
        public SMA25Day SMA25Days { get; set; }
        public SMA60Day SMA60Days { get; set; }
        public DailySMA()
        {
            SMADaily = new Daily();
            SMA5Days = new SMA5Day();
            SMA25Days = new SMA25Day();
            SMA60Days = new SMA60Day();
        }
    }
}
