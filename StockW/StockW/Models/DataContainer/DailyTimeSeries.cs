﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockW.Models.DataContainer
{
    public class DailyTimeSeries
    {
        public double Open
        {
            set;
            get;
        }

        public double High
        {
            set;
            get;
        }

        public double Low
        {
            set;
            get;
        }

        public double Close
        {
            set;
            get;
        }

        public double Volume
        {
            set;
            get;
        }

        public DateTime DateTime
        {
            set;
            get;
        }
    }
}
