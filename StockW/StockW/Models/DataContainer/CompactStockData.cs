﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace StockW.Models.DataContainer
{
    public class CompactStockData
    {
        public string Information { get; set; }
        public string Symbol { get; set; }
        public DateTime LastRefreshed { get; set; }
        public string OutputSize { get; set; }
        public string TimeZone { get; set; }
        public double LastClosedPrice { get; set; }
    }
}
