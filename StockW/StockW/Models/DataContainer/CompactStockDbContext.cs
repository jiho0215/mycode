﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace StockW.Models.DataContainer
{
    public class CompactStockDbContext : DbContext
    {
        public CompactStockDbContext() : base("name=StockContext")
        {

        }
        public DbSet<CompactStockData> CompactStocks { get; set; }
    }
}
