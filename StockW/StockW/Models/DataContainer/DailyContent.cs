﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockW.Models.DataContainer
{
    public class DailyContent
    {
        public DailyMetaData MetaData { get; set; }
        public List<DailyTimeSeries> TimeSeriese { get; set; }

        public DailyContent()
        {
            MetaData = new DailyMetaData();
            TimeSeriese = new List<DailyTimeSeries>();
        }
    }
}
