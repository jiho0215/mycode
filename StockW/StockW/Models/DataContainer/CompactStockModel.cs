﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockW.Models
{
    public class CompactStockModel
    {
        public string Symbol { get; set; }
        public double LastClosedPrice { get; set; }
        public double Open { get; set; }
        public double Close { get; set; }
        public double Low { get; set; }
        public double High { get; set; }
        public double Volume { get; set; }
        public DateTime DateTime { get; set; }
    }
}
