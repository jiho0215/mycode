﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StockW.Helpers;
using System.IO;
using System.Data.OleDb;
using System.Data;
using Excel = Microsoft.Office.Interop.Excel;
using StockW.Models.DataContainer;
using StockW.EntityFramwork.Procedures;

namespace StockW.Controllers
{
    public class SymbolController
    {
        SymbolData SymbolData = new SymbolData();
        DownloadHelper DownloadHelper = new DownloadHelper();
        public void RefreshSymbolList()
        {
            bool isNewNASDAQ = false;
            bool isNewNYSE = false;
            List<StockInfo> NewSymbolList = new List<StockInfo>();
            //Download Latest Lists
            DownloadSymbolFile(out isNewNASDAQ, out isNewNYSE);
            //Load Latest File from local
            if (isNewNASDAQ)
            {
                NewSymbolList.AddRange(ReadExcelFile(Properties.Download.Default.NASDAQ_Path));
            }
            if (isNewNYSE)
            {
                NewSymbolList.AddRange(ReadExcelFile(Properties.Download.Default.NYSE_Path));
            }
            var ODSSymbolList = SymbolData.GetAllSymbols();
            if (NewSymbolList.Count > 0)
            {
                //check for new
                foreach (var newSymbol in NewSymbolList)
                {
                    if (ODSSymbolList.Where(x => x.Symbol.Trim() == newSymbol.Symbol.Trim()).FirstOrDefault() == null)
                    {
                        StockInfo ss = new StockInfo()
                        {
                            Symbol = newSymbol.Symbol?.Trim(),
                            Sector = newSymbol.Sector?.Trim(),
                            ActiveFlag = "Y",
                            Industry = newSymbol.Industry?.Trim(),
                            IPOYear = newSymbol.IPOYear?.Trim(),
                            Name = newSymbol.Name?.Trim(),
                            CreateDTTM = DateTime.Now
                        };
                        SymbolData.AddSymbol(ss);
                        Console.WriteLine("New Symbol: " + ss.Symbol);
                    }
                }
                foreach (var ODSSymbol in ODSSymbolList)
                {
                    if (NewSymbolList.Where(x => x.Symbol.Trim() == ODSSymbol.Symbol.Trim()).FirstOrDefault() == null)
                    {
                        SymbolData.InactivateSymbol(ODSSymbol.Symbol);
                        Console.WriteLine("Inactivate Symbol: " + ODSSymbol.Symbol);
                    }
                }
            }
        }
        public void DownloadSymbolFile(out bool isNewNASDAQ, out bool isNewNYSE)
        {
            string NASDAQfileName = DateTime.Now.ToString("MM_dd_yyyy") + "_NASDAQ.csv";
            string NYSEfileName = DateTime.Now.ToString("MM_dd_yyyy") + "_NYSE.csv";
            FileInfo latestNASDAQFileInfo = GetLatestFileInfo(Properties.Download.Default.NASDAQ_Path);
            FileInfo latestNYSEFileInfo = GetLatestFileInfo(Properties.Download.Default.NYSE_Path);

            if (latestNASDAQFileInfo == null || latestNASDAQFileInfo.Name != NASDAQfileName)
            {
                System.IO.Directory.CreateDirectory(Properties.Download.Default.NASDAQ_Path);
                string NASDAQFullPath = Path.Combine(Properties.Download.Default.NASDAQ_Path, NASDAQfileName);
                DownloadHelper.DownloadSymbolListFile(Properties.Download.Default.NASDAQ_URL, NASDAQFullPath);
                Console.WriteLine("NYSDAQ downloaded.");
                isNewNASDAQ = true;
            }
            else
            {
                Console.WriteLine("Latest NASDAQ Already Exist.");
                isNewNASDAQ = false;
            }
            if (latestNYSEFileInfo == null || latestNYSEFileInfo.Name != NYSEfileName)
            {
                System.IO.Directory.CreateDirectory(Properties.Download.Default.NYSE_Path);
                string NYSEFullPath = Path.Combine(Properties.Download.Default.NYSE_Path, NYSEfileName);
                DownloadHelper.DownloadSymbolListFile(Properties.Download.Default.NYSE_URL, NYSEFullPath);
                Console.WriteLine("NYSE downloaded.");
                isNewNYSE = true;
            }
            else
            {
                Console.WriteLine("Latest NYSE Already Exist.");
                isNewNYSE = false;
            }

        }
        public List<StockInfo> ReadExcelFile(string fileFolder)
        {
            string latestFileFullPath = GetLatestFileInfo(fileFolder).FullName;
            List<StockInfo> Symbols = new List<StockInfo>();
            //Create COM Objects. Create a COM object for everything that is referenced
            Excel.Application xlApp = new Excel.Application();
            try
            {
                Excel.Workbook xlWorkbook = xlApp.Workbooks.Open(latestFileFullPath);
                Excel._Worksheet xlWorksheet = (xlWorkbook.Worksheets.Count > 0) ? xlWorkbook.Sheets[1] : null;
                if (xlWorksheet == null)
                {
                    return null;
                }
                Excel.Range xlRange = xlWorksheet.UsedRange;
                // Read all data from data range in the worksheet
                var valueArray = (object[,])xlRange.get_Value(Excel.XlRangeValueDataType.xlRangeValueDefault);

                int rowCount = xlRange.Rows.Count;
                int columnCount = xlRange.Columns.Count;

                for (int i = 2; i <= rowCount; i++)
                {
                    StockInfo symbol = new StockInfo();
                    for (int j = 1; j <= columnCount; j++)
                    {
                        var value2 = valueArray[i, j];
                        string value = string.Empty;
                        if (value2 != null)
                        {
                            value = value2.ToString();
                            if (value.ToUpper() == "N/A")
                            {
                                continue;
                            }
                        }
                        else
                        {
                            continue;
                        }
                        switch (j)
                        {
                            case (1):
                                symbol.Symbol = value;
                                break;
                            case (2):
                                symbol.Name = value;
                                break;
                            case (3):
                                //symbol.LastSale = value;
                                break;
                            case (4):
                                //symbol.MarketCap = value;
                                break;
                            case (5):
                                //symbol.ADR_TSO = value;
                                break;
                            case (6):
                                symbol.IPOYear = value;
                                break;
                            case (7):
                                symbol.Sector = value;
                                break;
                            case (8):
                                symbol.Industry = value;
                                break;
                            case (9):
                                //symbol.Summary_Quote = value;
                                break;
                            default:
                                break;
                        }
                    }
                    Symbols.Add(symbol);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return Symbols;
        }
        public FileInfo GetLatestFileInfo(string fileFolder)
        {
            var directory = new DirectoryInfo(fileFolder);
            try
            {
                var file = (from f in directory.GetFiles()
                            orderby f.LastWriteTime descending
                            select f).First();
                return file;
            }
            catch
            {
                return null;
            }
        }
    }
}
