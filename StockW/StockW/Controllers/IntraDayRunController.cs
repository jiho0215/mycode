﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using StockW.EntityFramwork.Procedures;
using StockW.DataAccess;
using StockW.Models.DataContainer;
using StockW.Converters;
using StockW.EntityFramwork;
using Avapi.AvapiTIME_SERIES_DAILY;
using StockW.Helpers;
using Avapi.AvapiTIME_SERIES_INTRADAY;

namespace StockW.Controllers
{
    public class IntraDayRunController
    {
        DateTime EndTime;
        List<string> MonitoringList = new List<string>();
        string Symbol { get; set; }
        InsertError InsertError = new InsertError();
        GetData GetData = new GetData();
        SymbolData SymbolData = new SymbolData();
        StockData StockData = new StockData();
        StockDataConverter Converter = new StockDataConverter();
        TimeHelper TimeHelper = new TimeHelper();
        

        public void StartRun(DateTime endTime)
        {
            if (!Properties.TestSettings.Default.IsIntraOn)
            {
                Console.WriteLine("Intraday log is off. " + DateTime.Now);
                return;
            }
            if (DateTime.Now.DayOfWeek == DayOfWeek.Saturday || DateTime.Now.DayOfWeek == DayOfWeek.Sunday)
            {
                Console.WriteLine("This is weekend. End IntraDay Run. " + DateTime.Now);
                return;
            }

            EndTime = endTime;
            InitialSetup();
            //Daily Loop
            int count = MonitoringList.Count;
            if (count > 0)
            {
                int MonitoringListIndex = 0;
                while (DateTime.Now < EndTime)
                {
                    var intradayContent = LoadIntradayLog(MonitoringList[MonitoringListIndex]);

                    if (intradayContent != null)
                    {
                        InsertIntradayLog(intradayContent);
                    }

                    MonitoringListIndex++;
                    if (MonitoringListIndex == count)
                    {
                        MonitoringListIndex = 0;
                    }

                    TimeHelper.Pause(Properties.RunManager.Default.AlphaIntervalSec);
                }
            }
        }

        public void OneTimeLoadIntradayLog(string symbol, bool saveDB = false)
        {
            var intradayContent = LoadIntradayLog(symbol);

            if (saveDB && intradayContent != null)
            {
                InsertIntradayLog(intradayContent);
            }
        }

        private void InitialSetup()
        {
            List<string> HoldingSymbols = new List<string>();
            List<string> ValidatedSymbols = new List<string>();
            //Set Symbols.
            int _alphaGetPerMin = 60 / Properties.RunManager.Default.AlphaIntervalSec;
            int _holdingListInterval = _alphaGetPerMin * Properties.RunManager.Default.HoldingIntervalMin;
            HoldingSymbols = SymbolData.GetAllHoldingList();
            ValidatedSymbols = SymbolData.GetAllValidatedList();
           if(ValidatedSymbols.Count > 0)
            {
                foreach (var symbol in ValidatedSymbols)
                {
                    if (MonitoringList.Count % _holdingListInterval == 0)
                    {
                        //Add Holding list
                        foreach (var hSymbol in HoldingSymbols)
                        {
                            MonitoringList.Add(hSymbol);
                        }
                    }
                    //Add next validated symbol
                    MonitoringList.Add(symbol);
                }
            }
        }

        /// <summary>
        /// Return false if failed to load.
        /// </summary>
        /// <param name="symbol"></param>
        /// <returns></returns>
        private IAvapiResponse_TIME_SERIES_INTRADAY LoadIntradayLog(string symbol)
        {
            IAvapiResponse_TIME_SERIES_INTRADAY intradayResponse = null;
            intradayResponse = StockData.GetIntradayContent(symbol);
            if (!ValidateRawData(intradayResponse))
            {
                intradayResponse = null;
            }
            return intradayResponse;
        }
        private bool ValidateRawData(IAvapiResponse_TIME_SERIES_INTRADAY rawData)
        {
            bool isValid = false;

            if (rawData == null)
            {
                new InsertError().InsertErrorMsg("", "Error. RawData == null.");
                return isValid;
            }

            if (rawData.Data == null)
            {
                new InsertError().InsertErrorMsg("", "Error. RawData.Data``````` == null.");
                return isValid;
            }

            if (rawData.Data.Error)
            {
                if (rawData.Data.ErrorMessage == "Application Error" || rawData.Data.ErrorMessage == "Offline for Maintenance")
                {
                    return isValid;
                }
                else
                {
                    new InsertError().InsertErrorMsg(rawData.Data.MetaData.Symbol, "Error from URL: " + rawData.LastHttpRequest + " Error Msg: " + rawData.Data.ErrorMessage);
                    return isValid;
                }
            }
            else if (rawData.RawData.IndexOf("verify your API signature") > -1)
            {
                new InsertError().InsertErrorMsg(rawData.Data.MetaData.Symbol, rawData.RawData);
                return isValid;
            }

            if (rawData.Data == null)
            {
                new InsertError().InsertErrorMsg("", "rawData.Data == null");
                return isValid;
            }
            if (rawData.Data.TimeSeries == null)
            {
                new InsertError().InsertErrorMsg("", "rawData.Data.TimeSeries == null");
                return isValid;
            }
            if (rawData.Data.TimeSeries.Count == 0)
            {
                new InsertError().InsertErrorMsg("", "rawData.Data.TimeSeries.Count == 0");
                return isValid;
            }

            string symbol = rawData.Data.MetaData.Symbol;

            string errorMsg = "";
            if (Convert.ToDouble(rawData.Data.TimeSeries[0].open) == 0)
            {
                errorMsg = "Open = 0; ";
            }
            if (Convert.ToDouble(rawData.Data.TimeSeries[0].close) == 0)
            {
                errorMsg = "Close = 0; ";
            }
            if (Convert.ToDouble(rawData.Data.TimeSeries[0].low) == 0)
            {
                errorMsg = "Low = 0; ";
            }
            if (Convert.ToDouble(rawData.Data.TimeSeries[0].high) == 0)
            {
                errorMsg = "High = 0; ";
            }
            if (!string.IsNullOrEmpty(errorMsg))
            {
                new InsertError().InsertErrorMsg(symbol, errorMsg);
                return isValid;
            }

            isValid = true;
            return isValid;
        }

        private void InsertIntradayLog(IAvapiResponse_TIME_SERIES_INTRADAY rawData)
        {
            Task.Run(() =>
            {
                new InsertData().InsertIntradayLog(rawData);
            });
        }
    }
}
