﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using StockW.EntityFramwork.Procedures;
using StockW.DataAccess;
using StockW.Models.DataContainer;
using StockW.Converters;
using StockW.EntityFramwork;
using Avapi.AvapiTIME_SERIES_DAILY;
using StockW.Helpers;
using StockWHelper;

namespace StockW
{
    class RunController
    {
        DateTime StartTime;
        DateTime EndTime;
        DateTime? TargetDate = null;
        Queue<string> Symbols = new Queue<string>();
        string Symbol { get; set; }
        InsertError InsertError = new InsertError();
        GetData GetData = new GetData();
        SymbolData SymbolData = new SymbolData();
        StockData StockData = new StockData();
        StockDataConverter Converter = new StockDataConverter();
        TimeHelper TimeHelper = new TimeHelper();
        RunManager RunManager = new RunManager();

        public void StartRun(DateTime startTime, DateTime endTime)
        {
            StartTime = startTime;
            EndTime = endTime;

            if (!Properties.TestSettings.Default.IsEODOn)
            {
                Console.WriteLine("EOD is off." + DateTime.Now);
            }
            //if (StartTime.DayOfWeek == DayOfWeek.Saturday || StartTime.DayOfWeek == DayOfWeek.Sunday)
            //{
            //    Console.WriteLine("It's weekend. End EOD Load. " + DateTime.Now);
            //    return;
            //}

            
            InitialSetup();
            
            //Daily Loop
            while (DateTime.Now < endTime)
            {
                //if start time, fill queue
                //Symbols = InitAtStartTime(Symbols);
                Console.WriteLine("Queue Count : " + Symbols.Count());
                //Main Run - Get data as long as queue has symbol.
                if(Symbols.Count > 0)
                {
                    Symbol = Symbols.Dequeue();
                    if(!string.IsNullOrEmpty(Symbol))
                    {
                        MainRun(Symbol);
                    }
                }
                else
                {
                    Console.WriteLine("Queue is empty. Ending EODLog. " + DateTime.Now);
                    break;
                    
                    //TimeHelper.Pause(60);
                }                
                //if end time, log and empty queue
                CleanAtEndTime(Symbols);
            }
        }

        private void InitialSetup()
        {
            Symbols = FillQueue();
        }

        private Queue<string> InitAtStartTime(Queue<string> symbols)
        {
            if (DateTime.Now.DayOfWeek != DayOfWeek.Saturday && DateTime.Now.DayOfWeek != DayOfWeek.Sunday)
            {
                if (symbols.Count == 0 && DateTime.Now.TimeOfDay > StartTime.TimeOfDay && DateTime.Now.TimeOfDay < StartTime.AddMinutes(2).TimeOfDay)
                {
                    Console.WriteLine("Filled Queue. It's starting time!");
                    return FillQueue();
                }
            }
            return symbols;
        }

        private void CleanAtEndTime(Queue<string> symbols)
        {
            if(DateTime.Now.TimeOfDay > EndTime.TimeOfDay && DateTime.Now.TimeOfDay < EndTime.AddMinutes(2).TimeOfDay)
            {
                ResetInitialValues();
                if(symbols.Count > 0)
                {
                    ClearQueue(symbols);
                }
                Console.WriteLine("Cleaned Queue. It's Ending time...");
            }
        }

        private void MainRun(string symbol)
        {
            bool isSuccess = false;
            if(TargetDate == null)
            {
                isSuccess = FirstCycle(symbol);
            }
            else
            {
                isSuccess = RegularCycle(symbol);
            }
            if(!isSuccess)
            {
                Symbols.Enqueue(symbol);
            }
        }

        private bool FirstCycle(string symbol)
        {
            bool isSuccess = false;
            bool isOffline = false;
            //1. Load Data 
            var dailyContent = LoadEODLog(symbol, out isOffline);
            if(!isOffline && dailyContent != null)
            {
                //Convert
                //var castedData = ConvertRawData(dailyContent);
                // then set TargetDate
                TargetDate = Convert.ToDateTime(dailyContent.Data.TimeSeries[0].DateTime);
                //2. check DB GetFlagIsDataExistInDB()
                if (!GetFlagIsDataExistInDB(symbol, (DateTime)TargetDate))
                {
                    //3. if not exist, save
                    //SaveDataToDB(castedData);
                    InsertEODLog(dailyContent);
                }
                else
                {
                    Console.WriteLine(symbol + " already exist for " + ((DateTime)TargetDate).Date);
                }
                isSuccess = true;
            }
            return isSuccess;
        }

        private bool RegularCycle(string symbol)
        {
            bool isSuccess = false;
            bool isOffline = false;
            if (!GetFlagIsDataExistInDB(symbol, ((DateTime)TargetDate)))
            {
                var dailyContent = LoadEODLog(symbol, out isOffline);
                if (!isOffline && dailyContent != null)
                {
                    //var castedData = ConvertRawData(dailyContent);
                    //SaveDataToDB(castedData);
                    InsertEODLog(dailyContent);
                    isSuccess = true;
                }
            }
            else
            {
                Console.WriteLine(symbol + " already exist for " + ((DateTime)TargetDate).Date);
                isSuccess = true;
            }
            return isSuccess;
        }

        /// <summary>
        /// Return false if failed to load.
        /// </summary>
        /// <param name="symbol"></param>
        /// <returns></returns>
        private IAvapiResponse_TIME_SERIES_DAILY LoadEODLog(string symbol, out bool isOffline)
        {
            Console.WriteLine("Load started. Time: " + DateTime.Now.TimeOfDay.ToString());
            IAvapiResponse_TIME_SERIES_DAILY rawData = null;
            rawData = StockData.GetDailyContent(symbol, out isOffline);
            if (!ValidateRawData(symbol, rawData))
            {
                rawData = null;
            }
            TimeHelper.Pause(Properties.RunManager.Default.AlphaIntervalSec);
            return rawData;
        }

        private bool ValidateRawData(string symbol, IAvapiResponse_TIME_SERIES_DAILY rawData)
        {
            bool isValid = false;
            if(rawData == null)
            {
                new InsertError().InsertErrorMsg(symbol, "Error. RawData == null.");
                return isValid;
            }
            if(rawData.Data == null)
            {
                new InsertError().InsertErrorMsg(symbol, "rawData.Data == null");
                return isValid;
            }
            if(rawData.Data.TimeSeries == null)
            {
                new InsertError().InsertErrorMsg(symbol, "rawData.Data.TimeSeries == null");
                return isValid;
            }
            if(rawData.Data.TimeSeries.Count == 0)
            {
                new InsertError().InsertErrorMsg(symbol, "rawData.Data.TimeSeries.Count == 0");
                return isValid;
            }

            //string symbol = rawData.Data.MetaData.Symbol;
            DateTime latestDataDate = Convert.ToDateTime(rawData.Data.TimeSeries[0].DateTime);
            if (TargetDate != null && ((DateTime)TargetDate).Date != latestDataDate.Date)
            {
                string message = symbol + "- No target date data. Target Date: " + ((DateTime)TargetDate).Date + " Latest date: " + latestDataDate.Date;
                new InsertError().InsertErrorMsg(symbol, message);
                Console.WriteLine(message);
                return isValid;
            }

            string errorMsg = "";
            if (Convert.ToDouble(rawData.Data.TimeSeries[0].open) == 0)
            {
                errorMsg = "Open = 0; ";
            }
            if(Convert.ToDouble(rawData.Data.TimeSeries[0].close) == 0)
            {
                errorMsg = "Close = 0; ";
            }
            if(Convert.ToDouble(rawData.Data.TimeSeries[0].low) == 0)
            {
                errorMsg = "Low = 0; ";
            }
            if (Convert.ToDouble(rawData.Data.TimeSeries[0].high) == 0)
            {
                errorMsg = "High = 0; ";
            }
            if(!string.IsNullOrEmpty(errorMsg))
            {
                new InsertError().InsertErrorMsg(symbol, errorMsg);
                return isValid;
            }

            isValid = true;
            return isValid;
        }

        private void InsertEODLog(IAvapiResponse_TIME_SERIES_DAILY rawData)
        {
            Task.Run(() =>
            {
                new InsertData().InsertEODLog(rawData);
            });
        }

        private Queue<string> FillQueue()
        {
            List<string> symbols = SymbolData.GetActiveSymbols();


            return new Queue<string>(symbols);
        }

        private void ClearQueue(Queue<string> queue)
        {
            while(queue.Count > 0)
            {
                string symbol = queue.Dequeue();
                LogUnsavedSymbol(symbol);
                TimeHelper.Pause(1);
            }
        }

        private bool GetFlagIsDataExistInDB(string symbol, DateTime date)
        {
            EODLog eodLog = GetData.GetEODLog(symbol, date);
            if(eodLog != null)
            {
                return true;
            }
            return false;
        }

        private void LogUnsavedSymbol(string symbol)
        {
            InsertError.InsertErrorMsg(symbol, "Unsaved Symbol. " + TargetDate);
        }

        private void ResetInitialValues()
        {
            TargetDate = null;
        }
    }
}