﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using StockW.Helpers;
using StockW.EntityFramwork.Procedures;
using StockW.Models.DataContainer;

namespace StockW.Controllers
{
    public class MainRunController
    {
        TimeHelper TimeHelper = new TimeHelper();
        GetData GetData = new GetData();
        InsertData InsertData = new InsertData();

        //int averageVolume = Properties.RunManager.Default.AverageVolume;
        //int movementUp = Properties.RunManager.Default.MovementUp;
        //int movementDown = Properties.RunManager.Default.MovementDown;
        //int volatility = Properties.RunManager.Default.Volatility;
        public enum RunEnum { EODStart, IntradayStart, EODEnd, IntradayEnd };
        Dictionary<RunEnum, string> NextRunDic = new Dictionary<RunEnum, string>();
        RunEnum NextRun;
        DateTime NextRunDate = DateTime.Now;

        //1. check time and return run method number
        //2. according to the number, call method
        //3. then return to the 1.

        // 1 cycle per 1 day.
        //at the end of the cycle, set the next cycle start dttm

        public void InitSettings()
        {
            //NextRunDic.Add(RunEnum.IntradayStart, Properties.RunManager.Default.IntradayStartTime);
            //NextRunDic.Add(RunEnum.IntradayEnd, Properties.RunManager.Default.IntradayEndTime);
            NextRunDic.Add(RunEnum.EODStart, Properties.RunManager.Default.EODStartTime);
            NextRunDic.Add(RunEnum.EODEnd, Properties.RunManager.Default.EODEndTime);
            //NextRun = RunEnum.IntradayStart;

            //DateTime now = DateTime.Now;
            //NextRunDic[RunEnum.IntradayStart] = (now.AddMinutes(1)).ToString("HH:mm");
            //NextRunDic[RunEnum.IntradayEnd] = (now.AddMinutes(100)).ToString("HH:mm");
            //NextRunDic[RunEnum.EODStart] = (now.AddMinutes(2)).ToString("HH:mm");
            //NextRunDic[RunEnum.EODEnd] = (now.AddMinutes(2)).ToString("HH:mm");
        }
        public void UnlimitCircle()
        {
            InitSettings();
            while (true)
            {
                if (IsRuntime(NextRun))
                {
                    RunTrigger(NextRun);
                }
                TimeHelper.Pause(10);
            }
        }

        public bool IsRuntime(RunEnum nextRunEnum)
        {
            bool isRunTime = false;
            DateTime runTime = Convert.ToDateTime(NextRunDic[nextRunEnum]);
            DateTime nextDTTM = NextRunDate.Date + runTime.TimeOfDay;
            if (DateTime.Now > nextDTTM)
            {
                isRunTime = true;
            }
            return isRunTime;
        }

        public void RunTrigger(RunEnum nextRunEnum)
        {
            switch (nextRunEnum)
            {
                case RunEnum.IntradayStart:
                    Console.WriteLine("* IntradayStart " + DateTime.Now);
                    DateTime intraEndDTTM = Convert.ToDateTime(NextRunDic[RunEnum.IntradayEnd]);
                    new IntraDayRunController().StartRun(intraEndDTTM);
                    NextRun = RunEnum.EODStart;
                    Console.WriteLine("* IntradayEnd " + DateTime.Now);
                    break;
                case RunEnum.EODStart:
                    Console.WriteLine("* EODStart" + DateTime.Now);
                    DateTime eodEndDTTM = Convert.ToDateTime(NextRunDic[RunEnum.EODEnd]).AddDays(1);
                    new RunController().StartRun(Convert.ToDateTime(NextRunDic[nextRunEnum]), eodEndDTTM);
                    NextRun = RunEnum.IntradayStart;
                    Console.WriteLine("* EODEnd " + DateTime.Now);
                    NextRunDate = NextRunDate.AddDays(1);
                    break;
                default:
                    break;
            }
        }
    }
}