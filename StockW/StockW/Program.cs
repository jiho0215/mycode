﻿using Avapi;
using Avapi.AvapiTIME_SERIES_DAILY;
using StockW.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StockW.DataAccess;
using StockW.Models;
using StockW.Models.TechnicalDataModel;
using StockW.Helpers;
using StockW.Models.DataContainer;
using System.Threading;
using StockW.Alarm;
using StockW.Controllers;
using StockW.EntityFramwork.Procedures;
using StockW.EntityFramwork;
using System.IO;
using System.Diagnostics;
using StockWHelper;

namespace StockW
{
    class Program
    {
        static void Main(string[] args)
        {

            //30/min calls

            //var db = new StockWEntities();
            //var error = new Error_Log()
            //{
            //    LogId = decimal.Parse(DateTime.Now.ToString("yyMMddHHmmssFFF")),
            //    Symbol = "Test",
            //    Message = "test message",
            //    CRE_DTTM = DateTime.Now
            //};
            //db.Error_Log.Add(error);
            //db.SaveChanges();

            //Console.WriteLine((DateTime.Now).ToString("HH:mm"));
            //Console.ReadLine();


            //Onetime Update Symbols Run
            //new SymbolController().RefreshSymbolList();

            //Onetime intraday run
            //new IntraDayRunController().OneTimeLoadIntradayLog("TSLA");

            new Program().Task();

            Console.ReadLine();




            MainRunController MainController = new MainRunController();
            MainController.UnlimitCircle();
        }
        public void Task()
        {
            var runManager = new StockWHelper.RunManager();
            var runSpecList = new List<RunSpec>()
            {
                new RunSpec()
                {
                    RunSpecId = "EOD",
                    Sequance = 1,
                    StartMessage = "This is EOD start",
                    RunActionMethod = TestRun,
                    StartTime = DateTime.Now,
                    EndTime = DateTime.Now.AddSeconds(10),
                    Interval = 2,
                    RunWeekdays = true,
                    RunWeekend = true,
                    IsOneTimeTriger = false
                },
                new RunSpec()
                {
                    RunSpecId = "Daily",
                    Sequance = 2,
                    StartMessage = "This is Daily start.",
                    RunActionMethod = AnotherTestMethod,
                    StartTime = DateTime.Now.AddSeconds(8),
                    EndTime = DateTime.Now.AddSeconds(12),
                    Interval = 1,
                    RunWeekdays = true,
                    RunWeekend = true
                }
            };
            runManager.Initialize(runSpecList);
            runManager.SyncRun();
        }
        public void TestRun()
        {
            Console.WriteLine("This is a method");
            AnotherTestMethod();
        }
        private void AnotherTestMethod()
        {
            Console.WriteLine("This is anotherTestMethod");
        }
        public void TestRun2()
        {
            Console.WriteLine("Second method.");
        }
    }
}
