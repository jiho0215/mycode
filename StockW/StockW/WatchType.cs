//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace StockW
{
    using System;
    using System.Collections.Generic;
    
    public partial class WatchType
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public WatchType()
        {
            this.WatchStocks = new HashSet<WatchStock>();
        }
    
        public decimal Id { get; set; }
        public string WatchTypeCode { get; set; }
        public string WatchDescription { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<WatchStock> WatchStocks { get; set; }
    }
}
