﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using MorseCode;

namespace MorseCodeTester
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            GetMorseConfig();
        }
        private void GetMorseConfig()
        {
            MorseCode.Morse.Frequency = int.Parse(ConfigurationManager.AppSettings["Frequency"]);
            MorseCode.Morse.Dot = int.Parse(ConfigurationManager.AppSettings["Dot"]);
        }
        private void button1_Click(object sender, EventArgs e)
        {
            MorseCode.Morse.ToMorse(messageBox.Text);
        }
    }
}
