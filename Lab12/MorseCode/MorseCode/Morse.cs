﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Diagnostics;

namespace MorseCode
{
    public static class Morse
    {
        public static int Frequency { get; set; } = 500;
        public static int Dot { get; set; } = 200;
        private static Dictionary<char, string> MorseCodes = new Dictionary<char, string>()
        {
            {'a', ".-"},
            {'b', "-..."},
            {'c', "-.-."},
            {'d', "-.."},
            {'e', "."},
            {'f', "..-."},
            {'g', "--."},
            {'h', "...."},
            {'i', ".."},
            {'j', ".---"},
            {'k', "-.-"},
            {'l', ".-.."},
            {'m', "--"},
            {'n', "-."},
            {'o', "---"},
            {'p', ".--."},
            {'q', "--.-"},
            {'r', ".-."},
            {'s', "..."},
            {'t', "-"},
            {'u', "..-"},
            {'v', "...-"},
            {'w', ".--"},
            {'x', "-..-"},
            {'y', "-.--"},
            {'z', "--.."},
            {'0', "-----"},
            {'1', ".----"},
            {'2', "..---"},
            {'3', "...--"},
            {'4', "....-"},
            {'5', "....."},
            {'6', "-...."},
            {'7', "--..."},
            {'8', "---.."},
            {'9', "----."}
        };
        public static void ToMorse(string message)
        {
            if (!string.IsNullOrEmpty(message))
            {
                message = message.ToLower();
                ConvertToMorse(MessageToCharList(message));
            }
        }
        private static List<char> MessageToCharList(string message)
        {
            List<char> charList = new List<char>();
            foreach (char unit in message)
            {
                if(IsMorsableChar(unit))
                {
                    charList.Add(unit);
                }
            }
            return charList;
        }
        private static bool IsMorsableChar(char unit)
        {
            if ((unit >= 'a' && unit <= 'z') || (unit >= '0' && unit <= '9') || unit == ' ')
            {
                return true;
            }
            else
            {
                throw new Exception("Only alphabets, digits, and space are allowed.");
            }
        }
        private static void ConvertToMorse(List<char> charList)
        {
            int charListLen = charList.Count;
            for (int i = 0; i < charListLen; i++)
            {
                if(charList[i] == ' ')
                {
                    Debug.Write(" 7Dots \n");//Between Words
                    Thread.Sleep(Dot * 7);
                }
                else
                {
                    int morseLen = MorseCodes[charList[i]].Length;
                    for (int j = 0; j < morseLen; j++)
                    {
                        int dot = 0;
                        dot = (MorseCodes[charList[i]].Substring(j, 1) == ".") ? Dot : Dot * 3;
                        Debug.Write((dot > Dot) ? "T" : "F");
                        Console.Beep(Frequency, dot);
                        if (j < morseLen - 1)
                        {
                            Debug.Write(" 1Dot ");//Between Parts
                            Thread.Sleep(Dot);
                        }
                    }
                    if (i < charListLen - 1 && charList[i + 1] != ' ')
                    {
                        Debug.Write(" 3Dots ");//Between Letters
                        Thread.Sleep(Dot * 3);
                    }
                }
            }
        }
    }
}