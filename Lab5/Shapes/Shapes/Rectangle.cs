﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shapes
{
    public class Rectangle : Shape
    {
        public double Height { get; set; }
        public double Width { get; set; }
        private Boolean IsSquare { get { return (Height == Width); } }
        public Rectangle(double height, double width)
        {
            Height = height;
            Width = width;
        }
        public override void Draw()
        {
            Console.WriteLine("Draw Rectangle ({0}, {1})", Height.ToString(), Width.ToString());
        }
        public void Draw(int x)
        {
            Console.WriteLine("dd");
        }
        public override double GetArea()
        {
            return Height * Width;
        }
    }
}
