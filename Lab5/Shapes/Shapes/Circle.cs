﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shapes
{
    public class Circle : Shape
    {
        public double Radius { get; set; }

        public Circle(double radius)
        {
            Radius = radius;
        }
        public override void Draw()
        {
            Console.WriteLine("Draw Circle ({0})", Radius.ToString());   
        }
        public override double GetArea()
        {
            return Math.PI * Radius * Radius;
        }
    }
}
