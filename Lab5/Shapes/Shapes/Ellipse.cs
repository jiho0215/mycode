﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shapes
{
    public class Ellipse : Shape
    {
        public double RadiusX { get; set; }
        public double RadiusY { get; set; }

        public Ellipse(double radiusX, double radiusY)
        {
            RadiusX = radiusX;
            RadiusY = radiusY;
        }
        public override void Draw()
        {
            Console.WriteLine("Draw Ellipse ({0}, {1})", RadiusX.ToString(), RadiusY.ToString());

        }
        public override double GetArea()
        {
            return Math.PI * RadiusX * RadiusY;
        }
        }
    }
}
