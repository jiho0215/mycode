﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shapes
{
    public class EquilateralTriangle : Triangle
    {
        public EquilateralTriangle(double Side) : base(Side, Side, Side) { }
        public override double GetArea()
        {
            return (Math.Sqrt(3) / 4) * Side1 * Side1;
        }
    }
}
