﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shapes
{
    public class Triangle : Shape
    {
        public double Side1 { get; set; }
        public double Side2 { get; set; }
        public double Side3 { get; set; }
        protected double sumOfAllSides { get { return Side1 + Side2 + Side3; } }
        protected double longestSide { get { return Math.Max(Math.Max(Side1, Side2), Side3); } }
        protected double semiperimeter { get { return (Side1 + Side2 + Side3) / 2d; } }
        protected Boolean IsEquilateral { get { return (Side1 == Side2 && Side2 == Side3); } }

        public Triangle(double side1, double side2, double side3)
        {
            Side1 = side1;
            Side2 = side2;
            Side3 = side3;
        }
        public override void Draw()
        {
            Console.WriteLine("Draw Triangle ({0}, {1}, {2})", Side1.ToString(), Side2.ToString(), Side3.ToString());
        }
        public override double GetArea()
        {
            return Math.Sqrt(semiperimeter * (semiperimeter - Side1) * (semiperimeter - Side2) * (semiperimeter - Side3));
        }        
    }
}
