﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shapes;

namespace Tester
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Shape> shapes = new List<Shape>();

            Ellipse ellipse = new Ellipse(2,3);
            Circle circle = new Circle(6);
            Rectangle rectangle = new Rectangle(10, 20);
            Triangle triangle = new Triangle(6,6,6);
            EquilateralTriangle ET = new EquilateralTriangle(1);

            shapes.Add(ellipse);
            shapes.Add(circle);
            shapes.Add(rectangle);
            shapes.Add(triangle);
            shapes.Add(ET);
            
            foreach (Shape s in shapes)
            {
                Console.Write(s.GetArea().ToString() + " ");
                s.Draw();
                Console.WriteLine();
            }
            Console.ReadKey();
        }
    }
}
/*
 Encapsulation: 
    It helps to hide and protect data and behaviors from users or other codes when they don't need access. 
    C# and other OOP languages enhance encapsulation by access modifiers. 
    Basically there are four access modifiers: public(globally available), protected(only containing and derived class), internal(in current assembly), private(containing type)
    
    For instance, Shape class(base) has public type access modifier which enables to access to the class from anywhere. Therfore any derived class can use its properties and method.
    Type members also has access modifier (default is private). Triangle class, derived class, has a few protected properties, which is hiden from the other access point but derived class. 
    In addition, Rectangle class has private property called IsSquare. This property is only available in the class.
 
Inheritance: 
    It enables to inherit a class's return type and type members from a super (base) class. 
    For instance, All the derived classes, such as Circle, Ellipse, and Triangle, inharited all the public and protected type members from the base class. (internal type member is not inheritable) 
 
Polymorphism: 
    Polymorphism, many times, is expressed as one interface, many functions. There are two types of polymorphism: run time and compile time polymorphism.
    Overload and override are benefits of polymorphism. Overload is compile time polymorphism (faster execution but less flexible) while override is runtime polymorphism(slower execution but more flexible).
    Overload enables multiple methods with different parameters with the same method name. (Not related to the return type)
    Override enables a derived class have a method with the same signature but different functionality.
    For instance, all the derived classes overrided Draw() and GetArea() functions with its unique functionality.  
    Polymorphism also can be found from List<Shape>. By using base class type, the List is allowed to store any derived class instances of the Shape class.
    
    In addition, Factory pattern also enhances code through Polymorphism. Factory pattern enables a method to return any instance of derived class as a base type via switch/case.          
*/
