﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyQueue
{
    public interface IMyQueue<T>
    {
        int Count { get; }
        bool IsEmpty { get; }
        void Enqueue(T input);
        T Dequeue();
        T Peek();
    }
}
