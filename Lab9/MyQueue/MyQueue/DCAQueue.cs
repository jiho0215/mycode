﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyQueue
{
    public class DCAQueue<T> : IMyQueue<T>
    {
        public int Count { get; set; } = 0;
        public bool IsEmpty { get { return (Count == 0); } }
        private int capacity = 16;
        private int inIndex = 0;
        private int outIndex = 0;
        private T[] queue;
        public DCAQueue()
        {
            queue = new T[16];
        }
        public DCAQueue(int capacity)
        {
            this.capacity = capacity;
            queue = new T[capacity];
        }
        public void Enqueue(T input)
        {
            queue[inIndex] = input;
            Count++;
            if (Count < capacity)
            {
                inIndex++;
                inIndex %= capacity;
            }
            else
            {
                DoubleArray();
            }
        }
        public T Dequeue()
        {
            if (Count == 0) throw new NullReferenceException("Null Array");
            T output = queue[outIndex];
            queue[outIndex] = default(T);
            outIndex++;
            outIndex %= capacity;
            Count--;
            return output;
        }
        public T Peek()
        {
            if (Count == 0) throw new NullReferenceException("Null Array");
            return queue[outIndex];
        }
        private void DoubleArray() //O(n)
        {
            int oldCapacity = capacity;
            int tempIndex = 0;
            capacity *= 2;
            T[] temp = new T[capacity];

            while (tempIndex < oldCapacity)
            {
                temp[tempIndex] = queue[outIndex % oldCapacity];
                tempIndex++;
                outIndex++;
            }
            queue = temp;
            outIndex = 0;
            inIndex = oldCapacity;
        }
    }
}
