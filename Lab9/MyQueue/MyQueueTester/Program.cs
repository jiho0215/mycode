﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyQueue;

namespace MyQueueTester
{
    class Program
    {
        static void Main(string[] args)
        {
            new Program().Run();
        }
        public void Run()
        {
            IMyQueue<int> queue = new DCAQueue<int>();
            Console.WriteLine("Is Empty? : {0}", queue.IsEmpty);
            Console.WriteLine("Count() : {0}", queue.Count);

            //Full 16 Enqueue 
            int x = 0;
            while (x < 16)
            {
                queue.Enqueue(x + 1);
                x++;
            }
            Console.WriteLine("\nIs Empty? : {0}", queue.IsEmpty);
            Console.WriteLine("Count() : {0}\n", queue.Count);

            Console.WriteLine("Assign 10 queues in a brand new Queue");
            //10 Enqueues, 2 Dequeue then 3 Enqueues
            queue = new DCAQueue<int>();
            x = 0;
            while (x < 10)
            {
                queue.Enqueue(x + 1);
                x++;
            }
            Console.WriteLine("\nIs Empty? : {0}", queue.IsEmpty);
            Console.WriteLine("Count() : {0}\n", queue.Count);

            Console.WriteLine("Dequeue two items.");
            Console.WriteLine("Dequeue() : {0}", queue.Dequeue());
            Console.WriteLine("Dequeue() : {0}", queue.Dequeue());
            Console.WriteLine("\n Assign 10 more queues.");
            while (x < 20)
            {
                queue.Enqueue(x + 1);
                x++;
            }
            Console.WriteLine("Count() : {0}", queue.Count);
            Console.WriteLine("\nDequeue all queues.");
            while (queue.Count != 0)
            {
                Console.WriteLine("Dequeue() : {0}", queue.Dequeue());
            }
            Console.ReadKey();
        }
    }
}
