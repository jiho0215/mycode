﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using StockWHelper;
using StockWHelper.Contracts;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        //static int a = 0;
        //static int b = 0;
        //[TestMethod]
        //public static void TestMethod1()
        //{

        //    var runManager = new StockWHelper.RunManager();
        //    var runSpecList = new List<RunSpec>()
        //    {
        //        new RunSpec()
        //        {
        //            RunSpecId = "EOD",
        //            StartMessage = "This is EOD start",
        //            RunActionMethod = TestRun,
        //            StartDateTime = DateTime.Now,
        //            EndDateTime = DateTime.Now.AddSeconds(5),
        //            RunWeekdays = true
        //            ,
        //            Interval = 1
        //        },
        //        new RunSpec()
        //        {
        //            RunSpecId = "Daily",
        //            StartMessage = "This is Daily start.",
        //            RunActionMethod = AnotherTestMethod,
        //            StartDateTime = DateTime.Now.AddSeconds(8),
        //            EndDateTime = DateTime.Now.AddSeconds(12),
        //            RunWeekdays = true
        //            ,Interval = 2
        //        }
        //    };
        //    runManager.Initialize(runSpecList);
        //    var response = runManager.SyncRun("Daily");
        //    Assert.AreEqual( a , 0);
        //    Assert.AreNotEqual(b, 1);
        //}
        
        public void NextRunSpecTest()
        {
            RunManager runM = new RunManager();
            runM.Initialize(GetRunSpec());
            PrivateObject obj = new PrivateObject(runM);
            RunSpec result;

            result = obj.Invoke("GetNextRun", DateTime.ParseExact("2019-08-08 08:28 00", "yyyy-MM-dd HH:mm tt", null)) as RunSpec;
            Assert.AreEqual("1", result.RunSpecId);

            result = obj.Invoke("GetNextRun", DateTime.ParseExact("2019-08-08 08:28 00", "yyyy-MM-dd HH:mm tt", null)) as RunSpec;
            Assert.AreEqual("1", result.RunSpecId);

            result = obj.Invoke("GetNextRun", DateTime.ParseExact("2019-08-08 08:28 00", "yyyy-MM-dd HH:mm tt", null)) as RunSpec;
            Assert.AreEqual("1", result.RunSpecId);

            result = obj.Invoke("GetNextRun", DateTime.ParseExact("2019-08-08 08:28 00", "yyyy-MM-dd HH:mm tt", null)) as RunSpec;
            Assert.AreEqual("1", result.RunSpecId);

            result = obj.Invoke("GetNextRun", DateTime.ParseExact("2019-08-08 08:28 00", "yyyy-MM-dd HH:mm tt", null)) as RunSpec;
            Assert.AreEqual("1", result.RunSpecId);

            result = obj.Invoke("GetNextRun", DateTime.ParseExact("2019-08-08 08:28 00", "yyyy-MM-dd HH:mm tt", null)) as RunSpec;
            Assert.AreEqual("1", result.RunSpecId);

        }

        public List<RunSpec> GetRunSpec()
        {
            List<RunSpec> runSpecs = new List<RunSpec>()
            {
                new RunSpec()
                {
                    RunSpecId = "1",
                    StartMessage = "This is 1",
                    RunActionMethod = null,
                    StartTime = DateTime.ParseExact("2019-08-08 08:30 00", "yyyy-MM-dd HH:mm tt", null),
                    EndTime = DateTime.ParseExact("2019-08-08 16:30 00", "yyyy-MM-dd HH:mm tt", null),
                    RunWeekdays = true,
                    Interval = 1
                },
                new RunSpec()
                {
                    RunSpecId = "2",
                    StartMessage = "This is 2.",
                    RunActionMethod = null,
                    StartTime = DateTime.ParseExact("2019-08-08 17:00 00", "yyyy-MM-dd HH:mm tt", null),
                    EndTime = DateTime.ParseExact("2019-08-09 07:00 00", "yyyy-MM-dd HH:mm tt", null),
                    RunWeekdays = true,
                    Interval = 2
                },
                new RunSpec()
                {
                    RunSpecId = "3",
                    StartMessage = "This is 3.",
                    RunActionMethod = null,
                    StartTime = DateTime.ParseExact("2019-08-08 07:10 00", "yyyy-MM-dd HH:mm tt", null),
                    EndTime = DateTime.Now,
                    RunWeekdays = true,
                    Interval = 2,
                    IsOneTimeTriger = true
                },
                new RunSpec()
                {
                    RunSpecId = "4",
                    StartMessage = "This is 4.",
                    RunActionMethod = null,
                    StartTime = DateTime.ParseExact("2019-08-08 07:30 00", "yyyy-MM-dd HH:mm tt", null),
                    EndTime = DateTime.ParseExact("2019-08-08 07:40 00", "yyyy-MM-dd HH:mm tt", null),
                    RunWeekdays = true,
                    Interval = 2
                }
            };
            return runSpecs;
        }
    }
}
