﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace StockWHelper
{
    public class RunHelper
    {
        public bool IsWithinTimeRange(DateTime startTime, DateTime endTime)
        {
            var now = DateTime.Now;
            return now > startTime && now < endTime;
        }
        public void Pause(int second)
        {
            DateTime now = DateTime.Now; 
            while ((DateTime.Now - now).TotalSeconds < second)
            {
                Thread.Sleep(100);
            }
        }
        public void PauseUntilStartTime(DateTime startTime)
        {
            while (DateTime.Now < startTime)
            {
                Pause(100);
            }
        }

        public bool IsRunningDay(RunSpec runSpec)
        {
            if(runSpec.RunWeekend)
            {
                switch(DateTime.Now.DayOfWeek)
                {
                    case DayOfWeek.Saturday:
                        return true;
                    case DayOfWeek.Sunday:
                        return true;
                }
            }

            if(runSpec.RunWeekdays)
            {
                switch (DateTime.Now.DayOfWeek)
                {
                    case DayOfWeek.Saturday:
                        return false;
                    case DayOfWeek.Sunday:
                        return false;
                    default:
                        return true;
                }
            }
            return false;
        }
    }
}
