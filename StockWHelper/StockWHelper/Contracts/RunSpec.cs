﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockWHelper
{
    public class RunSpec
    {
        public string RunSpecId { get; set; }
        public int Sequance { get; set; }
        public string StartMessage { get; set; }
        public string EndMessage { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public string Description { get; set; }
        public bool IsOneTimeTriger { get; set; }
        public bool RunWeekdays { get; set; }
        public bool RunWeekend { get; set; }
        public int Interval { get; set; }
        public Action RunActionMethod { get; set; }
    }
}
