﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StockWHelper.Contracts;

namespace StockWHelper
{
    //public delegate void TimeKeeperTask();

    public class RunManager
    {
        List<RunSpec> RunSpecList = new List<RunSpec>();
        RunHelper RunHelper = new RunHelper();
        Queue<RunSpec> RunSpecQueue = new Queue<RunSpec>();
        RunSpec CurrentRunSpec = null;
        public void Initialize(List<RunSpec> runSpecList)
        {
            Console.WriteLine("initialize");
            RunSpecList = runSpecList;
            InitialBuildQueue();
        }

        private RunSpec DequeueRunSpec()
        {
            RunSpec rs = RunSpecQueue.Dequeue();
            RunSpecQueue.Enqueue(rs);
            return rs;
        }

        private void InitialBuildQueue()
        {
            Console.WriteLine("initial build queue");
            RunSpecQueue.Clear();
            foreach(RunSpec rs in RunSpecList.OrderBy(x => x.Sequance))
            {
                RunSpecQueue.Enqueue(rs);
            }
            InitialRotateRunSpecQueue();
        }

        public void SyncRun()
        {
            Console.WriteLine("run");
            bool run = true;
            while(run)
            {
                Console.WriteLine("while run");
                var runSpec = DequeueRunSpec();
                ExecuteRunMethod(runSpec);
            }
        }
        
        private void InitialRotateRunSpecQueue()
        {
            Console.WriteLine("initial rotate queue");
            DateTime now = DateTime.Now;
            RunSpec lastRunSpec = null;
            TimeSpan longestTimeSpan = new TimeSpan();
            foreach(RunSpec rs in RunSpecList)
            {
                if(lastRunSpec == null)
                {
                    lastRunSpec = rs;
                }
                else
                {
                    DateTime startTime = new DateTime();
                    startTime.AddYears(now.Year);
                    startTime.AddMonths(now.Month);
                    startTime.AddDays(now.Day);
                    startTime.AddHours(rs.StartTime.Hour);
                    startTime.AddMinutes(rs.StartTime.Minute);

                    if(startTime < now)
                    {
                        startTime.AddDays(1);
                    }

                    TimeSpan ts = startTime.Subtract(now);
                    if(longestTimeSpan == null)
                    {
                        longestTimeSpan = ts;
                        lastRunSpec = rs;
                    }
                    else
                    {
                        if(ts > longestTimeSpan)
                        {
                            longestTimeSpan = ts;
                            lastRunSpec = rs;
                        }
                    }
                }
            }
            RunSpec next = DequeueRunSpec();
            while(next.Sequance != lastRunSpec.Sequance)
            {
                next = DequeueRunSpec();
            }
        }

        private void ExecuteRunMethod(RunSpec runSpec)
        {
            Console.WriteLine("execute run");
            if (!RunHelper.IsRunningDay(runSpec))
            {
                return;
            }
            try
            {
                Console.WriteLine("execute run try");
                if (runSpec.IsOneTimeTriger)
                {
                    runSpec.RunActionMethod();
                }
                else
                {
                    var now = DateTime.Now;
                    var startDateTime = new DateTime(now.Year, now.Month, now.Day, runSpec.StartTime.Hour, runSpec.StartTime.Minute, 0);
                    var endDateTime = new DateTime(now.Year, now.Month, now.Day, runSpec.EndTime.Hour, runSpec.EndTime.Minute, 0);

                    if (startDateTime > endDateTime)
                    {
                        Console.WriteLine("start day is grater than end day");
                        endDateTime = endDateTime.AddDays(1);
                    }

                    while (DateTime.Now <= endDateTime)
                    {
                        Console.WriteLine("It's before end time");
                        if (RunHelper.IsWithinTimeRange(startDateTime, endDateTime))
                        {
                            Console.WriteLine("within time range");
                            runSpec.RunActionMethod();
                        }
                        RunHelper.Pause(runSpec.Interval);
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}