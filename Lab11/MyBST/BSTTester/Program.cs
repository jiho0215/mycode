﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyBST;

namespace BSTTester
{
    class Program
    {
        static void Main(string[] args)
        {
            new Program().Run();
        }
        public void Run()
        {
            MyBST.BinarySearchTree<int> bsTree = new MyBST.BinarySearchTree<int>();
            //Test1
            Console.WriteLine("***Binary Search Tree Test.***");
            bsTree.Insert(20);
            bsTree.Insert(10);
            bsTree.Insert(30);
            bsTree.Insert(5);
            bsTree.Insert(25);
            bsTree.Insert(40);
            bsTree.Insert(50);
            bsTree.Insert(7);
            Console.Write("Return to list : ");
            bsTree.ToSortedList().ForEach(key => Console.Write("{0} ", key));

            Console.WriteLine("\n\nChanged key 40 to 4");
            bsTree.Root.Right.Right.Key = 4;
            Console.WriteLine("Is BST? : {0}", BinarySearchTree<int>.IsBST(bsTree.Root));
            Console.WriteLine("Changed key 4 to 40");
            bsTree.Root.Right.Right.Key = 40;
            Console.WriteLine("Is BST? : {0}\n", BinarySearchTree<int>.IsBST(bsTree.Root));
            //bsTree.Root.Left.Left.Right.Key = 4;

            Console.WriteLine("Search 100: {0}", bsTree.Search(100));
            Console.WriteLine("Search 50: {0}\n", bsTree.Search(50));

            //Remove Root
            bsTree.Remove(20);
            Console.WriteLine("Removed Root(20). Now root key is : {0}\n", bsTree.Root.Key);

            Console.Write("Return to list : ");
            bsTree.ToSortedList().ForEach(key => Console.Write("{0} ", key));
            Console.WriteLine();

            bsTree.Remove(30);
            Console.WriteLine("Removed Root(30).");

            Console.Write("Return to list : ");
            bsTree.ToSortedList().ForEach(key => Console.Write("{0} ", key));
            Console.WriteLine();
            //End Test1


            Console.WriteLine("\n***BTS Test.***\n");
            BST<int, string> bst = new BST<int, string>();
            BSTNode<int, string> node0 = new BSTNode<int, string>() { Key = 20, Value = "Twenty" };
            BSTNode<int, string> node1 = new BSTNode<int, string>() { Key = 10, Value = "Ten" };
            BSTNode<int, string> node2 = new BSTNode<int, string>() { Key = 30, Value = "Thirty" };
            BSTNode<int, string> node3 = new BSTNode<int, string>() { Key = 5, Value = "Five" };
            BSTNode<int, string> node4 = new BSTNode<int, string>() { Key = 25, Value = "Twentyfive" };
            BSTNode<int, string> node5 = new BSTNode<int, string>() { Key = 40, Value = "Fourty" };
            BSTNode<int, string> node6 = new BSTNode<int, string>() { Key = 50, Value = "Fifty" };
            BSTNode<int, string> node7 = new BSTNode<int, string>() { Key = 7, Value = "Seven" };
            bst.Insert(node0);
            bst.Insert(node1);
            bst.Insert(node2);
            bst.Insert(node3);
            bst.Insert(node4);
            bst.Insert(node5);
            bst.Insert(node6);
            bst.Insert(node7);


            if (bst.Search(10) == null)
            {
                Console.WriteLine("There is no node with the key.");
            }
            else
            {
                Console.WriteLine("The node's value is : {0}", bst.Search(10).Key);
            }

            Console.WriteLine("Delete node with key 10.");
            bst.Remove(node1);

            if (bst.Search(10) == null)
            {
                Console.WriteLine("There is no node with the key.");
            }
            else
            {
                Console.WriteLine("The node's value is : {0}", bst.Search(10).Key);
            }

            bst.Remove(node2);

            Console.ReadKey();
        }
    }
}
