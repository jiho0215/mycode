﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyBST
{
    public class BST<Tkey, Tvalue> where Tkey : IComparable<Tkey>
    {
        private BSTNode<Tkey, Tvalue> Root;
        public BST() { }
        public void Insert(BSTNode<Tkey, Tvalue> newNode)
        {
            if (Root == null)
            {
                Root = newNode;
            }
            else
            {
                Insert(Root, newNode);
            }
        }
        private void Insert(BSTNode<Tkey, Tvalue> node, BSTNode<Tkey, Tvalue> newNode)
        {
            int compare = newNode.Key.CompareTo(node.Key);
            if (compare < 0)
            {
                if (node.Left == null)
                {
                    node.Left = newNode;
                }
                else
                {
                    Insert(node.Left, newNode);
                }
            }
            else if (compare > 0)
            {
                if (node.Right == null)
                {
                    node.Right = newNode;
                }
                else
                {
                    Insert(node.Right, newNode);
                }
            }
            else
            {
                throw new Exception("Duplicated value is not allowed.");
            }
        }
        public void Remove(BSTNode<Tkey, Tvalue> target)
        {
            if (target.Key.CompareTo(Root.Key) == 0)
            {
                RemoveRoot(target);
            }
            BSTNode<Tkey, Tvalue> curr = Root;
            while (curr != null)
            {
                if (curr.Left != null && target.Key.CompareTo(curr.Left.Key) == 0)
                {
                    Remove(curr, curr.Left, target.Key);
                    return;
                }
                if (curr.Right != null && target.Key.CompareTo(curr.Right.Key) == 0)
                {
                    Remove(curr, curr.Right, target.Key);
                    return;
                }
                curr = (target.Key.CompareTo(curr.Key) < 0) ? curr.Left : curr.Right;
            }
        }
        private void RemoveRoot(BSTNode<Tkey, Tvalue> target)
        {
            if (Root.Left == null && Root.Right == null)
            {
                Root = null;
            }
            else if (Root.Right == null)
            {
                Root = Root.Left;
            }
            else if (Root.Left == null)
            {
                Root = Root.Right;
            }
            else
            {
                Remove(null, Root, target.Key);
            }
        }
        private void Remove(BSTNode<Tkey, Tvalue> parent, BSTNode<Tkey, Tvalue> target, Tkey key)
        {
            if (target.Right == null)
            {
                if (parent.Left != null && key.CompareTo(parent.Left.Key) == 0)
                {
                    parent.Left = target.Left;
                }
                else
                {
                    parent.Right = target.Left;
                }
            }
            else if (target.Right != null && target.Left == null)
            {
                if (parent.Left != null && key.CompareTo(parent.Left.Key) == 0)
                {
                    parent.Left = target.Right;
                }
                else
                {
                    parent.Right = target.Right;
                }
            }
            else
            {
                BSTNode<Tkey, Tvalue> temp = GetNodeSmallestKey(target.Right);
                Remove(temp);
                target.Key = temp.Key;
                target.Value = temp.Value;
            }
        }
        private BSTNode<Tkey, Tvalue> GetNodeSmallestKey(BSTNode<Tkey, Tvalue> curr)
        {
            while (curr.Left != null)
            {
                curr = curr.Left;
            }
            return curr;
        }
        public BSTNode<Tkey, Tvalue> Search(Tkey Key)
        {
            return Search(Root, Key);
        }
        
        private BSTNode<Tkey, Tvalue> Search(BSTNode<Tkey, Tvalue> curr, Tkey key)
        {
            while (curr != null)
            {
                int compare = key.CompareTo(curr.Key);
                if (compare == 0)
                {
                    return curr;
                }
                curr = (compare > 0) ? curr.Right : curr.Left;
            }
            return null;
        }
    }
}
