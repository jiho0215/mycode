﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyBST
{
    //public for IsBST() test
    public class Node<T>
    {
        //public for IsBST() test
        public T Key { get; set; }
        public Node<T> Left { get; set; }
        public Node<T> Right { get; set; }
        internal Node() { }
        internal Node(T key)
        {
            this.Key = key;
        }
    }
}
