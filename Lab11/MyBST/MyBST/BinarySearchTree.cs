﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyBST
{
    public class BinarySearchTree<T> where T : IComparable<T>
    {
        //public for IsBST() test
        public Node<T> Root;
        public BinarySearchTree() { }
        public BinarySearchTree(T Key)
        {
            Root = new Node<T>();
            Root.Key = Key;
        }
        public void Insert(T newKey)
        {
            if (Root == null)
            {
                Root = new Node<T>(newKey);
            }
            else
            {
                Insert(Root, newKey);
            }
        }
        private void Insert(Node<T> node, T newKey)
        {
            int compare = newKey.CompareTo(node.Key);
            if (compare < 0)
            {
                if (node.Left == null)
                {
                    node.Left = new Node<T>(newKey);
                }
                else
                {
                    Insert(node.Left, newKey);
                }
            }
            else if (compare > 0)
            {
                if (node.Right == null)
                {
                    node.Right = new Node<T>(newKey);
                }
                else
                {
                    Insert(node.Right, newKey);
                }
            }
            else
            {
                throw new Exception("Duplicated value is not allowed.");
            }
        }
        public void Remove(T key)
        {
            if (key.CompareTo(Root.Key) == 0)
            {
                RemoveRoot(key);
            }
            Node<T> curr = Root;
            while (curr != null)
            {
                if (curr.Left != null && key.CompareTo(curr.Left.Key) == 0)
                {
                    Remove(curr, curr.Left, key);
                    return;
                }
                if (curr.Right != null && key.CompareTo(curr.Right.Key) == 0)
                {
                    Remove(curr, curr.Right, key);
                    return;
                }
                curr = (key.CompareTo(curr.Key) < 0) ? curr.Left : curr.Right;
            }
        }
        private void RemoveRoot(T key)
        {
            if (Root.Left == null && Root.Right == null)
            {
                Root = null;
            }
            else if (Root.Right == null)
            {
                Root = Root.Left;
            }
            else if (Root.Left == null)
            {
                Root = Root.Right;
            }
            else
            {
                Remove(null, Root, key);
            }
        }
        private void Remove(Node<T> parent, Node<T> target, T key)
        {
            if (target.Right == null)
            {
                if (parent.Left != null && key.CompareTo(parent.Left.Key) == 0)
                {
                    parent.Left = target.Left;
                }
                else
                {
                    parent.Right = target.Left;
                }
            }
            else if (target.Right != null && target.Left == null)
            {
                if (parent.Left != null && key.CompareTo(parent.Left.Key) == 0)
                {
                    parent.Left = target.Right;
                }
                else
                {
                    parent.Right = target.Right;
                }
            }
            else
            {
                T temp = GetSmallestKey(target.Right);
                Remove(temp);
                target.Key = temp;
            }
        }
        private T GetSmallestKey(Node<T> curr)
        {
            while (curr.Left != null)
            {
                curr = curr.Left;
            }
            return curr.Key;
        }
        public bool Search(T Key)
        {
            return Search(Root, Key);
        }
        private bool Search(Node<T> curr, T key)
        {
            while(curr != null)
            {
                int compare = key.CompareTo(curr.Key);
                if (compare == 0)
                {
                    return true;
                }
                curr = (compare > 0) ? curr.Right : curr.Left;
            }
            return false;
        }
        public List<T> ToSortedList()
        {
            List<T> sortedList = new List<T>();
            ToSortedList(Root, sortedList);
            return sortedList;
        }
        private void ToSortedList(Node<T> node, List<T> sortedList)
        {
            if (node == null)
            {
                return;
            }
            ToSortedList(node.Left, sortedList);
            sortedList.Add(node.Key);
            ToSortedList(node.Right, sortedList);
        }
        public static bool IsBST(Node<T> root)
        {
            List<T> list = new List<T>();
            return IsBST(root, list);
        }
        private static bool IsBST(Node<T> curr, List<T> list)
        {
            if (curr == null)
            {
                return true;
            }

            //Inorder Traversal
            if (!IsBST(curr.Left, list))
            {
                return false;
            }

            if (list.Count > 0)
            {
                if (list[0].CompareTo(curr.Key) >= 0)
                {
                    return false;
                }
                list[0] = curr.Key;
            }
            else
            {
                list.Add(curr.Key);
            }

            if (!IsBST(curr.Right, list))
            {
                return false;
            }

            return true;
        }
        public static bool IsBST(Node<int> root)
        {
            return IsBST(root, int.MinValue, int.MaxValue);
        }
        private static bool IsBST(Node<int> curr, int min, int max)
        {
            if (curr == null)
            { return true; }
            if (curr.Key < min || curr.Key > max)
            { return false; }
            return (IsBST(curr.Left, min, curr.Key - 1) && IsBST(curr.Right, curr.Key + 1, max));
        }
    }
}
