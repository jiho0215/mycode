﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyBST
{
    public class BSTNode<Tkey, Tvalue>
    {
        public BSTNode() { }
        public BSTNode<Tkey, Tvalue> Left { get; set; }
        public BSTNode<Tkey, Tvalue> Right { get; set; }
        public Tkey Key { get; set; }
        public Tvalue Value { get; set; }
    }
}
