﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;
using System.Diagnostics;
using System.Configuration;
using System.Threading;
namespace Logger
{
    class JsonLogger : ILogger
    {
        public static readonly object objLock = new object();
        public override void WriteLog(string LogType, string LogMsg)
        {
            JsonLog jsonobj = new JsonLog(LogType, LogMsg);
            string json = JsonConvert.SerializeObject(jsonobj);
            string directory = Path.GetDirectoryName(LoggerTarget);
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }

            try
            {
                lock (objLock)
                {
                    using (StreamWriter writer = new StreamWriter(LoggerTarget, true))
                    { 
                        writer.WriteLine(json);
                    }
                }
            }
            catch (IOException ioe)
            {
                Console.WriteLine("File writer error: " + ioe.Message);
            }
            catch(Exception e)
            {
                Console.WriteLine("Failed to write a log" + e.Message);
            }
        }
    }
}
