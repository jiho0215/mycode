﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace Logger
{
    public class ILogger
    {
        public string LoggerTarget
        {
            get
            {
                return ConfigurationManager.AppSettings["LoggerTarget"];
            }
        }
        public virtual void WriteLog(string logtype, string logmsg){}
    }
}
