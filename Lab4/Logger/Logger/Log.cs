﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Diagnostics;

namespace Logger
{
    public enum ELogType { Error, Warning , Info, Debug}
    public static class Log
    {
        public static void WriteLog(ELogType logtype, string logmsg)
        {
            GetLogger().WriteLog(logtype.ToString(), logmsg);
        }
        static ILogger GetLogger()
        {
            string LoggerType = ConfigurationManager.AppSettings["LoggerType"];
            switch (LoggerType)
            {
                case "DbLogger":
                    return new DbLogger();
                case "CsvLogger":
                    return new CsvLogger();
                case "JsonLogger":
                    return new JsonLogger();
                default:
                    return new JsonLogger();
            }
        }
    }
}
