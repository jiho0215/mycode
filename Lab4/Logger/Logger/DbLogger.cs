﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Data.OracleClient;
using System.Data;
using System.Diagnostics;
using System.Configuration;

namespace Logger
{
    class DbLogger : ILogger
    {
        public override void WriteLog(string LogType, string LogMsg)
        {
            try
            {
                using (IDbConnection dbCon = GetDbConnection())
                {
                    using (IDbCommand dbcmd = dbCon.CreateCommand())
                    {
                        dbcmd.CommandText = @"Insert Into LogTable (LogType, LogMessage, LogTime) values(@logtype, @logmsg, @logtime)";
                        AddParameter(dbcmd, "@logtype", LogType);
                        AddParameter(dbcmd, "@logmsg", LogMsg);
                        AddParameter(dbcmd, "@logtime", DateTime.Now.ToString());
                        dbcmd.Connection.Open();
                        dbcmd.ExecuteNonQuery();
                    }
                }
            }
            catch (SqlException sqlex)
            {
                Console.WriteLine("SQL Exception. : " + sqlex.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine("Use DBLogger Failed. : " + e.Message);
            }
        }

        private IDbConnection GetDbConnection()
        {
            IDbConnection dbConn = null;
            switch (ConfigurationManager.AppSettings["DbType"])
            {
                case "SQLServer":
                    dbConn = new SqlConnection(LoggerTarget);
                    break;
                case "Oracle":
                    dbConn = new OracleConnection(LoggerTarget);
                    break;
                case "OleDB":
                    dbConn = new OleDbConnection(LoggerTarget);
                    break;
                default:
                    break;
            }
            return dbConn;
        }

        private void AddParameter(IDbCommand dbcmd, string paraName, string paraValue)
        {
            var parameter = dbcmd.CreateParameter();
            parameter.ParameterName = paraName;
            parameter.Value = paraValue;
            dbcmd.Parameters.Add(parameter);
        }
    }
}
