﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Configuration;
using System.IO;
using System.Net;

namespace Logger
{
    class CsvLogger : ILogger
    {
        public override void WriteLog(string LogType, string LogMsg)
        {
            string LogData = LogType + "," + LogMsg + "," + DateTime.Now.ToString() + Environment.NewLine;
            string userName = ConfigurationManager.AppSettings["UserId"];
            string password = ConfigurationManager.AppSettings["Password"];
            Uri serverUri = new Uri(LoggerTarget);
            if (serverUri.Scheme != Uri.UriSchemeFtp)
            {
                Console.WriteLine("Fail to save file to ftp.");
                return;
            }
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(serverUri);
            request.Method = WebRequestMethods.Ftp.AppendFile;
            byte[] fileContents = Encoding.UTF8.GetBytes(LogData.ToCharArray());
            try
            { 
                request.Credentials = new NetworkCredential(userName, password);
                request.ContentLength = fileContents.Length;
                using (Stream requestStream = request.GetRequestStream())
                {
                    requestStream.Write(fileContents, 0, fileContents.Length);
                }
            }
            catch (WebException webex)
            {
                Console.WriteLine("Web access error occured.: {0}", webex.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine("Failed to write a log on the ftp server." + e.Message);
            }
        }
        public string Test { get; set; }
    }
}