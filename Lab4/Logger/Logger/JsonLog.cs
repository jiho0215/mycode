﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logger
{
    public class JsonLog
    {
        public string LogType { get; }
        public string LogMsg { get; }
        public string LogTime { get; set; }
        public JsonLog(string jlogtype, string jlogmsg)
        {
            LogType = jlogtype;
            LogMsg = jlogmsg;
            LogTime = DateTime.Now.ToString();
        }
    }
}
