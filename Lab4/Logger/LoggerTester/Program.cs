﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Logger;
using System.Threading;

namespace LoggerTester
{
    class Program
    {
        static void Main(string[] args)
        {
            string msg = "Log Message YYYes";
            Log.WriteLog(ELogType.Warning, msg);
            //Thread t1 = new Thread(MethodOne);
            //Thread t2 = new Thread(MethodTwo);
            //Thread t3 = new Thread(MethodThree);
            //Thread t4 = new Thread(MethodFour);
            //t1.Start();
            //t2.Start();
            //t3.Start();
            //t4.Start();
            Console.ReadKey();
        }
        static void MethodOne()
        {
            for (int i = 0; i < 10; i++)
            {
                Log.WriteLog(ELogType.Info, i + " Method Two Message");
            }
            Console.WriteLine("Method One is done");
        }
        static void MethodTwo()
        {
            for (int i = 0; i < 10; i++)
            {
                Log.WriteLog(ELogType.Error, i + " Method Two Message");
            }
            Console.WriteLine("Method Two is done");
        }
        static void MethodThree()
        {
            for (int i = 0; i < 10; i++)
            {
                Log.WriteLog(ELogType.Warning, i + " Method Three Message");
            }
            Console.WriteLine("Method Three is done");
        }
        static void MethodFour()
        {
            for (int i = 0; i < 10; i++)
            {
                Log.WriteLog(ELogType.Debug, i + " Method Four Message");
            }
            Console.WriteLine("Method Four is done");
        }
    }
    }
