﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCPractice.Models
{
    public class Employee
    {
        public string FistName { get; set; }
        public string LastName { get; set; }
        public double Salary { get; set; }
    }
}