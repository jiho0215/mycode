﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCPractice.Controllers
{
    public class ViewModelController : Controller
    {
        // GET: ViewModel
        public ActionResult MyViewModel()
        {
            return View();
        }
    }
}