﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVCPractice.Models;

namespace MVCPractice.Controllers
{
    public class MyViewDataController : Controller
    {
        // GET: MyPage
        public ActionResult MyViewData()
        {
            Employee em = new Employee();
            em.FistName = "jiho";
            em.LastName = "lee";
            em.Salary = 42000;
            ViewData["Jiho"] = em;
            return View("MyViewData");
        }
    }
}