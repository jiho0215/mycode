﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVCPractice.Models;

namespace MVCPractice.Controllers
{
    public class StronglyTypedViewController : Controller
    {
        // GET: StronglyTypedView
        public ActionResult STV()
        {
            Employee emp = new Employee();
            emp.FistName = "Jin";
            emp.LastName = "Kim";
            emp.Salary = 60000;

            ViewBag.Employee = emp;
            return View("STV", emp);
        }
    }
}