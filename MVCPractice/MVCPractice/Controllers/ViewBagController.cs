﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVCPractice.Models;

namespace MVCPractice.Controllers
{
    public class ViewBagController : Controller
    {
        // GET: ViewBag
        public ActionResult MyViewBag()
        {
            Employee em1 = new Employee();
            em1.FistName = "Jun";
            em1.LastName = "Kim";
            em1.Salary = 50000;

            ViewBag.Employee = em1;

            return View("MyViewBag");
        }
    }
}