﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyStack
{
    public class SLLStack<T> : IMyStack<T>
    { 
        private Node<T> head;
        private int count;
        public SLLStack() { }
        public SLLStack(T input)
        {
            head = new Node<T>();
            head.Data = input;
            count++;
        }
        public void Push(T input)
        {
            Node<T> newNode = new Node<T>(input);
            if (head == null)
            {
                head = newNode;
                count++;
                return;
            }
            newNode.Next = head;
            head = newNode;
            count++;
        }
        public T Pop()
        {
            if (head == null)
            {
                throw new NullReferenceException("Null array.");
            }
            T output = head.Data;
            head = head.Next;
            count--;
            return output;
        }
        public T Peek()
        {
            if (head == null)
            {
                throw new NullReferenceException("Null array.");
            }
            return head.Data;
        }
        public int Count()
        {
            return count;
        }
        public bool IsEmpty()
        {
            return (count == 0);
        }
        public bool IsCircular()
        {
            if (head.Next == null)
            {
                return false;
            }
            Node<T> curr1 = head;
            Node<T> curr2 = head.Next;
            while (curr2.Next != null)
            {
                if (curr1.Equals(curr2))
                {
                    return true;
                }
                if (curr2.Next.Next == null)
                {
                    return false;
                }
                curr1 = curr1.Next;
                curr2 = curr2.Next.Next;
            }
            return false;
        }
    }
}
