﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyStack
{
    public enum NodeType { FLA, SLLNode };
    public interface IMyStack<T>
    {
        void Push(T input);
        T Pop();
        T Peek();
        int Count();
        bool IsEmpty();
    }
    public class StackFactory
    {
        
        public static IMyStack<T> Stack<T>(NodeType nodeType)
        {
            IMyStack<T> stack;
            switch (nodeType)
            {
                case NodeType.SLLNode:
                    return stack = new SLLStack<T>();
                case NodeType.FLA:
                    return stack = new FLAStack<T>();
                default:
                    throw new Exception("Stack Type Error.");
            }
        }
    }
}
