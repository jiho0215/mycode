﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyStack
{
    public class Node<T>
    {
        internal Node<T> Next { get; set; }
        internal T Data { get; set; }
        internal Node() { }
        internal Node(T input)
        {
            Data = input;
        }
    }
}
