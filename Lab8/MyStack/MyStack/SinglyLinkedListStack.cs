﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyStack
{
    public class SinglyLinkedListStack<T>
    {
        private Node<T> head;
        public SinglyLinkedListStack() { }
        public SinglyLinkedListStack(Node<T> input)
        {
            head = input;
        }

        public void Push(Node<T> input)
        {
            input.Next = head;
            head = input;
        }
        public Node<T> Pop()
        {
            Node<T> output = head;
           head = head.Next;
            return output;
        }
        public Node<T> Peek()
        {
            return head;
        }
        public Boolean IsEmpty()
        {
            return (head == null);
        }
        public int Count()
        {
            int count = 0;
            Node<T> curr = head;
            while(curr.Next != null)
            {
                curr = curr.Next;
                count++;
            }
            return count;
        }
        public bool IsCircular()
        {
            if (head.Next == null)
            {
                return false;
            }
            Node<T> curr1 = head;
            Node<T> curr2 = head.Next;
            while (curr2.Next != null)
            {
                if (curr1.Equals(curr2))
                {
                    return true;
                }
                if (curr2.Next.Next == null)
                {
                    return false;
                }
                curr1 = curr1.Next;
                curr2 = curr2.Next.Next;
            }
            return false;
        }
    }
}
