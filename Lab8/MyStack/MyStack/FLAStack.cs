﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyStack
{
    public class FLAStack<T> : IMyStack<T>
    {
        private int lastArray = -1;
        private T[] arr = new T[16];
        public void Push(T input)
        {
            lastArray++;
            arr[lastArray] = input;
        }
        public T Pop()
        {
            if (lastArray == -1)
            {
                throw new Exception("Null array.");
            }
            T output = arr[lastArray];
            arr[lastArray] = default(T);
            lastArray--;
            return output;
        }
        public T Peek()
        {
            if (lastArray == -1)
            {
                throw new Exception("Null array.");
            }
            return arr[lastArray];
        }
        public int Count()
        {
            return lastArray + 1;
        }
        public bool IsEmpty()
        {
            return (lastArray == -1);
        }
    }
}
