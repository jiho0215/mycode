﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyStack;

namespace StackPractice
{
    class Program
    {
        static void Main(string[] args)
        {
            //SinglyLinkedListStack<string> stack = new SinglyLinkedListStack<string>();
            //stack.Push(new Node<string>("one"));
            //stack.Push(new Node<string>("two"));
            //stack.Push(new Node<string>("three"));
            //stack.Push(new Node<string>("four"));

            //Console.WriteLine("Is stack empty? {0}, Count is : {1}\n", stack.IsEmpty(), stack.Count());

            //Console.WriteLine("Peek() : {0}", stack.Peek().data);
            //Console.WriteLine("Peek() : {0}", stack.Peek().data);
            //Console.WriteLine("Peek() : {0}", stack.Peek().data);

            //Console.WriteLine("\nIs stack empty? {0}, Count is : {1}\n", stack.IsEmpty(), stack.Count());

            //Console.WriteLine("Pop() : {0}", stack.Pop().data);
            //Console.WriteLine("Pop() : {0}", stack.Pop().data);
            //Console.WriteLine("Pop() : {0}", stack.Pop().data);

            //Console.WriteLine("\nIs stack empty? {0}, Count is : {1}\n", stack.IsEmpty(), stack.Count());

            //Console.WriteLine("Pop() : {0}", stack.Pop().data);
            ////Console.WriteLine("\nPop() : {0}", stack.Pop().data);

            //Console.WriteLine("\nIs stack empty? {0}, Count is : {1}\n", stack.IsEmpty(), stack.Count());


            //*******************************************
            //*******Fixed Length Array Stack Test*******

            //Console.WriteLine("\n*******Fixed Length Array Stack Test*******\n");
            //FixedLengthArrayStack<int> arr = new FixedLengthArrayStack<int>();
            //arr.Push(1);
            //arr.Push(2);
            //arr.Push(3);
            //arr.Push(4);
            ////arr.Push(5);

            //Console.WriteLine("Peek(): " + arr.Peek());
            //Console.WriteLine("Peek(): " + arr.Peek());
            //Console.WriteLine("Peek(): " + arr.Peek());
            //Console.WriteLine("\nIs stack empty? {0}, Count is : {1}\n", arr.IsEmpty(), arr.Count());
            //Console.WriteLine("Pop(): " + arr.Pop());
            //Console.WriteLine("Pop(): " + arr.Pop());
            //Console.WriteLine("Pop(): " + arr.Pop());
            //Console.WriteLine("\nIs stack empty? {0}, Count is : {1}\n", arr.IsEmpty(), arr.Count());
            //Console.WriteLine("Pop(): " + arr.Pop());
            ////Console.WriteLine(arr.Pop());

            ////Console.WriteLine("\nIs stack empty? {0}, Count is : {1}\n", arr.IsEmpty(), arr.Count());

            Console.WriteLine("\n*******Interfaced Fixed Length Array Stack Test*******\n");
            IMyStack<string> stack = StackFactory.Stack<string>(NodeType.SLLNode);
            stack.Push("One");
            stack.Push("Two");
            stack.Push("Three");
            stack.Push("Four");

            SLLStack<string> sss = stack as SLLStack<string>;
            Console.WriteLine("Is Circular? : {0}\n", sss.IsCircular());

            Console.WriteLine("Peek(): " + stack.Peek());
            Console.WriteLine("Peek(): " + stack.Peek());
            Console.WriteLine("Peek(): " + stack.Peek());
            Console.WriteLine("\nIs stack empty? {0}, Count is : {1}\n", stack.IsEmpty(), stack.Count());
            Console.WriteLine("Pop(): " + stack.Pop());
            Console.WriteLine("Pop(): " + stack.Pop());
            Console.WriteLine("Pop(): " + stack.Pop());
            Console.WriteLine("\nIs stack empty? {0}, Count is : {1}\n", stack.IsEmpty(), stack.Count());
            Console.WriteLine("Pop(): " + stack.Pop());

            Console.WriteLine("\nIs stack empty? {0}, Count is : {1}\n", stack.IsEmpty(), stack.Count());

            



            Console.ReadKey();

        }
    }
}
