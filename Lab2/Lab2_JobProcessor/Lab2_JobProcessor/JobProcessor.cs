﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace Lab2_JobProcessor
{
    delegate void JobProcessorEventHandler();
    
    class JobProcessor
    {
        public event EventHandler JobStartEvent;
        public event JobProcessorEventHandler JobCloseEvent;
        public event EventHandler<JobProgressArgs> JobProgressEvent;

        public string JobName { get; }
        public int JobTimeOut { get; }
        public string JobCmd { get; }
        public int JobPriority
        {
            get
            {
                return JobCore.Priority;
            }
            set
            {
                JobCore.Priority = value;
            }
        }
        public JobProcessor(string JobName, string JobCmd, int JobPriority, int JobTimeout = 30000)
        {
            this.JobName = JobName;
            this.JobTimeOut = JobTimeOut;
            this.JobCmd = JobCmd;
            this.JobPriority = JobPriority;
        }

        public void Run()
        {
            ShowJobStarted();
            ShowJobProgress(0);
            JobCore.InitializeJob(JobCmd);
            ShowJobProgress(20);
            JobCore.PreJob();
            ShowJobProgress(40);
            JobCore.MainJob();
            ShowJobProgress(60);
            JobCore.PostJob();
            ShowJobProgress(80);
            JobCore.CloseJob();
            ShowJobProgress(100);
            ShowJobClosed();
        }
        void ShowJobStarted()
        {
            OnStartEvent();
        }
        void ShowJobClosed()
        {
            OnCloseEvent();
        }
        void ShowJobProgress(int prog)
        {
            OnProgressEvent(new JobProgressArgs(prog));
        }
        protected virtual void OnStartEvent()
        {
            //Why copy to a new instanse?
            EventHandler JobStartHandler = JobStartEvent;
            if (JobStartHandler != null)
            {
                JobStartHandler(this, new EventArgs());
            }
        }
        protected virtual void OnProgressEvent(JobProgressArgs e)
        {
            EventHandler<JobProgressArgs> ProgressHandler = JobProgressEvent;
            if (ProgressHandler != null)
            {
                ProgressHandler(this, e);
            }
        }
        protected virtual void OnCloseEvent()
        {
            JobProcessorEventHandler JobCloseHandler = JobCloseEvent;
            if (JobCloseHandler != null)
            {
                JobCloseHandler();
            }
        }
    }
    class JobProgressArgs : EventArgs
    {
        public int Progress { get; set; }
        public JobProgressArgs(int progress)
        {
            this.Progress = progress;
        }
    }
}
