﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2_JobProcessor
{
    class Program
    {
        static JobProcessor JP;

        static void Main()
        {
            JP = new JobProcessor("JobNmae", "JobCmd", 2);
            JP.JobStartEvent += new EventHandler(JobStartNotify);
            //JP.JobStartEvent += JobStartNotify;
            JP.JobCloseEvent += new JobProcessorEventHandler(JobCloseNotify);
            //JP.JobProgressEvent += JobProcessNotify;
            JP.JobProgressEvent += new EventHandler<JobProgressArgs>(JobProcessNotify);
            JP.Run();
            Console.ReadKey();
        }
        static void JobStartNotify(object sener, EventArgs e)
        {
            Console.WriteLine("Job Processor is started.");
        }
        static void JobCloseNotify()
        {
            Console.WriteLine("Job Processor is Completed.");
        }
        static void JobProcessNotify(object sender, JobProgressArgs e)
        {
            Console.WriteLine(e.Progress + "% is completed...");
        }
    }

}
