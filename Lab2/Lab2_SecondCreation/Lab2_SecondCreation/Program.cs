﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2_SecondCreation
{
    class Program
    {
        static void Main(string[] args)
        {
            JobProcessor JP = new JobProcessor();
            JP.EventJobStart += new EventDelegate(StartNoti);
            JP.EventJobProcess += new EventDelegate(ProgressNoti);
            JP.EventJobClose += new EventDelegate(CompleteNoti);
            JP.Run();

        }
        static void StartNoti()
        {
            Console.WriteLine("Job Started");
        }
        static void ProgressNoti()
        {
            Console.WriteLine("% is done...");
        }
        static void CompleteNoti()
        {
            Console.WriteLine("Job Completed");
        }
    }
}
