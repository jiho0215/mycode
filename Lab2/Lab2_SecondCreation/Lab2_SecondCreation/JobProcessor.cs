﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace Lab2_SecondCreation
{
    public delegate void EventDelegate();
    public enum JobPriority { Top = 0, High = 1, Normal = 2, Low = 3 }
    class JobProcessor
    {
        public string cmd { get; }
        public string JobName { get; }
        [System.ComponentModel.DefaultValue(30000)]
        public int Timeout { get; }
       

        internal EventDelegate EventJobStart;
        internal EventDelegate EventJobProcess;
        internal EventDelegate EventJobClose;
        void EventJobStarter()
        {
            if (EventJobStart != null)
                EventJobStart();
        }
        void EventJobProcesser()
        {
            if (EventJobProcess != null)
                EventJobProcess();
        }
        void EventJobCloser()
        {
            if (EventJobClose != null)
                EventJobClose();
        }

        public void Run()
        {
            EventJobStarter();
            EventJobProcesser();
            JobCore.InitializeJob(cmd);
            JobCore.PreJob();
            JobCore.MainJob();
            JobCore.PostJob();
            JobCore.CloseJob();
            EventJobCloser();
        }
        public void SetPriority(int i)
        {

        }
    }
}
