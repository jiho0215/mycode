﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuickSortTester
{
    class Employee : IComparable<Employee>
    {
        public Employee() { }
        public Employee(string id, int age, double salary)
        {
            ID = id;
            Age = age;
            Salary = salary;
        }
        public string ID { get; set; }
        public int Age { get; set; }
        public double Salary { get; set; }

        public int CompareTo(Employee other)
        {
            return this.ID.CompareTo(other.ID);
        }
        #region <<<IComparer Definitions>>>
        public static IDDesc IComparerIDDesc()
        {
            return new IDDesc();
        }
        public class IDDesc : IComparer<Employee>
        {
            public int Compare(Employee x, Employee y)
            {
                if (x.ID.CompareTo(y.ID) > 0) return -1;
                if (x.ID.CompareTo(y.ID) < 0) return 1;
                return 0; 
            }
        }

        public static AgeAsc IComparerAgeAsc()
        {
            return new AgeAsc();
        }
        public class AgeAsc : IComparer<Employee>
        {
            public int Compare(Employee x, Employee y)
            {
                return x.Age.CompareTo(y.Age);
            }
        }
       
        public static AgeDesc IComparerAgeDesc()
        {
            return new AgeDesc();
        }
        public class AgeDesc : IComparer<Employee>
        {
            public int Compare(Employee x, Employee y)
            {
                if (x.Age.CompareTo(y.Age) > 0) return -1;
                if (x.Age.CompareTo(y.Age) < 0) return 1;
                return 0;
            }
        }

        public static SalaryAsc IComparerSalaryAsc()
        {
            return new SalaryAsc();
        }
        public class SalaryAsc : IComparer<Employee>
        {
            public int Compare(Employee x, Employee y)
            {
                return x.Salary.CompareTo(y.Salary);
            }
        }

        public static SalaryDesc IComparerSalaryDesc()
        {
            return new SalaryDesc();
        }
        public class SalaryDesc : IComparer<Employee>
        {
            public int Compare(Employee x, Employee y)
            {
                if (x.Salary.CompareTo(y.Salary) > 0) return -1;
                if (x.Salary.CompareTo(y.Salary) < 0) return 1;
                return 0;
            }
        }
        #endregion <<<IComparer Definitions>>>
        #region <<<Comparison Definitions>>>
        public static int ComparisonIDDesc(Employee x, Employee y)
        {
            if (x.ID.CompareTo(y.ID) > 0) return -1;
            if (x.ID.CompareTo(y.ID) < 0) return 1;
            return 0;
        }

        public static int ComparisonAgeAsc(Employee x, Employee y)
        {
            return x.Age.CompareTo(y.Age);
        }
        
        public static int ComparisonAgeDesc(Employee x, Employee y)
        {
            if (x.Age.CompareTo(y.Age) > 0) return -1;
            if (x.Age.CompareTo(y.Age) < 0) return 1;
            return 0;
        }

        public static int ComparisonSalaryAsc(Employee x, Employee y)
        {
            return x.Salary.CompareTo(y.Salary);
        }

        public static int ComparisonSalaryDesc(Employee x, Employee y)
        {
            if (x.Salary.CompareTo(y.Salary) > 0) return -1;
            if (x.Salary.CompareTo(y.Salary) < 0) return 1;
            return 0;
        }
        #endregion <<<Comparison Definition>>>

    }
}
