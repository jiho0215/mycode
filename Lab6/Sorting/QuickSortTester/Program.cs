﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sorting;

namespace QuickSortTester
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rnd = new Random();
            List<Employee> emList = new List<Employee>();
            int EmployeeNum = 0;
            while (EmployeeNum < 10)
            {
                emList.Add(new Employee(rnd.Next(1000, 9999).ToString(), rnd.Next(19, 99), Math.Round((rnd.Next(40000, 200000) + rnd.NextDouble()),2)));
                EmployeeNum++;
            }

            Console.WriteLine("Original");
            foreach(Employee emp in emList) { Console.WriteLine("ID = {0}, Age = {1}, Salary = {2}", emp.ID, emp.Age, emp.Salary); }
            Console.WriteLine();

            QuickSort.Sort(emList);
            Console.WriteLine("QuickSort Default");
            foreach (Employee emp in emList) { Console.WriteLine("ID = {0}, Age = {1}, Salary = {2}", emp.ID, emp.Age, emp.Salary); }
            Console.WriteLine();

            QuickSort.Sort(emList, Employee.IComparerAgeAsc());
            Console.WriteLine("QuickSort IComparer Age Asc");
            foreach (Employee emp in emList) { Console.WriteLine("ID = {0}, Age = {1}, Salary = {2}", emp.ID, emp.Age, emp.Salary); }
            Console.WriteLine();

            QuickSort.Sort(emList, Employee.IComparerSalaryDesc());
            Console.WriteLine("QuickSort IComparer Salary Desc");
            foreach (Employee emp in emList) { Console.WriteLine("ID = {0}, Age = {1}, Salary = {2}", emp.ID, emp.Age, emp.Salary); }
            Console.WriteLine();

            QuickSort.Sort(emList, Employee.ComparisonIDDesc);
            Console.WriteLine("QuickSort Comparison Delegate ID Desc");
            foreach (Employee emp in emList) { Console.WriteLine("ID = {0}, Age = {1}, Salary = {2}", emp.ID, emp.Age, emp.Salary); }
            Console.WriteLine();

            QuickSort.Sort(emList, Employee.ComparisonAgeDesc);
            Console.WriteLine("QuickSort Comparison Delegate Age Desc");
            foreach (Employee emp in emList) { Console.WriteLine("ID = {0}, Age = {1}, Salary = {2}", emp.ID, emp.Age, emp.Salary); }
            Console.WriteLine();



            Console.ReadKey();
        }
    }
}
