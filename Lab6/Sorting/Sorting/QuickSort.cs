﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sorting
{
    public class QuickSort
    {
        public static void Sort<T>(List<T> list) where T : IComparable<T>
        {
            QuickSorter(list, 0, list.Count - 1);
        }
        static void QuickSorter<T>(List<T> list, int left, int right) where T : IComparable<T>
        {
            int i = left;
            int j = right;
            T pivot = list[left];
            while (i <= j)
            {
                while(list[i].CompareTo(pivot) < 0)
                {
                    i++;
                }
                while(list[j].CompareTo(pivot) > 0 )
                {
                    j--;
                }
                if (i <= j)
                {
                    T temp = list[i];
                    list[i] = list[j];
                    list[j] = temp;
                    i++;
                    j--;
                }
            }
            if (left < j)
            {
                QuickSorter(list, left, j);
            }
            if (i < right)
            {
                QuickSorter(list, i, right);
            }
        }

        public static void Sort<T>(List<T> list, IComparer<T> comparer)
        {
            QuickSorter(list, 0, list.Count - 1, comparer);
        }
        static void QuickSorter<T>(List<T> list, int left, int right, IComparer<T> comparer)
        {
            int i = left;
            int j = right;
            T pivot = list[left];
            while (i <= j)
            {
                while (comparer.Compare(list[i], pivot) < 0)
                {
                    i++;
                }
                while (comparer.Compare(list[j], pivot) > 0)
                {
                    j--;
                }
                if (i <= j)
                {
                    T temp = list[i];
                    list[i] = list[j];
                    list[j] = temp;
                    i++;
                    j--;
                }
            }
            if (left < j)
            {
                QuickSorter(list, left, j, comparer);
            }
            if (i < right)
            {
                QuickSorter(list, i, right, comparer);
            }
        }
        
        public static void Sort<T>(List<T> list, Comparison<T> comparison)
        {
            QuickSorter(list, 0, list.Count - 1, comparison);
        }
        static void QuickSorter<T>(List<T> list, int left, int right, Comparison<T> comparison)
        {
            int i = left;
            int j = right;
            T pivot = list[left];
            while (i <= j)
            {
                while (comparison(list[i], pivot) < 0)
                {
                    i++;
                }
                while (comparison(list[j], pivot) > 0)
                {
                    j--;
                }
                if (i <= j)
                {
                    T temp = list[i];
                    list[i] = list[j];
                    list[j] = temp;
                    i++;
                    j--;
                }
            }
            if (left < j)
            {
                QuickSorter(list, left, j, comparison);
            }
            if (i < right)
            {
                QuickSorter(list, i, right, comparison);
            }
        }

    }
}
