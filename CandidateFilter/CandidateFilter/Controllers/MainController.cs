﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CandidateFilter.Helpers;

namespace CandidateFilter.Controllers
{
    public class MainRunController
    {
        TimeHelper TimeHelper = new TimeHelper();

        public enum RunEnum { EODStart, IntradayStart, EODEnd, IntradayEnd, SymbolUpdate, FilterList, ValidateList };
        Dictionary<RunEnum, string> NextRunDic = new Dictionary<RunEnum, string>();
        RunEnum NextRun;
        DateTime NextRunDate = DateTime.Now;

        public void InitSettings()
        {
            NextRunDic.Add(RunEnum.FilterList, CandidateFilter.Setting.Default.CandidateFilterTime);
            NextRun = RunEnum.FilterList;
        }
        public void UnlimitCircle()
        {
            InitSettings();
            while (true)
            {
                if (IsRuntime(NextRun))
                {
                    RunTrigger(NextRun);
                }
                Console.WriteLine("waiting... " + DateTime.Now);
                TimeHelper.Pause(60000);
            }
        }

        public bool IsRuntime(RunEnum nextRunEnum)
        {
            bool isRunTime = false;
            DateTime runTime = Convert.ToDateTime(NextRunDic[nextRunEnum]);
            DateTime nextDTTM = NextRunDate.Date + runTime.TimeOfDay;
            if (nextDTTM.DayOfWeek == DayOfWeek.Saturday || nextDTTM.DayOfWeek == DayOfWeek.Sunday)
            {
                NextRunDate = GetNextBusinessDay(NextRunDate);
                Console.WriteLine("It's weekend. End Update Watch List. " + DateTime.Now);
                return isRunTime;
            }
            if (DateTime.Now > nextDTTM)
            {
                isRunTime = true;
            }
            return isRunTime;
        }

        public void RunTrigger(RunEnum nextRunEnum)
        {
            switch (nextRunEnum)
            {
                case RunEnum.FilterList:
                    Console.WriteLine("* Update WatchList Start " + DateTime.Now);
                    new CandidateFilterController().UpdateCandidateFilter();
                    Console.WriteLine("* Update WatchList End " + DateTime.Now);
                    NextRunDate = GetNextBusinessDay(NextRunDate);
                    break;
                default:
                    break;
            }
        }
        private DateTime GetNextBusinessDay(DateTime dateTime)
        {
            dateTime = dateTime.AddDays(1);
            if(dateTime.DayOfWeek == DayOfWeek.Saturday)
            {
                dateTime = dateTime.AddDays(1);
            }
            if (dateTime.DayOfWeek == DayOfWeek.Sunday)
            {
                dateTime = dateTime.AddDays(1);
            }
            return dateTime;
        }
    }
}