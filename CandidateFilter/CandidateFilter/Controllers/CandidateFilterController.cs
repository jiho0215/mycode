﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CandidateFilter.EntityFramework.Procedures;

namespace CandidateFilter.Controllers
{
    public class CandidateFilterController
    {
        CandidateFilterData candidateFilterData = new CandidateFilterData();
        public void UpdateCandidateFilter()
        {
            var FreshStocks = candidateFilterData.Get30DayFreshStocks();

            var BaseFilteredList = candidateFilterData.Get30DaysBaseFilteredList();
            List<string> watchList = new List<string>();
            watchList.AddRange(FreshStocks.Select(x => x.Symbol).ToList());
            watchList.AddRange(BaseFilteredList.Select(x => x.Symbol).ToList());
            new InsertData().UpsertFilteredList(watchList);
        }
    }
}
