﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace CandidateFilter.Helpers
{
    public class TimeHelper
    {
        public bool IsWithinTimeRange(string startTime, string endTime)
        {
            bool isWithInTimeRange = false;
            DateTime _startTime = Convert.ToDateTime(startTime);
            DateTime _endTime = Convert.ToDateTime(endTime);//.AddDays(1);
            if (_startTime > _endTime)
            {
                _endTime = _endTime.AddDays(1);
            }

            if (DateTime.Now > _startTime && DateTime.Now < _endTime)
            {
                isWithInTimeRange = true;
            }
            return isWithInTimeRange;
        }
        public void Pause(int second)
        {
            DateTime currentTime = DateTime.Now;
            while ((DateTime.Now - currentTime).TotalSeconds < second)
            {
                Thread.Sleep(10);
            }
        }
        public void PauseUntilStartTime(DateTime startTime)
        {
            while (DateTime.Now < startTime)
            {
                Pause(1);
            }
        }
    }
}
