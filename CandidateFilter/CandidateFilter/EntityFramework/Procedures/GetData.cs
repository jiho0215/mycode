﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CandidateFilter.EntityFramework.Procedures
{
    public class GetData
    {
        public EODLog GetEODLog(string symbol, DateTime date)
        {
            using (var db = new StockWEntities())
            {
                return db.EODLogs.Where(x => x.Symbol == symbol).ToList().Where(x => ((DateTime)x.CloseDate).Date == date.Date).FirstOrDefault();
            }
        }

        public IntraDayLog GetIntradayLogByCloseDTTM(string symbol, DateTime closeDTTM)
        {
            using (var db = new StockWEntities())
            {
                IntraDayLog log = db.IntraDayLogs.Where(x => x.Symbol == symbol).ToList().Where(x => ((DateTime)x.CloseDTTM) == closeDTTM).FirstOrDefault();
                return log;
            }
        }

        public List<EODLogGetAverage_Result> GetAverage(string symbol, int startDay, int endDay, bool isCloseAverage)
        {
            List<EODLogGetAverage_Result> result = new List<EODLogGetAverage_Result>();
            using (var db = new StockWEntities())
            {
                result = db.EODLogGetAverage(symbol, startDay, endDay, (isCloseAverage) ? "Y" : "N").ToList();
            }
            return result;
        }
        public List<EODLogGetDistribution_Result> GetDistribution(string symbol, int startDay, int endDay, bool isUseAverage)
        {
            List<EODLogGetDistribution_Result> result = new List<EODLogGetDistribution_Result>();
            using (var db = new StockWEntities())
            {
                result = db.EODLogGetDistribution(symbol, "Y", startDay, endDay, (isUseAverage) ? "Y" : "N").ToList();
            }
            return result;
        }
        public List<EODLogGetLowestHighest_Result> GetLowestHighest(string symbol, int interval)
        {
            List<EODLogGetLowestHighest_Result> result = new List<EODLogGetLowestHighest_Result>();
            using (var db = new StockWEntities())
            {
                result = db.EODLogGetLowestHighest(symbol, interval).ToList();
            }
            return result;
        }
        public List<EODLogGetMovement_Result> GetMovement(string symbol, int startDay, int endDay, bool isUseAverage)
        {
            List<EODLogGetMovement_Result> result = new List<EODLogGetMovement_Result>();
            using (var db = new StockWEntities())
            {
                result = db.EODLogGetMovement(symbol, startDay, endDay, (isUseAverage) ? "Y" : "N").ToList();
            }
            return result;
        }
        public List<EODLogGetPosition_Result> GetPosition(string symbol, int startDay, int endDay, bool isUseAverage)
        {
            List<EODLogGetPosition_Result> result = new List<EODLogGetPosition_Result>();
            using (var db = new StockWEntities())
            {
                result = db.EODLogGetPosition(symbol, startDay, endDay, (isUseAverage) ? "Y" : "N").ToList();
            }
            return result;
        }
        public List<EODLogGetVolatility_Result> GetVolatility(string symbol, int startDay, int endDay)
        {
            List<EODLogGetVolatility_Result> result = new List<EODLogGetVolatility_Result>();
            using (var db = new StockWEntities())
            {
                result = db.EODLogGetVolatility(symbol, startDay, endDay).ToList();
            }
            return result;
        }
        public WatchSymbol GetFilteredWatchStock(string symbol)
        {
            WatchSymbol result = null;
            using (var db = new StockWEntities())
            {
                result = db.WatchSymbols.Where(x => x.Symbol == symbol).FirstOrDefault();
            }
            return result;
        }
    }
}
