﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CandidateFilter.Models.DataContainer;

namespace CandidateFilter.EntityFramework.Procedures
{
    public class InsertData
    {
        private GetData GetData = new GetData();

        public void InsertFilteredStock(FilteredStock stock)
        {
            DateTime now = DateTime.Now;
            WatchSymbol watchSymbol = new WatchSymbol()
            {
                CreateDTTM = now,
                IsActive = "Y",
                IsHoldingStock = "N",
                IsManualInput = "N",
                IsMarketIndex = stock.IsMarketIndex ? "Y" : "N",
                IsValidated = "N",
                Symbol = stock.Symbol,
                TargetBuy = stock.TargetBuy,
                TargetSell = stock.TargetSell
            };
            using (var db = new StockWEntities())
            {
                try
                {
                    db.WatchSymbols.Add(watchSymbol);
                    db.SaveChanges();
                }
                catch (Exception e)
                {
                    new InsertError().InsertErrorMsg(stock.Symbol, "Failed to insert data InsertValidatedStock(). Message: " + e.Message);
                }
            }
        }
        public void UpsertFilteredList(List<FilteredStock> filteredStocks)
        {
            ResetAllFilteredStockFlag();

            if (filteredStocks != null && filteredStocks.Count > 0)
            {
                foreach (var stock in filteredStocks)
                {
                    if (IsFilteredStockExist(stock.Symbol))
                    {
                        UpdateFilteredStock(SetTargetPrice(stock));
                    }
                    else
                    {
                        InsertFilteredStock(SetTargetPrice(stock));
                    }

                }
            }
        }
        public void UpsertFilteredList(List<string> symbols)
        {
            List<FilteredStock> filteredStocks = new List<FilteredStock>();
            foreach(var symbol in symbols)
            {
                filteredStocks.Add(new FilteredStock()
                {
                    Symbol = symbol
                });
            }
            UpsertFilteredList(filteredStocks);
        }

        public void ResetAllFilteredStockFlag()
        {
            using (var db = new StockWEntities())
            {
                try
                {
                    var watchSymbols= db.WatchSymbols.ToList();
                    if (watchSymbols != null)
                    {
                        foreach (var watchSymbol in watchSymbols)
                        {
                            watchSymbol.IsActive = "N";
                        }
                        db.SaveChanges();
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }

        private bool IsFilteredStockExist(string symbol)
        {
            return (GetData.GetFilteredWatchStock(symbol) != null);

        }
        private void UpdateFilteredStock(FilteredStock stock)
        {
            using (var db = new StockWEntities())
            {
                try
                {
                    var existingStock = db.WatchSymbols.FirstOrDefault(x => x.Symbol == stock.Symbol);
                    if (existingStock != null)
                    {
                        existingStock.IsActive = "Y";
                        db.SaveChanges();
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }
        public FilteredStock SetTargetPrice(FilteredStock stock)
        {
            return stock;
        }
    }
}
