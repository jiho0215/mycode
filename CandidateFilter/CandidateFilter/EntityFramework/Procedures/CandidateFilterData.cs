﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CandidateFilter.EntityFramework.Procedures
{
    public class CandidateFilterData
    {
        StockWEntities StockWEntities = new StockWEntities();
        public List<EODLog30DayFreshGetVolaVolume_Result> Get30DayFreshStocks()
        {
            var freshStockInfo = StockWEntities.EODLog30DayFreshGetVolaVolume(null, 100000, (decimal?)1.5).ToList();
            return freshStockInfo;
        }

        public List<EODLogGetVolaVolume_Result> Get30DaysBaseFilteredList()
        {
            return StockWEntities.EODLogGetVolaVolume(null, 0, 30, 1000000, (decimal?)1.5, "N").ToList();
        }
    }
}
