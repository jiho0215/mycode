﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CandidateFilter.Models.DataContainer
{
    public class FilteredStock
    {
        public string Symbol { get; set; }
        public double? AvgVolatility { get; set; }
        public DateTime? VolatilityStartdate { get; set; }
        public DateTime? VolatilityEndDate { get; set; }
        public decimal? AvgVolume { get; set; }
        public DateTime? MovementStartdate { get; set; }
        public DateTime? MovementEndDate { get; set; }
        public double? Movement { get; set; }
        public double? TargetSell { get; set; }
        public double? TargetBuy { get; set; }
        public bool IsMarketIndex { get; set; }
        public int TargetSellPercentage { get; set; }
        public int TargetStopLossPercentate { get; set; }
    }
}
